# Architecture : systèmes sur puce (SoC)



La réduction de taille des éléments des circuits électroniques a conduit à l’avènement de 
**systèmes sur puce** (**SoC** pour System on Chip en anglais) qui regroupent dans un seul
circuit de nombreuses fonctions autrefois effectuées par des circuits séparés assemblés sur une carte électronique. 



## I. Composants d'un ordinateur

### 1. Entraînement à l'oral

**Consigne :**

Vous allez travailler en binôme sur l'un des composants d'un ordinateur. L'objectif est de réaliser un exposé de *5 minutes* (temps de parole également réparti). Vous n'avez pas le droit à un support de présentation (pas de diaporama, pas d'affiche...). Vous pouvez éventuellement avoir une feuille avec des notes, mais ces notes doivent être composées de *mots-clefs uniquement* (pas de phrases ni de texte).

**Ce que doit contenir l'exposé :**

* histoire du composant (date de création + éventuelles évolutions depuis)
* description physique (aspect, taille, poids, matériaux...)
* définition / principe / composition (qu'est-ce que ce composant ? comment fonctionne t-il ? + éventuellement ce dont il est composé)
* fonctions (à quoi le composant sert-il ?)
* lien avec les autres composants (à quoi est-il relié ? comment y est-il relié ? pourquoi ?)
* exemples de ce composant (modèles + concepteurs)
* toute autre information que vous jugez utile

**Sujets :**

* la carte mère *Noé - Arthur*
* le processeur (CPU) *Antoine - Younes - Esteban*
* la mémoire vive (RAM) *Thomas - Tristan*
* la mémoire de masse (disques durs...) *Fabien - Julien*
* la carte graphique (GPU) *Noah - Adam*
* la carte son *Nathan - Capucine*
* la carte réseau *Clément - Mamadou*
* les entrées/sorties (I/O + périphériques externes) *Gabriel - Sacha*
* les bus *Chloé - Baptiste*

### 2. Bilan

​	**1)** Replacez les différents composants présentés lors des exposés sur l'image suivante :

![composants ordinateur](img/ordinateur_composants.jpg)

|    Composant    | Numéro |
| :-------------: | :----: |
|  Alimentation   |        |
|   Processeur    |        |
|   Ventilation   |        |
|   Carte mère    |        |
| Carte graphique |        |
|    Carte son    |        |
|  Carte réseau   |        |
| Lecteur CD/DVD  |        |
|   Disque dur    |        |
|       RAM       |        |
|       Bus       |        |

C'est la carte mère qui supporte tous ces composants : 



![carte mère](img/carte_mere_composants.jpg)

Cette structure possède des avantages mais aussi des inconvénients.

Du côté des avantages, on peut citer :

- la facilité à changer de composants
- la possibilité d'*upgrader* son ordinateur avec des composants meilleurs ou plus récent
- la possibilité de remplacer un composant qui tombe en panne sans avoir à changer les autres
- L'espace permettant une bonne ventilation, il est possible de dissiper la chaleur et d'utiliser des composants très puissants en terme de performance.

Du côté des inconvénients, on trouve :

- l'espace nécessaire pour l'ensemble des pièces
- La quantité de câblages nécessaires pour relier les composants entre eux
- la relative lenteur engendrée par la taille des bus et la distance que doivent parcourir les données entre des composants éloignés
- une tension d'alimentation élevée et une consommation énergétique importante
- les coûts de fabrication et d'industrialisation de tous les composants qu'il faut concevoir séparément.



## II. SoC

### 1. Qu'est-ce que c'est ?

La poursuite de la performance pure n'est plus la seule course dans laquelle sont engagés les concepteurs. Avec la miniaturisation et les systèmes embarqués et le succès planétaire des smartphones (entre autres), le gain de place, la consommation (l'autonomie donc), la facilité et le coût de production sont devenus des problèmes de premier plan.

Une solution proposée est d'embarquer tous les composants, ou presque, sur une même puce : c'est le **S**ystem **o**n **C**hip.

Les SoCs sont utilisés dans tous les **téléphones, tablettes, systèmes embarqués et consoles**.

​	**2)** Cherchez le nom des SoC utilisés dans :

* le Samsung Galaxy S11 :
* la Nintendo Switch :
* votre téléphone :

Sur une seule puce, on trouve tous les contrôleurs nécessaires à son fonctionnement :

- le CPU
- le GPU
- la RAM
- les contrôleurs de son et de communication
- les bus
- d'autres capteurs

Le stockage de masse est par contre souvent confié à une carte SD en attendant de pouvoir également être intégré à la puce.

Il n'existe que deux architectures pour les SoC :

- L'**architecture [x86](https://fr.wikipedia.org/wiki/X86)** d'Intel fondée en 1978
- L'**architecture [ARM](https://fr.wikipedia.org/wiki/Architecture_ARM)** utilisée pour la première fois en 1987, qui est la plus répandue et que l'on retrouve dans la plupart des systèmes embarqués :
  - la téléphonie mobile et les tablettes (SoC [Qualcomm Snapdragon](https://fr.wikipedia.org/wiki/Qualcomm_Snapdragon) par exemple).
  - les microcontrôleurs comme ceux embarqués par les célèbres cartes [Arduino](https://fr.wikipedia.org/wiki/Arduino).xi

Les SoC progressent également en terme de performance, notamment grâce au gain en vitesse de transports des données, obtenu en réduisant les distances entre les composants. Il commencent à apparaître également dans les ordinateurs portables où la recherche d'autonomie est également cruciale. En 2020, Apple sort le SoC [M1](https://en.wikipedia.org/wiki/Apple_M1), basé sur une architecture ARM, pour ses MacBook Pro. Ces derniers sont donnés comme les plus performants des MacBook tout en ayant la meilleurs autonomie de leur histoire.

### 2. Microcontrôleurs

Dans de nombreux **systèmes embarqués**, des **microcontrôleurs** se chargent d'effectuer des calculs spécifiques. Par exemple, le régulateur de vitesse d'une voiture utilise des microcontrôleurs qui adaptent le régime moteur en fonction de la vitesse du véhicule.

​	**3)** Donnez au moins 3 domaines d'applications des systèmes embarqués.

Le microcontrôleur possède donc un SoC dédié spécifiquement à sa tâche, une quantité de mémoire et une puissance de calcul réduite et une consommation électrique très faible. Ce sont la plupart du temps des composants autonomes qui exécutent un programme unique.

Un ordinateur utilise une architecture pour laquelle programmes et données se partagent la même mémoire.

​	**4)** Comment s'appelle cette architecture utilisée par les ordinateurs (vue en 1ère) ?

Ainsi une tablette ou un smartphone sont des ordinateurs. Pour nos microcontrôleurs ce n'est pas le cas :

- le programme est stocké dans une mémoire dite *morte*, c'est à dire une mémoire qui ne perd pas ses données même lorsque l'alimentation est coupée.
- les données sont stockée dans une mémoire volatile.

Nous avons déjà parlé des architectures x86 et ARM des SoC destinés à des ordinateurs. Les microcontrôleurs sont, eux, souvent basés sur l'**architecture [Harvard](https://fr.wikipedia.org/wiki/Architecture_de_type_Harvard#/media/Fichier:Architecture_Harvard.png)** afin de simplifier l'architecture globale et de gagner en efficacité dans leur tâche spécifique.

[![Architecture Harvard](https://upload.wikimedia.org/wikipedia/commons/6/69/Architecture_Harvard.png)](https://upload.wikimedia.org/wikipedia/commons/6/69/Architecture_Harvard.png)

### 3. Exemples de SoC

Les smartphones utilisent ce type de puce, comme, ci dessous le SoC Exynos du Samsung Galaxy S3 :

[![smartphone](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Samsung-Exynos-4412-Quad_SoC_used_in_I9300.jpg/800px-Samsung-Exynos-4412-Quad_SoC_used_in_I9300.jpg)](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Samsung-Exynos-4412-Quad_SoC_used_in_I9300.jpg/800px-Samsung-Exynos-4412-Quad_SoC_used_in_I9300.jpg)

Voici le SoC de l'IPhone 11 :

[![Iphone 11](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/A13_APL1W85_iPhone11_mlb_820-01523-A.jpg/747px-A13_APL1W85_iPhone11_mlb_820-01523-A.jpg)](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/A13_APL1W85_iPhone11_mlb_820-01523-A.jpg/747px-A13_APL1W85_iPhone11_mlb_820-01523-A.jpg)

Pour finir une carte d'un Arduino où on aperçoit le microcontrôleur [Atmel](https://fr.wikipedia.org/wiki/Atmel_AVR) basé sur l'architecture Harvard :

[![arduino](https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Arduino_Diecimila.jpg/800px-Arduino_Diecimila.jpg)](https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Arduino_Diecimila.jpg/800px-Arduino_Diecimila.jpg)



### 4. Bilan (avantages / inconvénients)

Ainsi les Soc :

- sont moins onéreux à produire
- sont moins gourmand en énergie donc offre une grande autonomie
- permettent de gagner en vitesse en réduisant la taille des bus
- simplifient le circuits de la carte mère

Mais ils ne sont pas exempts de défauts :

- Il est impossible d'upgrader le matériel. Il est figé définitivement.
- Si un composant intégré tombe en panne, il est irréparable et inchangeable : le Soc entier est alors hors service.
- En 2021, même si ils font des progrès en terme de performance, ils n'atteignent pas les sommets des architectures traditionnelles.



### 5. Pour aller plus loin :

* Vous pouvez regarder [cette vidéo](https://www.youtube.com/watch?v=L4XemL7t6hg) qui résume les SoC (arrêtez-vous à la minute 5:33).
* Vous pouvez lire [cet article](https://www.embedded.com/designing-with-arm-cortex-m-based-system-on-chips-socs-part-i-the-basics/) qui détaille le fonctionnement d'un Soc.

* Cherchez quels sont les principaux concepteurs actuels des SoC ?

_________________

II. inspiré du travail de Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
