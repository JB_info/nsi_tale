# Sécurisation des communications

La sécurisation des échanges est un enjeu majeur de l’économie moderne. Sans elle il serait impossible de mettre en œuvre une économie globalisée.

Elle repose sur la **cryptographie** (écriture secrète) et la **cryptanalyse** (analyse de cette dernière).

La cryptographie est pourtant une science très ancienne, on en trouve des traces 2000 ans avant notre ère en Égypte ancienne.  
Les techniques mises en œuvre étaient très variées : substitutions de lettres, langues secrètes, stéganographie (faire passer un message inaperçu dans un autre support), etc.

## I. Principe & vocabulaire

Voici le vocabulaire de la cryptographie :

* **Message clair** = message que tout le monde peut lire.  
  *exemple : "LES NAVIRES ARRIVENT A MINUIT"*
* **Message chiffré** = message auquel on a appliqué une méthode de chiffrement avec une clé.  
  *exemple : "KZQ BPCUEZ PEEUCZBR P LUBYUR"*
* **Clé** = renseignement permettant de chiffrer ou déchiffrer un message.
  *exemple : un nombre, une phrase, une image...*
* **Chiffrer** = transformer un message clair en message chiffré en appliquant une clé.
* **Déchiffrer** = transformer un message chiffré en message clair en appliquant une clé.
* **Décrypter** : retrouver le message clair sans connaître la clé.

Vous remarquerez que "CRYPTER UN MESSAGE" NE SE DIT PAS.

**Q1)** Vous avez réussi à comprendre que le message chiffré "KZQ BPCUEZ PEEUCZBR P LUBYUR" signifie en clair "LES NAVIRES ARRIVENT A MINUIT". Vous avez intercepté un nouveau message, "VEPCI CIYQ PCZA REIYCZ". Que signifie-t-il ? Avez-vous *déchiffrer* ou *décrypter* le message ?

Les différentes étape d'une communication sécurisée sont donc :

* création du message clair
* modification du message par chiffrement
* envoi du message chiffré
* déchiffrement par le destinataire
* lecture du message clair

Les méthodes de chiffrement utilisent des clés qui vont permettre, grâce à des algorithmes, de chiffrer ou déchiffrer des messages.  
Une fois le message chiffré, il sera illisible par quelqu’un qui ne possède pas la clé de déchiffrement.

Il existe deux méthodes de chiffrement: le **chiffrement symétrique** et le **chiffrement asymétrique**.

## II. Chiffrement symétrique

### Principe

Dans un chiffrement symétrique, c'est **la même clé** qui sert au chiffrement *et* au déchiffrement.

![symétrique](img/sym.png)
### Un exemple

Un exemple historique : le code César (60 av. J-C).  
La clé est un nombre qui correspond à un décalage appliqué dans l'alphabet.

Par exemple, Alice chiffre son message en décalant les lettres de 3 rangs vers la droite dans l'alphabet, Bob saura qu'il doit les décaler de 3 rangs vers la gauche pour retrouver le message initial :

![césar](img/cesar.png)

**Q2)** Chiffrez le message "CODE CESAR" avec la clé 4. Quelle clé devrez-vous utiliser pour déchiffrer ?

**Q3)** Déchiffrez le message "UAOGVTKSWG" avec la clé 2. Quelle clé a été utilisée pour chiffrer ?

**Q4)** Dans un fichier Python, définissez une variable globale `ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"`.

**Q5)** Écrivez une fonction Python `chiffre(message_clair, cle)` qui renvoie le message chiffré en utilisant le code César et la clé passée en paramètre.

**Q6)** Écrivez une fonction Python `dechiffre(message_chiffre, cle)` qui renvoie le message clair en utilisant le code César et la clé passée en paramètre.

Le code César n'est plus utilisé aujourd'hui car les messages chiffrés sont trop faciles à décrypter : il suffit de tester toutes les clés possibles, soit seulement 25 possibilités.

**Q7)** Pourquoi n'y a-t-il que 25 clés à tester ?

**Q8)** Écrivez une fonction Python `decrypte(message_chiffre)` qui affiche toutes les clés et message clair possibles pour un message chiffré.  
Par exemple avec le message chiffré "OGUUCIG", cela pourra ressembler à ceci :
```python
clé 1 : NFTTBHF
clé 2 : MESSAGE
clé 3 : LDRRZFD
...
```

**Q9)** En utilisant votre fonction précédente, devinez la clé utilisée pour chiffrer le message "OVRA WBHR".

### Autres méthodes

Mis à part le code César, il existe de nombreux chiffrements symétriques :

* Le chiffrement affine : la clé est un double nombre (a,b) et on applique un calcul mathématique. Facilement décryptable en analysant la fréquence des lettres.

**Q10)** Quelle est la lettre la plus fréquente dans l'alphabet français ? Trouvez donc par quoi est chiffrée cette lettre dans le message suivant : "QL WLBE KLJYFWALY SLZ TLZZHNLZ MHJPSLTLUA"

* Le chiffre de [Vigenère](https://fr.wikipedia.org/wiki/Chiffre_de_Vigen%C3%A8re) : la clé est un mot plus petit que le message clair. Il a résisté 300 ans aux assauts des analystes mais a été percé en 1863 par le cryptologue Friedrich Kasiski.

* Le [masque jetable](https://fr.wikipedia.org/wiki/Masque_jetable) : la clé est un mot/une phrase cette fois plus grande que le message clair. Extrêmement robuste encore aujourd'hui, mais difficile à mettre en place.

* [AES](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard) (Advanced Encryption Standard) : c'est le chiffrement symétrique le plus utilisé aujourd'hui. Il dépasse largement le programme de NSI, vous devez juste savoir qu'il repose sur des transformations bit par bit qui donnent au message une apparence aléatoire.

* Et bien d'autres encore :
	* TKIP (wifi WPA)
	* DES (qui ne résiste plus aux attaques brute force)
	* SERPENT, Blowfish, Twofish (concurrents moins employés d'AES)
	* E0 (bluetooth)
	* ChaCha20 (web)
	* ...


### Avantages / inconvénients

L'avantage ? Les chiffrements symétriques sont souvent rapides, consommant peu de ressources et donc adaptés au chiffrement de flux important d'informations.

L'inconvénient ? La clé : si Alice et Bob ont besoin d'utiliser un chiffrement pour se parler, comment peuvent-ils échanger leurs clés puisque leur canal de transmission n'est pas sûr ?

Le chiffrement symétrique impose qu'Alice et Bob aient pu se rencontrer physiquement au préalable pour convenir d'une clé secrète, ou bien qu'ils aient réussi à établir une connexion sécurisée pour s'échanger cette clé.  

> Cela nous amène au principe de Kerckhoffs (1883) : *La sécurité d'un système de chiffrement ne doit reposer que sur la sécurité de la clé, et non pas sur la connaissance de l'algorithme de chiffrement. Cet algorithme peut même être public (ce qui est pratiquement toujours le cas).*


## III. Chiffrement asymétrique

Inventé par Whitfield Diffie et Martin Hellman en 1976, le chiffrement asymétrique vient résoudre l'inconvénient essentiel du chiffrement symétrique : le nécessaire partage d'un secret (la clé) avant l'établissement de la communication sécurisée.

### Principe

Le principe de base est l'existence de **deux clés différentes** : une **clé publique**, amenée à être distribuée largement pour le chiffrement, et **une clé privée**, qui ne quitte jamais son propriétaire pour le déchiffrement.

![asymétrique](img/asym.png)

### Un exemple simple

Le chiffrement asymétrique repose sur une *fonction à sens unique*. L'exemple le plus connu repose sur le produit et la factorisation.

* Produit = multiplier deux entiers.
* Factorisation = retrouver les facteurs à partir du produit.

Le produit est très facile à calculer : toutes les machines savent le faire !

La factorisation est très difficile par contre : les machines savent faire mais sont extrêmement lentes, surtout quand le produit est grand.

C'est donc bien une fonction *à sens unique* : on peut multiplier facilement mais factoriser est quasi-impossible.

Le lien avec le chiffrement asymétrique ?
* la clé publique est le produit
* la clé privée est les facteurs

Ainsi même si quelqu'un intercepte la clé publique, il ne pourra jamais retrouver la clé privée.

**Q11)** Vous êtes parvenus à intercepter la clé publique : 263467. Comment faudrait-il faire pour retrouver la clé privée ? Est-ce réalisable à la main ?

**Q12)** Ecrivez une fonction `factorise(cle_pub)` qui prend en paramètre une clé publique (=produit) et retrouve les deux facteurs entiers de ce produit.

**Q13)** Testez votre fonction avec `cle_pub = 14`, puis `cle_pub = 263467` et enfin `cle_pub = 20704949968242053`. Que se passe-t-il ?

### RSA

Lorsqu'en 1976 Diffie et Hellman présentent le concept de chiffrement asymétrique, ils en proposent uniquement un modèle théorique, n'ayant pas trouvé une réelle implémentation de leur protocole.

Trois chercheurs du MIT (Boston), Ron **R**ivest, Adi **S**hamir et Len **A**dleman se penchent alors sur ce protocole, convaincus qu'il est en effet *impossible* d'en trouver une implémentation pratique. En 1977, au cours de leurs recherches, ils démontrent en fait l'inverse de ce qu'ils cherchaient : ils créent le premier protocole concret de chiffrement asymétrique : le chiffrement RSA.

Le chiffrement RSA est basé sur l'arithmétique modulaire et sur la factorisation de nombres premiers. On n'entrera pas dans les détails car cela dépasse le programme, mais les matheux pourront se renseigner [ici](https://fr.wikipedia.org/wiki/Chiffrement_RSA).

*RSA est toujours utilisé aujourd'hui !* 

Le chiffrement RSA a des défauts, notamment une grande consommation des ressources, due à la manipulation de très grands nombres.  
Mais le choix d'une clé publique de grande taille (actuellement 1024 ou 2048 bits) le rend pour l'instant inviolable.

Actuellement, il n'existe pas d'algorithme efficace pour factoriser un nombre ayant plusieurs centaines de chiffres.

Deux évènements pourraient faire s'écrouler la sécurité du RSA :

* La découverte d'un algorithme efficace de factorisation, capable de tourner sur les ordinateurs actuels.
* L'avènement d'*ordinateurs quantiques*, dont la vitesse d'exécution permettrait une factorisation rapide (notez que l'algorithme de factorisation destiné à tourner sur un ordinateur quantique existe déjà : l'algorithme de [Schor](https://fr.wikipedia.org/wiki/Algorithme_de_Shor) !).

## IV. HTTPS

### Principe

On rappelle que le protocole **HTTP** (HyperText Transfer Protocol) permet au client de faire une requête au serveur et le serveur répond à la requête.

**Q14)** Rappelez à quelle couche réseau appartient le protocole HTTP ?

Le problème de HTTP est la sécurité : par exemple, quelqu’un pourrait intercepter les données envoyées par un client au serveur et les lire sans problème.

C’est pourquoi la version sécurisé de HTTP a été créée. Elle s’appelle **HTTPS** (HyperText Transfer Protocol Secure).

HTTPS est la combinaison de HTTP et d'un autre protocole, **TLS** (Transport Layer Security, première version en 1999), qui a remplacé SSL (Secure Socket Layer, première version en 1994).

Aujourd'hui, plus de 90 % du trafic sur internet est chiffré : les données ne transitent plus en clair (protocole HTTP) mais de manière chiffrée (protocole HTTPS), ce qui empêche la lecture de paquets éventuellement interceptés.

Trois objectifs sont visés par ce protocole :

1. **Authenticité** : un certificat assure que vous visitez bien le site voulu.
2. **Confidentialité** : les échanges sont chiffrés et ne peuvent être lus par un tiers.
3. **Intégrité** : les données ne sont pas modifiées par un tiers pendant la communication (l'attaque ["man in the middle"](https://fr.wikipedia.org/wiki/Attaque_de_l%27homme_du_milieu) est contrée).

**Q15)** En allant voir l'URL, dites si les échanges sur le site framagit sont sécurisés ou non.

### Étapes de la communication

#### 1. Authentification

HTTPS permet aux visiteurs de vérifier l’identité du site web auquel il se connecte. Cette identité est assurée par un **certificat** délivré par une **autorité**.

Cette étape se réalise en plusieurs temps :
* le client demande au serveur son certificat
* le serveur récupère un certificat auprès d'une autorité
* le serveur envoie le certificat récupéré au client
* le client vérifie que le certificat est valide

**Q16)** Numérotez les étapes du schéma suivant en expliquant ce qui est réalisé :

![certificat](img/certificat.png)

Il est possible de savoir quelle autorité de certification a fourni le certificat de site. Pour cela, il faut cliquer sur le cadenas à gauche de l'URL : ![cadenas](img/cadenas.png)

Vous avez alors le nom de l'autorité ainsi que la date de fin de validité du certificat :

![autorite](img/autorite.png)

Vous pouvez éventuellement aussi afficher le certificat en entier pour plus d'informations.

**Q17)** Quelle est l'autorité de certification pour le site framagit ? Le certificat est-il valide ?

Grâce à cette étape, HTTPS assure l'**authenticité** des communications.

#### 2. Initialisation par chiffrement asymétrique

Le certificat transmis pour l'authentification contient également la **clé publique** du serveur. Le serveur conserve la clé privée correspondante.

Le client utilise cette clé publique serveur pour chiffrer un message qu'il envoie au serveur. Ce message contient lui aussi une clé, la clé publique client.

Cette étape se réalise donc aussi en plusieurs temps :
* le serveur génère une clé publique `CléPubServeur` et une clé privée `CléPrivServeur` (= chiffrement asymétrique)
* le serveur ajoute sa clé publique `CléPubServeur` à son certificat avant de l'envoyer au client
* le client génère une clé publique `CléPubClient` uniquement (= chiffrement symétrique)
* le client chiffre sa clé publique `CléPubClient` avec la clé publique serveur reçue `CléPubServeur`
* le client envoie sa clé chiffrée au serveur
* le serveur déchiffre la clé reçue en utilisant sa clé privée `CléPrivServeur` : il retrouve donc la clé du client `CléPubClient`
* toutes les clés sont partagées, l'échange est donc bien initialisé

**Q18)** L'initialisation par chiffrement asymétrique a été rajoutée sur le schéma suivant. Numérotez et expliquez chaque étape :

![initialisation](img/initialisation.png)

Cette étape d'initialisation de la communication utilise donc un chiffrement asymétrique.

**Q19)** En cliquant à nouveau sur le cadenas, il est possible de retrouver le chiffrement asymétrique utilisé et la taille de la clé. Pouvez-vous trouver ces informations pour le site framagit ?

**Q20)** Si un utilisateur malveillant intercepte le dernier message envoyé par le client au serveur, parviendra-t-il à retrouver la clé publique du client ? Pourquoi ?

Grâce à cette étape, HTTPS assure l'**intégrité** des communications.

#### 3. Communication par chiffrement symétrique

Une fois l'initialisation terminée, le client et le serveur sont donc tous deux en possession de la clé publique du client. Ils peuvent donc s'en servir pour chiffrer leurs communications de manière symétrique.

**Q21)** Ajoutez au schéma précédent l'envoi d'une page web du serveur au client. Il faut donc que :
* le serveur chiffre la page web avec la clé publique client
* le serveur envoie la page chiffrée au client
* le client déchiffre la page web avec sa clé publique

**Q22)** En cliquant à nouveau sur le cadenas, il est possible de retrouver le chiffrement symétrique utilisé pour les échanges. Pouvez-vous trouver cette information pour le site framagit ?

**Q23)** Si un utilisateur malveillant intercepte une communication, pourra-t-il lire son contenu ? Pourquoi ?

Grâce à cette étape, HTTPS assure la **confidentialité** des communications.

#### Hachage

Il reste un problème : si un utilisateur intercepte un message du serveur et le modifie avant de le renvoyer au client, celui-ci n'a aucun moyen de le savoir.

Pour détecter les changements, il est nécessaire d'utiliser des **fonctions de hachages cryptographiques**.

Il faut juste savoir que ces fonctions transforment le message original en un autre mot. Si un seul des symboles est changé, le résultat est très différent donc le client voit tout de suite que le message a été altéré :

![hachage](img/hash.png)

Les plus utilisées aujourd'hui sont **SHA-2**, **SHA-3** et **MD5**.

**Q24)** En cliquant à nouveau sur le cadenas, il est possible de retrouver la fonction de hachage utilisée pour les échanges. Pouvez-vous trouver cette information pour le site framagit ?

**Q25)** Ces fonctions contribuent-elles à l'authenticité, la confidentialité ou l'intégrité des communications avec HTTPS ?


## Pour aller plus loin :

1. *End to End Encryption* : c'est un principe qui assure que le serveur qui fait la transition entre les messages de deux personnes ne peut pas lire leurs messages. Il est utilisé par Signal, WhatsApp, Telegram... Recherchez comment il fonctionne ?

2. *Énigme* : réussirez-vous à retrouver le personnage caché dans cette image ?

Vous avez trouvé une image bien étrange :

![mystere](img/mystere.png)

Un visage semble se deviner derrière un champ de fleurs... mais quel est ce visage ?

L'image du champ de fleurs ne vous est pas inconnue, d'ailleurs en cherchant bien vous l'avez retrouvée dans vos dossiers :

![masque](img/mask.jpg)


On dirait que le personnage mystère a voulu se fondre dans le champ de fleurs...

XORez-vous découvrir qui est ce personnage-mystère ?

*Aide : [documentation de la bibliothèque PIL pour la manipulation d'images en Python](https://pillow.readthedocs.io/en/stable/reference/Image.html) / [résumé ici](https://framagit.org/JB_info/nsi_1ere/-/blob/main/projets/filtres_images.md)*

_________________

Q1 inspirée de [qkzk](qkzk.xyz)  
Images et énigme finale inspirées de [G. LASSUS](https://github.com/glassus)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
