# Routage : exercices

### Connaissez-vous le cours ?

1. Qu'est-ce qu'un serveur DNS ?

2. Qu'est-ce qu'un serveur DHCP ?

3. Qu'est-ce qu'une architecture client/serveur ? Pour quel service sur Internet cette architecture est-elle la plus utilisée ?

4. Avec la norme IPv4, de combien d'adresses différentes dispose-on ?

5. Quel est le rôle du protocole TCP ?

6. Quel est le rôle du protocole IP ?

7. Donnez trois exemples de protocoles applicatifs.

8. Quel machine utilise t-on pour des communications au sein d'un même réseau ? Entre plusieurs réseaux différents ?

9. Quelle commande permet de tester la connexion entre deux appareils ?

10. Que fait la commande *traceroute* ?

11. Qu'est-ce qu'une table ARP ?

12. Quel est le délai entre 2 envois de tables avec RIP ?

13. Quel est le nombre de saut maximal de RIP ? À quoi sert-il ?

14. Citez deux avantages d'OSPF par rapport à RIP ?

15. Quelle est la formule pour trouver le coût en fonction de la bande passante pour OSPF ?


### Masques et IP

1. Dans un réseau, un hôte est configuré de la façon suivante :

   ```txt
   IP : 172.16.1.21

   masque : 255.255.0.0
   ```
   

    a. Quelle est l'adresse du réseau ?

   

    b. Quelle est l'adresse de broadcast ?

   

    c. Combien de machines ce réseau peut-il gérer ?



2. Mêmes questions pour la machine suivante :

   

   ```txt
   IP : 192.168.1.23/24
   ```


### Configurer un réseau



1. Configurez les hôtes ci-dessous pour que le réseau fonctionne.

    ![reseau a completer](img/reseau_a_completer.jpg)


2. Quel(s) est(sont) le(s) routeur(s) d'accès ? 

3. Comment qualifie-t-on les autres routeurs ?


### Protocole RIP

Voici un exemple de réseau :



![réseau](img/ex_routage.jpg)



1. Suivant le protocole RIP, comment calcule-t-on le coût d'une route ?

2. Complétez, selon ce protocole la table de routage du routeur A ci-dessous :

    | Destination | Passerelle | Métrique |
    | :---------: | :--------: | :------: |
    |      B      |     B      |    1     |
    |      C      |            |          |
    |      D      |            |          |
    |      E      |            |          |
    |      F      |            |          |
    |      G      |            |          |

3. Compléter, selon ce protocole, le tableau ci-dessous qui indique, pour chaque routeur de départ, la portion de sa table de routage vers le routeur G.

    | Départ | Destination | Passerelle | Métrique |
    | :----: | :----------: | :--------: | :------: |
    |   A    |      G       |            |          |
    |   B    |      G       |            |          |
    |   C    |      G       |            |          |
    |   D    |      G       |            |          |
    |   E    |      G       |            |          |
    |   F    |      G       |            |          |

4. Le routeur E tombe en panne. Quelle(s) ligne(s) de la table de routage du routeur B va (vont) être modifiée(s) ? 


### Protocole OSPF


Voici un exemple de réseau :



![réseau](img/ex_routage.jpg)


1. On rappelle que le coût, qui n'a pas d'unité, se calcule en appliquant la formule $`coût = \frac{10^8}{débit}`$ où le _débit_ est exprimé en bit/s. Complétez le tableau ci-dessous :

    |   interface   |            Débit             | Coût |
    | :-----------: | :--------------------------: | :--: |
    |     Modem     |          56 kbit /s          |      |
    |   Bluetooth   |           3 Mbit/s           |      |
    |   Ethernet    |                              |  1   |
    |     ADSL      |          13 Mbit/s           |      |
    |      4G       |          100 Mbit/s          |      |
    | FastEthernet  |          100 Mbit/s          |      |
    | FFTH ( fibre) |          10 Gbit/s           |      |

2. Suivant le protocole OSPF, comment calcule-t-on le coût d'une route (métrique) ?

3. Complétez, selon ce protocole la table de routage du routeur A ci-dessous :

    | Destination | Passerelle | Métrique |
    | :---------: | :--------: | :--: |
    |      B      |            |      |
    |      C      |            |      |
    |      D      |            |      |
    |      E      |            |      |
    |      F      |            |      |
    |      G      |            |      |

4. Complétez, toujours selon ce protocole, le tableau ci-dessous qui indique, pour chaque routeur de départ, la portion de sa table de routage vers le routeur G.

    | Départ | Destination | Passerelle | Métrique |
    | :----: | :---------: | :--------: | :------: |
    |   A    |      G      |            |          |
    |   B    |      G      |            |          |
    |   C    |      G      |            |          |
    |   D    |      G      |            |          |
    |   E    |      G      |            |          |
    |   F    |      G      |            |          |

5. Le routeur E tombe en panne. Quelle(s) ligne(s) de la table de routage du routeur B va (vont) être modifiée(s) ? 


### A l'envers

Voici les tables de routages de 5 routeurs A, B, C, D, E qui utilisent le protocole RIP.


Table de A :

| Destination | Passerelle | Distance |
| :---------: | :--------: | :------: |
|      B      |     B      |    1     |
|      C      |     B      |    2     |
|      D      |     D      |    1     |
|      E      |     D      |    2     |

Table de B

| Destination | Passerelle | Distance |
| :---------: | :--------: | :------: |
|      A      |     A      |    1     |
|      C      |     C      |    1     |
|      D      |     A      |    2     |
|      E      |     A      |    3     |

Table de C

| Destination | Passerelle | Distance |
| :----------: | :--------: | :------: |
| A            | B          | 2        |
| B            | B          | 1        |
| D            | B          | 3        |
| E            | B          | 4        |

Table de D

| Destination | Passerelle | Distance |
| :----------: | :--------: | :------: |
| A            | A          | 1        |
| B            | A          | 2        |
| C            | A          | 3        |
| E            | E          | 1        |

Table de E

| Destination | Passerelle | Distance |
| :----------: | :--------: | :------: |
| A            | D          | 2        |
| B            | D          | 3        |
| C            | D          | 4        |
| D            | D          | 1        |

Dessinez un réseau compatible avec ces tables de routage.


### Pour aller plus loin

1. Donnez un exemple de réseau à 5 routeurs où le protocole RIP donne une table de routage différente de celle du protocole OSPF.

2. Donnez les tables de routages des routeurs 3 et B du réseau suivant, selon RIP puis selon OSPF :

	![reseau](img/routeurs.jpg)

3. Configurez le réseau précédent sur Filius.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : [kxs](kxs.fr) & [C. Mieszczak](https://framagit.org/tofmzk/informatique_git)
