# Gestion des processus et des ressources par le système d'exploitation

## I. Système d'exploitation (rappels de Première)

Vous avez étudié les différentes tâches d'un **système d'exploitation** (**OS** = Operating System) en Première.
Le schéma ci-dessous illustre le fonctionnement d'un OS :

![schéma os](img/os.jpg)

* L'utilisateur agit sur les **applications** (Firefox, LibreOffice, Thonny...) en utilisant soit une interface graphique soit directement en ligne de commande dans une console (SHELL).
* Les applications utilisent des bibliothèques de fonctions qui s'appuient directement sur le **noyau** du système d'exploitation. Ce noyau gère la mémoire, le système de fichiers, les protocoles réseaux et l'**ordonnancement des processus** (que nous verrons dans ce chapitre).
* La gestion de la mémoire, du système de fichiers et des protocoles réseaux utilisent des périphériques (disque dur, RAM, cartes réseau ...) en passant par leur pilote respectif, c'est à dire le programme dédié au fonctionnement du périphérique.
* La gestion des processus est une affaire qui se règle entre l'OS et le microprocesseur.

**Q1)** Résumez (avec le schéma ou avec vos mots) le rôle du système d'exploitation quand :

1. un utilisateur ouvre un fichier avec LibreOffice
2. un utilisateur lance un *ping* en ligne de commande vers un autre ordinateur du réseau

## II. Processus

### 1. Définitions

**Un processus est un programme en cours d’exécution sur un ordinateur**. Il peut s'agir de fichiers liés au fonctionnement du système ou d'une application. Notez que si on exécute deux fois le même programme, on crée alors deux processus différents.

Pour s’exécuter, chaque processus à besoin d'utiliser des **ressources** : une certaine quantité de mémoire, les données d'un fichier, des entrées comme le clavier ou la souris, des sorties comme l'écran ou l'imprimante...

**L'OS gère les ressources et l’exécution des processus**. On dit que le système d'exploitation gère l'**ordonnancement des processus**.

**Q2)** Expliquez avec vos mots la différence entre un programme et un processus.

### 2. Création d'un processus 

La **création d'un processus** peut intervenir :

* au démarrage du système
* par un appel d'un autre processus
* par une action d'un utilisateur (lancement d'application)

**Q3)** Pour chacun des processus suivants, indiquez comment il a été créé :

1. une fenêtre Turtle a été ouverte lors de l'exécution d'un programme Python
2. un utilisateur a ouvert une fenêtre Firefox
3. un antivirus tourne en arrière plan

### 3. PID, PPID

Sous un système d'exploitation comme Linux, au moment du  démarrage de l'ordinateur un tout premier processus (appelé *processus 0*) est créé.  
Ensuite, ce processus 0 crée un processus souvent appelé *init*. Vous l'avez compris, un processus peut créer un ou plusieurs processus. Il est alors le père de ces processus qui, eux, sont ses fils.  
À partir de init, les processus fils nécessaires au bon fonctionnement du système sont créés, puis d'autres processus sont créés à partir des ces fils...  On peut modéliser ces  relations père/fils par une structure arborescente (arbre).

Dès leur création, chaque processus se voit attribuer un **numéro unique qui l'identifie** : son **PID**. Tous les processus, en dehors du processus 0, ont également un **PPID** qui indique le PID de leur parent.

Voici, par exemple, une représentation de cette arborescence pour un démarrage de Linux :

![ex arborescence](img/arborescence.jpg)

**Q4)**

* Quel est le PID du processus login ?
* Quel est le processus de PID 1239 ?
* Quel est le PPID du processus getty ?
* Quel est le processus de PID 1 ?
* Que peut-on déduire du fait que le PPID de inetd soit 1 ?
* Quel est le PID et le nom du processus qui a créé le processus shell ?

*Remarque : Les processus fils de init sont appelés démons, ils tournent en permanence en arrière plan. Par exemple, inetd permet de gérer les connexions à des services réseau, 
cron permet aux utilisateurs d’exécuter automatiquement des scripts, getty crée le processus login qui permet de s'identifier lorsqu'il détecte une connexion, etc.*


### 4. Etats des processus

Un processus a plusieurs états :

* L'**initialisation** est l'état dans lequel se trouve le processus lors de sa création.
* Il est ensuite dans un état **prêt**, c'est à dire dans l'attente d'être choisi par l'ordonnanceur pour être exécuté.
* Lorsqu'il est **élu**, c'est à dire choisi par l'OS pour être exécuté, il peut alors demander d'accéder à une ressource.
* Lorsqu'il est **bloqué**, il attend que les ressources dont il a besoin soient disponibles pour pouvoir repasser à l'état prêt.
* Enfin, il passe à l'état **terminé** lorsqu'il a achevé son travail ou qu'il a été forcé de s'arrêter.

![etats](img/etats.jpg)

L'OS choisit un processus parmi les processus prêts :

* si les ressources dont il a besoin sont disponibles, un temps lui est attribué pendant lequel il est exécuté puis remis en attente.
* sinon, il passe à l'état bloqué et l'OS élit un autre processus.

**Q5)** Donnez un exemple de situation où un processus a besoin d'une ressource non disponible.

### 5. Algorithmes d'ordonnancement des processus

Pour élire un processus (le faire passer de prêt à élu), l'OS utilise un **algorithme d'ordonnancement**.

Considérons ces trois processus, chacun comportant plusieurs lignes d'instruction :

![3 processus](img/trois_processus.jpg)

Dans un microprocesseur, un cœur exécute un seul processus à la fois.  Aujourd'hui, les micro-processeurs étant multicœurs, plusieurs processus peuvent être exécutés simultanément en parallèle... si l'OS est programmé de façon à pouvoir le faire.

**La performance du système dépend donc énormément de la façon dont l'OS gère ces processus.**

Voici quelques exemples d'algorithmes d'ordonnancement :

#### En parallèle (aussi appelée concurrente)

Si le microprocesseur possède plusieurs cœurs et que l'OS sait les gérer, il peut ordonner les processus en parallèle. Avec trois processus pour trois cœurs, cela donne alors 3 piles d'instructions, une par cœur :

![parallèle](img/parallele.jpg)

#### Tourniquet

Pour donner l'illusion que les différents programmes tournent en même temps, un temps très court est alloué à tour de rôle aux processus. C'est un algorithme simple et facile à mettre en œuvre. La rapidité avec laquelle on passe d'un processus au suivant donne l'illusion d'une exécution simultanée :

![tourniquet](img/tourniquet.jpg)

#### Premier entré, premier servi

Aussi appelé FCFS (First-Come First-Served), il est basé sur le principe FIFO des files. Chaque processus créé est placé dans une file, et les processus s'exécutent les uns après les autres. C'est celui qu'on utilise, par exemple, pour gérer la file d'impression d'une imprimante.

![fcfs](img/fcfs.jpg)

#### Plus court d'abord

Aussi appelé SJF (Shortest Job First). Le processus qui se terminera le plus vite passera en priorité. c'est un algorithme très efficace pour satisfaire l'utilisateur, mais il est compliqué de déterminer si un processus sera rapidement terminé ou pas avant qu'il ne s'exécute.

![sjf](img/sjf.jpg)

#### Priorités

Il est aussi possible de définir (à la main ou automatiquement par l'OS) des priorités pour chaque processus.

Sous Linux, on peut passer des consignes à l'ordonnanceur en fixant des priorités aux processus : cette priorité est un nombre entre -20 (plus prioritaire) et +20 (moins prioritaire).

Par exemple, on pourrait fixer une priorité de 4 au processus 1, de -7 au processus 2 et de 20 au processus 3. L'ordonnancement serait alors Processus 2, puis Processus 1, et enfin Processus 3.

#### Exercices

On considère les processus suivants :

![exercice_ordonnancement](img/exercice_ordonnancement.jpg)

* On suppose qu'on a un seul cœur. Donnez l'ordonnancement avec l'algorithme du tourniquet, puis premier entré premier servi et enfin plus court d'abord.
* On suppose qu'on a deux cœurs. Le premier ordonne les processus 1 et 2 avec un plus court d'abord, et le second les processus 3 et 4 avec un premier entré premier servi.
	* Donnez les deux piles d'exécution.
	* Quelles instructions vont s'exécuter en parallèle ?
* On suppose qu'on a deux cœurs, qui utilisent tous les deux un tourniquet. Le premier ordonne les processus 2 et 3 et le second les processus 1 et 4.
	* Donnez les deux piles d'exécution.
	* Quelles instructions vont s'exécuter en parallèle ?
* On suppose qu'on a un seul cœur. On fixe une priorité 5 au processus 1, 20 au processus 2, 1 au processus 3 et 12 au processus 4. Donnez l'ordonnancement.

### 6. Interblocage (deadlock)

Les interblocages sont des situations de la vie quotidienne. Un exemple est celui du carrefour avec priorité à droite où chaque véhicule est bloqué car il doit laisser le passage au véhicule à sa droite :

![carrefour](img/carrefour.png)

Les interblocages sont des problèmes qui peuvent survenir dans l'ordonnancement des processus.

**Un interblocage survient lorsque deux processus (ou plus de deux processus), s'attendent mutuellement** : le premier utilise une ressource nécessaire au second qui, lui même, utilise une ressource nécessaire au premier. Les deux processus sont donc placés dans un état bloqué  sans fin, temps qu'aucun des deux n'est détruit et ne libère la ressource qui débloquera l'autre.

L'ordonnancement est alors dans une impasse.

![interblocage](img/interblocage.svg)

Deux solutions sont mises en œuvre par l'OS devant ce problème :

* essayer d'éviter un interblocage... mais il n'est pas simple de les prévoir à l'avance, et même souvent impossible.
* détecter les interblocages quand ils surviennent et les supprimer, par exemple en détruisant les processus concerné. C'est la solution la plus fréquemment utilisée.

On détecte les interblocages en dessinant un graphe orienté comme celui montré ci-dessus. **Si le graphe contient un cycle, il y a un interblocage.**

**Q6)** Dessinez le graphe et montrez qu'il y a interblocage pour les 2 situations suivante :

* Ressource 1 = table Classes
* Ressource 2 = table Elèves
* Ressource 3 = table Profs
* Processus 1 = application DB Browser
* Processus 2 = programme Python lancé depuis Thonny
* Processus 3 = fichier ouvert avec un traitement de texte
* Processus 4 = application de tableur LibreOffice Calc

1. Situation 1 : DB Browser est ouvert sur une vue de la table Elèves, et est en attente de pouvoir accéder à la table Profs. Le tableur possède la table Classes, et est en attente de la table Elèves. Le programme Python est en attente de la table Profs. Le traitement de texte est ouvert sur une vue de la table Profs, et est en attente de la table Classes.
2. Situation 2 : DB Browser est ouvert sur une vue de la table Elèves, et est en attente de pouvoir accéder à la table Profs. Le tableur possède la table Classes, et est en attente de la table Profs. Le traitement de texte est en attente de la table Classes. Le programme Python est ouvert sur une vue de la table Profs, et est en attente de la table Classes.


### 7. Visualisation des processus

#### OS Windows

**Q7)** Appuyez sur CTRL + SHIFT + ECHAP. Allez sur l'onglet Détails. Quelles informations trouvez-vous ?

#### OS Linux

Sous Linux, la visualisation des processus se fait via la **console**. Pour rappel, la console ressemble à ceci :

![console](img/console.png)

*Remarque : La ligne de commande s'affiche en boucle. Elle se décompose ainsi : nomDeLUtilisateur@nomDeLaMachine:cheminDeLemplacementActuel$*

Elle permet d'utiliser des **commandes** :

![pwd](img/pwd.png)

*Remarque : Les commandes vues en Première que vous devez connaître pour le bac sont : man / pwd / cd / ls / cat / mkdir / touch / rm / cp / chmod / ping / traceroute / ifconfig.*

La commande pour visualiser les processus est **ps** :

![ps](img/ps.png)

**Q8)** La console de Linux s'appelle *bash*. Quel est son PID ?

**Q9)** Combien de processus Python tournent actuellement ? Quels sont leurs PID ?

Pour avoir plus de renseignements, on peut utiliser des options avec la commande *ps* (-e ajoute tous les processus, y compris le init ; et -f ajoute le PPID) :

![psfe](img/psfe.png)

**Q10)** Quel est le PPID du processus de la commande ps ?

**Q11)** Quel est le nom du processus de PID 1 ?

**Q12)** Quels sont les processus qui ont été créés par le système ? Comment les avez-vous identifiés ?

**Q13)** Les processus Python ont-ils tous été créés par le même processus ? Justifiez.

**Q14)** A votre avis, pourquoi les PID ne se suivent-ils pas tous ?

Il existe également une commande qui permet de représenter les processus sous forme d'arbre, **pstree** :

![pstree](img/pstree.png)

**Q15)** Quel est l'avantage de cette représentation ?

Pour finir, il est possible de forcer un processus à se terminer. On utilise la commande **kill** suivie d'un PID de processus.

Par exemple pour terminer le processus de PID 127 (le premier Python) :

![kill](img/kill.png)

On vérifie avec *ps* :

![killps](img/killps.png)

**Q16)** Le processus a-t-il bien été supprimé ?

**Q17)** Donnez la commande pour terminer le dernier processus Python. Donnez la commande pour vérifier qu'il a bien été supprimé.

**Q18)** Donnez la commande pour terminer le processus de la console (bash). A votre avis, que va-t-il arriver à ses fils (les processus Python restants) ?


# Pour aller plus loin

* Les commandes vues en Première que vous devez connaître pour le bac sont : man / pwd / cd / ls / cat / mkdir / touch / rm / cp / chmod / ping / traceroute / ifconfig. Rappelez à quoi servent chacune de ces commandes.
* Sous Windows, il est aussi possible de visualiser les processus depuis la console. Ouvrez le *cmd* et essayez la commande *tasklist*.
* Recherchez ce qu'est un *processus zombie*.

_________________

Inspiré du travail de Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
