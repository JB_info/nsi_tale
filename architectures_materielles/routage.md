# Réseau : protocoles de routage

## I. Rappels de Première

Si nécessaire, le cours de Première se trouve [ici](https://framagit.org/JB_info/nsi_1ere/-/blob/main/architectures_materielles/reseau.md).

Pour envoyer les données d'un émetteur à un destinataire hors du réseau local, il faut passer par des routeurs, qui servent de passerelles entre les réseaux ou vers d'autres routeurs.
Un routeur possède autant de cartes réseaux qu'il relie de réseaux ou de routeurs entre eux, de façon à pouvoir communiquer avec chacun d'entre eux avec des adresses IP compatibles avec toutes ces machines.
Il existe deux type de routeurs :

* Les *routeurs d'accès* donnent directement accès à un ou plusieurs réseaux. Ils utilisent des interfaces Wi-Fi ou Ethernet.
* Les *routeurs internes* relaient les paquets vers des routeurs d'accès. Ces routeurs, souvent connectés sur de plus grandes distances, utilisent des fibres optiques, liaisons satellite ou câbles téléphoniques.

![routeurs](img/routeurs.jpg)

**Q1)** Quel(s) routeur(s) sont des routeurs internes ? Des routeurs d'accès ?

**Q2)** Combien d'interface(s) réseau possèdent les routeurs Routeur1, Routeur A, Routeur C ?

*Remarque :  
Une box internet est à la fois :*

* *Switch : elle permet de connecter entre eux les machines du réseau local.*
* *Serveur DHCP : elle attribue une adresse IP privée à chaque nouvelle machine connectée sur son réseau local.*
* *Serveur DNS : elle fait le lien entre le nom de domaine du site que vous voulez atteindre et l'adresse IP du serveur qui l'héberge. Si elle ne connait pas le site, elle interrogera le serveur DNS de vote fournisseur d'accès.*
* *Routeur NAT : elle fait la correspondance entre son adresse privée et l'adresse publique de l'émetteur.*
* *Routeur : elle offre un accès au réseau global qu'est internet.*

Une fois arrivés à destination, les paquets remontent les couches du modèle TCP/IP à l'envers :

![tcp/ip](img/aller_retour.jpg)

L'objet de cette leçon est de comprendre comment se passe ce routage.

## II. Tables de routage

Nous allons d'abord utiliser des notations simplifiées pour comprendre les mécanismes. Nous verrons ensuite des situations plus proches de la réalité utilisant des adresses IP.

Nous considérons donc la situation suivante avec huit routeurs représentés par des lettres : 

![A...H](img/abcdefgh.png)

Le but d'un protocole de routage est de **déterminer pour chaque routeur la « meilleure » route pour atteindre chaque autre routeur**. Par exemple, pour notre réseau, la meilleure route pour aller de A à F est-elle A-C-F ou A-B-D-F ?

Nous verrons que la meilleure route va dépendre de la **métrique** choisie. La métrique est la façon de calculer la distance entre deux routeurs.

*Remarque : Le chemin qui utilise le moins de routeur n'est pas forcément le meilleur : le débit de l'interface utilisée par le routeur (fibre optique plutôt que câble téléphonique en cuivre par exemple) peut rendre un chemin plus rapide qu'un autre, même en passant par un plus grand nombre de routeurs.*

Une table de routage peut être vue comme un tableau contenant les informations nécessaires au routeur pour savoir à quelle machine transmettre les données qu'il reçoit pour qu'elles puissent atteindre leur destination. Chaque routeur possède sa table de routage.

Chaque routeur va donc établir une **table de routage** qui contiendra une ligne par **destination** lui indiquant le voisin à contacter, c'est-à-dire la **passerelle** et la distance jusqu'à la destination, c'est-à-dire la **métrique**. Voici un exemple de table routage pour le routeur A :

![table A](img/table_A.png)

**Q3)** Donnez un exemple de table de routage pour le routeur D.

Un réseau est donc modélisé par un graphe dont les sommets sont les routeurs et les arêtes les interfaces. Pour trouver un chemin dans le réseau, on recherche donc un chemin dans le graphe en utilisant les algorithmes adéquats. Il en existe de nombreux, nous allons en étudier deux : les **protocoles RIP et OSPF**.

Ce routage automatique par un protocole est appelé routage **dynamique** : on utilise des protocoles qui vont permettre de "découvrir" les différentes routes automatiquement afin de pouvoir remplir la table de routage tout aussi automatiquement.  
On peut également utiliser le routage **statique** : chaque ligne de la table doit être renseignée "à la main". Le nombre de routeurs sur le réseau est généralement trop grand pour qu'on puisse les configurer à la main. De plus, cette configuration doit être actualisée en permanence au cas ou un routeur tombe en panne (il faut alors trouver un autre chemin) ou que sa topologie est modifiée (nouveaux routeurs installés, nouveaux réseaux ajoutés, type d'interface modifiée ..).


## III. RIP

Le protocole de routage RIP (**Routing Information Protocol**) est un protocole datant de 1969 qui n'est plus beaucoup utilisé. Il a connu plusieurs évolutions, la dernière, RIPng, permet de gérer l'IPv6.

Le protocole RIP est basé sur l'algorithme de Bellman-Ford. Il fonctionne sur le principe d'un échange d'information périodique entre les routeurs (toutes les 30 secondes). **La métrique utilisée est le nombre de sauts** (hops) pour atteindre une destination. Le nombre de sauts correspond au nombre de routeurs à traverser pour atteindre la destination.

Au démarrage et **toutes les 30 secondes, chaque routeur envoie sa table de routage à ses voisins**. Ils mettent alors à jour leur propre table de routage en fonction des informations reçues de façon à ne garder que le chemin le plus court pour chaque destination. Au début, les tables de routages ne contiennent que les voisins directs de chaque routeur. Mais au fur et à mesure des échanges, les tables grandissent et convergent vers un état stable. Le protocole **RIP limite le nombre de sauts à 15** pour éviter les boucles de routage (pour ne pas tourner à l'infini). La valeur 16 signifie que la destination est injoignable.

![A...H](img/abcdefgh.png)

Prenons l'exemple du routeur A dans notre réseau.

Sa première table de routage ne contient que ses proches voisins :

![table](img/t1.png)

Après la première étape d'échanges avec ses voisins proches, il récupère les informations venant de B et C. B et C pouvant atteindre D, E et F avec un saut, A va insérer ces destinations dans sa table de routage en ajoutant un saut (permettant de joindre B et C). Sa table de routage sera alors la suivante :

![table](img/t2.png)

Il va alors continuer de mettre à jour sa table en fonction des informations reçues. Sa table de routage va converger vers la table suivante :

![table](img/t3.png)

**Q4)** Donnez étape par étape les évolutions de la table de routage du routeur D.

**Q5)** Combien de temps a pris la construction de la table de routage  complète de D ?

**Q6)** Donnez la table de routage complète de E.

**Q7)** Quel sera le chemin emprunté par un paquet allant de A à G ?

**Q8)** Le routeur F tombe en panne. Donnez alors les nouvelles tables de routage des routeurs A, D et E.

RIP n'autorise pas les chemins de plus de 15 sauts, cela est très **limitant pour les réseaux importants**. De plus, RIP ne se base que sur le nombre de sauts et **ne tient pas compte l'état de la liaison** entre deux routeurs. Ainsi RIP ne verra pas une route plus rapide qui utilise un nombre plus grand de routeurs. Le protocole OSPF corrige ces principales limitations.

## IV. OSPF

Le protocole de routage OSPF (**Open Shortest Path First**) est un protocole plus récent que RIP et destiné à corriger ses limitations. Sa dernière version datant de 2008 est massivement utilisée sur internet.

OSPF est basé sur l'algorithme de Dijkstra pour déterminer le chemin le plus court. Le principe d'échange d'information entre les proches voisins reste le même que pour le protocole RIP. Néanmoins dans le cas de OSPF chaque routeur garde en mémoire toutes les informations avant de déterminer, avec l'algorithme de Dijkstra, la route la plus courte. Les routeurs ont donc une vision plus globale du réseau avec le protocole OSPF qu'avec le protocole RIP.

La différence majeure avec RIP est la métrique choisie qui dépend ici du **débit des connexions** entre les routeurs. On appelle cette métrique coût et on la définie par la formule suivante :

$`\frac{10^8}{débit}`$ où débit représente la bande passante en **bits par seconde** entre les deux routeurs. Ainsi un lien avec une forte bande passante aura un coût très faible et sera donc privilégié par rapport à un lien avec une bande passante plus faible.

**Q9)** Voici quelques débits théoriques. Calculez leur coût.

>  | Technologie              |    Bande passante     | Coût |
>  | ------------------------ | :-------------------: | :--: |
>  | `Modem`                  |       56 kbit/s       |      |
>  | `Bluetooth`              |       3 Mbit/s        |      |
>  | `Ethernet`               |       10 Mbit/s       |      |
>  | `Wi-Fi`                  |       54 Mbit/s       |      |
>  | `ADSL`                   |       13 Mbit/s       |      |
>  | `4G`                     |      100 Mbit/s       |      |
>  | `Satellite`              |       50 Mbit/s       |      |
>  | `FastEthernet`           |      100 Mbit/s       |      |
>  | `Fibre optique` (`FFTH`) |       10 Gbit/s       |      |


**Q10)** Quel est la bande passante d'une liaison avec un coût de 1 ?

Revenons sur le même exemple que celui du protocole RIP. Nous avons ajouté ici les bandes passantes des liaisons entre routeurs.

![ospf](img/ospf_abcdefgh.png)

**Q11)** Reproduisez le graphe du réseau en remplaçant les débits par les coûts sur les liaisons entre routeurs.

**Q12)** Sans nécessairement utiliser un algorithme (le graphe étant simple), déterminez la table de routage finale de A, D et E.


## V. Comparaison de RIP et OSPF

Nous allons illustrer à l'aide d'un cas très simple la principale différence entre RIP et OSPF. Voici donc notre réseau très simple composé de trois routeurs :

![diff](img/diff.png)

Nous avons précisé les débits des liens entre routeurs pour pouvoir utiliser le protocole OSPF. Ces débits sont bien entendu inutiles pour le protocole RIP.

**Q13)** Quel est le chemin emprunté par un paquet allant de A à B si le protocole RIP est utilisé ?

**Q14)** Reproduisez le graphe du réseau en remplaçant les débits par les coûts sur les liaisons entre routeurs.

**Q15)** Donnez alors la route avec le moindre coût (OSPF) empruntée par un paquet allant de A à B.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : [kxs](kxs.fr) & [C. Mieszczak](https://framagit.org/tofmzk/informatique_git)
