# Le modèle relationnel

Les bases de données peuvent être construites selon différents modèles.

Le plus utilisé est le **modèle relationnel**, défini par [E.F. Codd](https://fr.wikipedia.org/wiki/Edgar_Frank_Codd) en 1970. Une base de donnée utilisant ce modèle est appelée **BDR** (**B**ase de **D**onnées **R**elationnelle).

## I. Vocabulaire : relation, attribut, domaine

On appelle **relation** un tableau à deux dimensions (un peu comme une table _csv_).  

Il s'agit donc d'un groupe d'informations relatives à un sujet :

- Les colonnes correspondent aux **attributs**.
- Les lignes sont appelées **n-uplets** (ou *tuples*, ou *enregistrements*). Chaque n-uplet doit être unique : il est interdit d'avoir, dans une relation, deux fois la même ligne.
- Chaque case est une **entrée** (ou *valeur*).

_Exemple :_ Voici une petite relation que nous appellerons _Classes_, qui précise les professeurs principaux des 4 terminales d'un lycée.

| classe | nom      | prenom |
|:------:|:--------:|:------:|
| TG1    | Pruvost  | Jean   |
| TG2    | Erneste  | Louise |
| TG3    | Duquesne | Jean   |
| TG4    | Szczezcz | Kasek  |

​    **1)** Quels sont les attributs ?

​    **2)** Donnez un n-uplet.

​    **3)** Donnez une entrée.

_Remarque :_ Le tableau de la relation a une particularité : on peut intervertir n'importe quelle ligne avec une autre ligne ou n'importe quelle colonne avec une autre colonne sans changer quoi que ce soit à la relation.

Chaque attribut prend ses valeurs dans un ensemble appelé **domaine**. 

Un domaine peut être :

* L'ensemble des nombres entiers ou une partie de l'ensemble. Il y a d'ailleurs différents type d'entiers : INT, SMALLINT ,LONGINT ...
* L'ensemble des nombres flottants ou une partie d'entre eux
* Les champs de type DATE ou TIME pour mesure du temps.
* les chaînes de caractères (TEXT) ou une structure particulière de chaîne de caractères (pour le nom d'un email par exemple)
* uniquement vrai ou faux, un booléen donc (BOOL).

Dans notre exemple :

* Le domaine de la classe est une chaîne de trois caractères commençant par `TG` et finissant par un numéro.
* Le domaine des noms et prénoms des professeurs principaux est TEXT.

​    **4)** Donnez le domaine des attributs suivants :

* l'âge d'une personne
* le site internet d'une entreprise
* la disponibilité d'un livre à la bibliothèque
* le prix du carburant

Pour le SGBD que nous utiliserons, `DB Browser for SQLite`, les domaines sont :

* INT pour les entiers
* TEXT pour les chaînes de caractères
* REAL pour les nombres flottants
* BLOB (Binary large objet) pour les gros objets en format binaire

## II. Contraintes

### 1. Clé primaire ou Primary Key (PK)

Dans une relation, une **clé primaire**  est un attribut , ou un groupe d'attribut, qui identifie de manière unique une ligne. Elle ne doit pas être `NULL` (vide), peut être composée d'une ou plusieurs colonnes (on peut parfois ajouter une colonne dédiée si besoin).

​    **5)** Dans la relation vue plus haut, quel attribut peut être une clé primaire à lui tout seul ?

​    **6)** Peut-on prendre une autre clé primaire ?

L'existence d'une clé primaire est une **contrainte d'unicité** qui permet de vérifier qu'il n'existe pas deux n-uplets identiques dans la base, ce qui est interdit comme nous l'avons déjà dit plus haut. Cette contrainte permet de s'assurer de la cohérence de la base de donnée.

### 2. D'autres relations

Une base de données n'est pas constituée d'une unique `relation` mais d'un ensemble de sujets connexes (c'est à dire relatifs à un même domaine) répartis en plusieurs tables.

En reprenant notre exemple, on peux ajouter à la base la `relation` suivante, que nous appellerons _Eleves_ , concernant un groupe d'élèves du même lycée.

| **prenom** | nom        | classe | date_naissance |
|:----------:|:----------:|:------:|:--------------:|
| Ernest     | Legrand    | TG2    | 12/02/2002     |
| Jean-yves  | Petit      | TG1    | 13/03/2001     |
| Sophie     | Allexandre | TG2    | 21/05/2002     |
| Jocelin    | Lamirand   | TG2    | 16/10/2001     |
| Eve        | Lawniczak  | TG1    | 23/06/2002     |
| Gaëlle     | Prudhomme  | TG3    | 12/04/2002     |
| Christophe | Scherer    | TG4    | 13/03/2002     |
| Corentin   | Sten       | TG3    | 21/06/2001     |
| Condette   | Sophie     | TG4    | 6/12/2001      |
| Emeline    | Vanherzek  | TG4    | 3/01/2002      |
| Louise     | Vandhamme  | TG1    | 24/12/2000     |
| Pierre     | Techec     | TG3    | 22/11/2002     |
| Abdel      | Arabah     | TG1    | 17/09/2002     |
| Matéo      | Diterlizi  | TG4    | 13/08/2002     |
| Pierre     | Perrin     | TG1    | 24/072002      |
| Léo        | Lawnisak   | TG2    | 5/11/2002      |
| Perrine    | Lannoy     | TG1    | 7/12/2001      |
| Youcef     | Ourcenar   | TG3    | 27/04/2002     |
| Ursula     | Andrews    | TG4    | 13/02/2002     |

​    **7)** Quels sont les attributs ?

​    **8)** Donnez un n-uplet.

​    **9)** Donnez une entrée.

​    **10)** Donnez un attribut ou un groupe d'attributs pouvant constituer une clé primaire pour cette relation.

Une clé primaire qui ne contiendrait que le nom et le prénom des élèves risque de poser un problème en cas de présence d'homonyme.  On ajoutera alors à la relation une colonne dédiée qui sera la clé primaire avec un simple numéro, un identifiant souvent appelé `id`  qui fera une clé primaire simple et fiable.

| id  | **prenom** | nom        | classe | date_naissance |
|:---:|:----------:|:----------:|:------:|:--------------:|
| 0   | Ernest     | Legrand    | TG2    | 12/02/2002     |
| 1   | Jean-yves  | Petit      | TG1    | 13/03/2001     |
| 2   | Sophie     | Allexandre | TG2    | 21/05/2002     |
| 3   | Jocelin    | Lamirand   | TG2    | 16/10/2001     |
| 4   | Eve        | Lawniczak  | TG1    | 23/06/2002     |
| 5   | Gaëlle     | Prudhomme  | TG3    | 12/04/2002     |
| 6   | Christophe | Scherer    | TG4    | 13/03/2002     |
| 7   | Corentin   | Sten       | TG3    | 21/06/2001     |
| 8   | Condette   | Sophie     | TG4    | 6/12/2001      |
| 9   | Emeline    | Vanherzek  | TG4    | 3/01/2002      |
| 10  | Louise     | Vandhamme  | TG1    | 24/12/2000     |
| 11  | Pierre     | Techec     | TG3    | 22/11/2002     |
| 12  | Abdel      | Arabah     | TG1    | 17/09/2002     |
| 13  | Matéo      | Diterlizi  | TG4    | 13/08/2002     |
| 14  | Pierre     | Perrin     | TG1    | 24/072002      |
| 15  | Léo        | Lawnisak   | TG2    | 5/11/2002      |
| 16  | Perrine    | Lannoy     | TG1    | 7/12/2001      |
| 17  | Youcef     | Ourcenar   | TG3    | 27/04/2002     |
| 18  | Ursula     | Andrews    | TG4    | 13/02/2002     |

La base peut contenir plus de deux relations bien entendu. Voici, par exemple,  une autre relation que nous appellerons _Profs_ :

| nom      | prenom | classe | matiere       | salle |
|:--------:|:------:|:------:|:-------------:|:-----:|
| Pruvost  | Jean   | TG1    | Anglais       | 101   |
| Erneste  | Louise | TG2    | Mathématiques | 209   |
| Duquesne | Jean   | TG3    | NSI           | 102   |
| Szczezcz | Kasek  | TG4    | Français      | 212   |

_Optimisation des données :_

* C'est le fait d'avoir plusieurs tables qui nous rend inutile de stipuler, pour chaque élève, quel est son professeur principal et dans quelle classe il se trouve, par exemple. **Définir plusieurs tables évite donc la redondance des valeurs et des colonnes et optimise ainsi la base de données.**
* On pourrait avoir envie d'indiquer, pour chaque classe, le nombre d'élèves. Cependant, cela ajouterait un attribut supplémentaire alors que ce dernier peut être calculé grâce à des fonctions du SGBD (fonctions d'agrégation que nous verrons plus tard). **On évitera toujours de déclarer des attributs supplémentaires s'ils peuvent être déduits d'autres attributs**

​    **11)** Quel attribut de la relation Profs peut être obtenue à partir d'une autre relation ?

Dans la suite, nous allons donc supprimer cet attribut !

​    **12)** Quel attribut (ou groupe d'attributs) pourrait alors faire office de clé primaire ?

### 3. Clé étrangère ou Foreign Key (FK)

Il faut maintenant créer un _lien_ entre nos relations.

Un attribut d'une relation peut prendre pour valeur les valeurs d'un attribut, ou d'un groupe d'attributs, qui constitue une clé primaire d'une autre relation. Un tel attribut est appelé **clé étrangère**. Une clé étrangère **référence**, c'est à dire _fait le lien avec_, une clé primaire d'une autre table : ses valeurs doivent pré-exister dans la table qu'elle référence.

Dans la relation _Eleves_, la clé `classe` est une clé étrangère qui référence la clé primaire `classe` de la relation _Classes_.

Une clé étrangère est une **contrainte d'intégrité** qui permet de garder une cohérente entre les deux relations :

* ses valeurs doivent pré-exister dans la relation à laquelle elle fait référence : on ne peut pas ajouter à la clé étrangère une valeur qui n'est pas présente parmi les valeurs de la clé primaire de l'autre relation. 
* On ne peut pas supprimer une valeur d'une clé primaire qui est référencée dans une clé étrangère

Dans notre exemple: 

* impossible de placer un élève en TG5
* impossible de supprimer la classe TG1 de la relation _Classes_.

Il nous faut déterminer un attribut (ou groupe d'attribut) qui constitue une clé étrangère de la relation _Classes_ vers la relation _Profs_. Pour rappel, la relation _Profs_ a été modifiée et se trouve être celle-ci :

| nom      | prenom | matiere       | salle |
|:--------:|:------:|:-------------:|:-----:|
| Pruvost  | Jean   | Anglais       | 101   |
| Erneste  | Louise | Mathématiques | 209   |
| Duquesne | Jean   | NSI           | 102   |
| Szczezcz | Kasek  | Français      | 212   |

​    **13)** A quelles possibles clés primaires de la relation _Profs_ la relation _Classes_ ne peut en aucun cas faire référence ?

​    **14)** Quelle clé primaire doit-on alors impérativement choisir pour la relation _Profs_ ?

​    **15)** Quelle est la clé étrangère de la relation _Classes_ qui référence cette clé primaire ?

### 4. Autres contraintes

Nous avons vu que la clé primaire imposait une **contrainte d'unicité** et la clé étrangère une **contrainte d'intégrité**. Il existe d'autre type de contraintes, par exemple :

* Une bonne pratique consiste à interdire qu'un attribut soit nul (NULL). Cette contrainte de non nullité se fait au moyen du mot clé `NOT NULL`.
* Il arrive parfois qu'un attribut, bien qu'il ne soit pas une clé primaire, doivent être unique. Il est possible de forcer cette unicité sans utiliser l'attribut comme clé primaire.
* On peut définir des **contraintes de domaine** en réduisant le domaine. Par exemple, un attribut entier peut voir sa plage réduite à un chiffre.

## III. Schéma relationnel

Un **schéma de relation** d'une relation en précise le nom, les attributs et leurs domaines, la clé primaire et, éventuellement , la clé étrangère.

Ainsi, le schéma de relation de _Classes_ est :

```
Classes (
    classe (PK) : "TG" suivi d'un entier,
    nom (FK Profs) : TEXT,
    prenom (FK Profs) : TEXT
)
```

​    **16)** Donnez le schéma de relation de _Eleves_.

​    **17)** Donnez le schéma de _Profs_.

On appelle **schéma relationnel** l'ensemble de tous les schémas de relation d'une base de donnée.

Graphiquement, on peut représenter le schéma relationnel de notre base ainsi :

![schéma relationnel](img/eleves_profs_classes.png)

## Pour aller plus loin

### 1. D'autres modèles de bases de données

Le modèle relationnel que nous étudierons cette année est le modèle le plus couramment utilisé, mais il n'est pas le seul. Il en existe même beaucoup d'autres.

#### Modèle de données *entité-association*

Ce type de modèle est le plus couramment utilisé pour la conception de modèles de données *logiques*. Selon ce type de modèle, une base de données est un lot d'entités et d'associations. Une *entité* est un sujet concret, un objet, une idée, pour laquelle il existe des informations. Un *attribut* est un renseignement concernant ce sujet — exemple le nom d'une personne. À chaque attribut correspond un *domaine* : un ensemble de valeurs possibles. Une *association* désigne un lien entre deux entités — par exemple, un élève et une école.

#### Modèle de données *objet*

Ce type de modèle est fondé sur la notion d'objet de la programmation orientée objet. Selon ce type de modèle, une base de données est un lot d'*objets* de différentes *classes*. Chaque objet possède des *propriétés* — des caractéristiques propres, et des *méthodes* qui sont des opérations en rapport avec l'objet. 

#### Modèle de données *hiérarchique*

Ce type de modèle de données a été créé dans les années 1960 ; c'est le plus ancien modèle de données. Selon ce type de modèle, les  informations sont groupées dans des *enregistrements*, chaque enregistrement comporte des *champs*. Les enregistrements sont reliés entre eux de manière hiérarchique : à  chaque enregistrement correspond un enregistrement parent.

#### Modèle de données *réseau*

Ce type de modèle de données est semblable au modèle hiérarchique. Les informations sont groupées dans des *enregistrements*, chaque enregistrement possède des *champs*. Les enregistrements sont reliés entre eux par des pointeurs.  Contrairement aux modèles hiérarchiques, l'organisation des liens n'est pas obligatoirement hiérarchique, ce qui rend ces modèles plus  polyvalents.

​    **18)** À quelle structure de données vue cette année s'apparente le modèle de données hiérarchique ?

​    **19)** À quelle structure de données vue cette année s'apparente le modèle de données réseau ?

### 2. Plus d'infos sur le modèle relationnel

* Vous pouvez regarder [cette vidéo](https://www.youtube.com/watch?v=iu8z5QtDQhY&t=316s).
* [Ce site](http://193.49.249.136:20180/~web/terminale/sgbd_vocabulaire.php#4.) propose un QCM d'entraînement.

_________________

Inspiré du travail de Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)