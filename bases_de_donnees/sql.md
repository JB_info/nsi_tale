# Requêtes SQL

Pour effectuer des traitements (création, modification, lecture, écriture...) dans une base de données, nous devons utiliser un SGBD.
Nous allons utiliser  [DB browser for SQLite](https://sqlite.org/) , un SGBD libre, gratuit et très répandu.
Il va nous permettre de créer, gérer et interroger une BDR.

Le langage utilisé par quasiment tous les SGBD est le **SQL** : **S**tructured **Q**uery **L**anguage (*langage de requête structuré*).
Ce langage a été inventé par Don Chamberlin et Ray Boyce (société IBM) en 1974.

SQL correspond au **paradigme de programmation déclaratif**. Cela signifie que le langage décrit le résultat voulu sans décrire la manière de l'obtenir (SQL s'occupe du "quoi" mais pas du "comment").

Les SGBD déterminent automatiquement la manière optimale d'effectuer les opérations nécessaires à l'obtention du résultat de la requête SQL (à l'inverse du Python, on ne s'occupe donc pas du coût de nos programmes).

Pour manipuler une BDR, nous allons donc effectuer des **requêtes SQL** par l'intermédiaire d'un SGBD.
Une requête est simplement une demande envoyée à la base de données. Il existe différents types de requêtes : création, insertion, suppression, mise à jour, interrogation.

## I. Créer la base

Nous allons commencer par créer une base de données contenant les relations _Classe_, _Profs_ et _Eleves_ vues dans [le cours précédent](modele_relationnel.md).

![rappel base](img/eleves_profs_classes.png)

    1) Commencez par lancer notre SGBD : *DB Browser for SQLite*.

    2) Cliquez sur `Nouvelle base de données` dans l'onglet du haut. Choisissez un nom pour votre base. Vous devriez maintenant voir la fenêtre suivante : ![](img/db1.png)

`CREATE TABLE` est une requête SQL pour créer une relation.

    3) Cliquez sur `Annuler` (oui, c'est un peu étrange, pour accéder à cette nouvelle base en fait, on annule la requête et on crée un base vide). Vous obtenez ceci : ![](img/db2.png)

    4) Cliquez sur l'onglet `Exécuter SQL` pour accéder  à l'éditeur de SQL. Grâce à la requête **CREATE TABLE**, on va créer une relation correspondant à la relation _Classes_ : tapez le code ci-dessous puis exécutez le en cliquant sur la flèche bleue à droite de l'icône _imprimante_.
    
![](img/db3.png)
    
Notez bien les domaines de chaque attribut : classe, nom et prenom sont des TEXT, c'est à dire des chaînes de caractères.

    5) Revenez dans l'onglet `Structure de la Base de Données` : nous avons bien une nouvelle relation _Classes_ : ![](img/db4.png)


## II. Insérer des données

Nous allons maintenant insérer les données ci-dessous dans la base :

| classe | nom      | prenom |
|:------:|:--------:|:------:|
| TG1    | Pruvost  | Jean   |
| TG2    | Erneste  | Louise |
| TG3    | Duquesne | Jean   |
| TG4    | Szczezcz | Kasek  |

    6) Pour cela, retournez dans l'onglet `Exécuter le SQL` et utilisez la requête **INSERT** (n'oubliez pas d'exécuter la requête en cliquant sur la flèche bleue) : ![](img/db5.png)

Notez la structure de la requête :

**INSERT INTO** _nom de la table_ _(tuple d'attributs)_

**VALUES**

(tuple correspondant au premier n-uplet)**,**

(tuple correspondant au deuième n-uplet)**,**

...

(tuple correspondant au dernier n-uplet)


*Remarque : Les majuscules aux noms des requêtes sont facultatives, elles sont seulement là pour une meilleure lecture du code. Ecrivez toujours vos requêtes de cette façon, cela fait partie des bonnes pratiques à respecter.*

    7) Allez maintenant dans l'onglet `Parcourir les données` pour visualiser cela : ![](img/db6.png)

## III. Supprimer des données

En cas d'erreur, on peut supprimer un n-uplet grâce à la requête **DELETE FROM** *nom_table* **WHERE** _condition_.

    8) Supprimez de la relation _Classes_ le n-uplet dont l'attribut `nom` prend la valeur 'Erneste' : ![](db7.png)

    9) Retournez dans `Parcourir les données` pour constater la suppression. 

    10) Bon, c'était juste pour l'exemple mais elle existe bel et bien, Louise... En utilisant la requête `INSERT` déjà vue plus haut, ajoutez le n-uplet qui lui correspond dans notre table _Classes_.


## IV. Modifier des données

Décidément, nous sommes maladroits... En réalité, le nom correct de Louise n'est pas 'Erneste' mais 'Ernestine' !

    11) Modifiez l'attribut _nom_ de notre relation _Classes_ en 'Ernestine' lorsque ce _nom_ est 'Erneste', en utilisant la requête **UPDATE** dont voici la structure :

**UPDATE** _nom_table_

**SET** *nom_attribut* = *nouvelle_valeur*

**WHERE** _condition_

    12) N'oubliez pas d'enregistrer vos modifications en passant par le menu _Fichier/Enregistrer les modifications_. 

## V. Lien avec les tables _csv_

    13) Créez la relation _Profs_ puis ajouter lui ces n_uplets :

| nom      | prenom | matiere       | salle |
|:--------:|:------:|:-------------:|:-----:|
| Pruvost  | Jean   | Anglais       | 101   |
| Erneste  | Louise | Mathématiques | 209   |
| Duquesne | Jean   | NSI           | 102   |
| Szczezcz | Kasek  | Français      | 212   |

    14) Modifiez le nom de Louise en 'Ernestine' dans la relation _Profs_.
    
Il y a 44 n-uplets à ajouter dans notre relation _Eleves_... ça va être long !
Heureusement, DB Browser est un SGBD conçu pour manipuler une BDR à la fois avec SQL et par des moyens dont vous avez plus l'habitude.

    15) Téléchargez le [fichier contenant la table _Eleves_](bases/Eleves.csv). Dans DB Browser, suivez ensuite le menu _Fichiers/Importer/Table depuis un fichier CSV_ et choisissez _Eleves.csv_. Cochez la case `Nom des Col. en première ligne` pour nommer correctement les colonnes et cliquez sur `OK`.

    16) Vérifiez que la table est créée correctement et que les données sont bien ajoutées.
    
N'oubliez pas d'enregistrer vos modifications !

## VI. Domaines, clés, contraintes

### 1. Domaines

    17) Vérifiez que le domaine de chaque attribut de chaque table est correct. Si ce n'est pas le cas, il faut cliquer droit sur le nom de la relation, puis "Modifier une table" et sélectionner le bon type dans le menu déroulant. Par exemple, il faut modifier le domaine de l'attribut *date_naissance* en NUMERIC.
    
### 2. Clés

Chaque relation doit avoir une clé primaire et certaines relations peuvent avoir une clé étrangère qui référence une clé primaire d'une autre table.

Nous avons vu que :

* la relation _Classes_ :
	* admet pour clé primaire l'attribut _classe_
	* admet pour clé étrangère _nom_ qui référence la clé primaire _nom_ de la relation _Profs_

* la relation _Eleves_ :
	* admet pour clé primaire l'attribut _id_
	* admet une clé étrangère _classe_ qui référence la clé primaire _classe_ de la relation _classes_

* la relation _Profs_ :
	* admet pour clé primaire l'attribut _nom_

Commençons par ajouter une clé primaire à la relation _Classes_.

    18) Allez dans l'onglet `Structure de la Base de Données` et cliquez droit sur la relation _Classes_ puis suivez le menu _Modifier une table_. Il suffit maintenant de cocher la case CP (clé primaire) de l'attribut _classe_. Regardez ce qui a été modifié dans la requête CREATE ?

![](img/db8.png)

    19) Cliquez sur OK, puis allez de nouveau dans l'onglet `Structure de la Base de Données` et créez les clés primaires des relations _Profs_ (_nom_) et _Eleves_ (_id_).

Nous allons maintenant ajouter les clés étrangères.

    20) Retournez dans "Modifier une table" de la relation _Eleves_. Sur la ligne de l'attribut _classe_, colonne "Clé étrangère", sélectionnez d'abord la relation référencée (ici _Classes_) puis l'attribut référencé (ici _classes_). Laissez le dernier champ vide (hors programme), cliquez sur Entrée pour valider. Regardez ce qui a été modifié dans la requête CREATE ?

    21) Validez en cliquant sur `OK` puis modifiez de même la table _Classes_ pour ajouter la clé étrangère _nom_ vers la relation _Profs_. On ne s'occupe pas du prenom (DB Browser ne permet pas les clés étrangères avec plusieurs attributs).
    
    22) Pourquoi faut-il créer les clés primaires avant les clés étrangères ?

N'oubliez pas d'enregistrer les modifications de temps en temps !!

### 3. Contraintes liées aux clés

Nous avons vu qu'une clé primaire imposait une contrainte d'unicité : impossible d'insérer dans une table un élément déjà présent !

    23) Testons cela en essayant d'insérer une classe déjà présente dans la relation _Classes_ :
```sql
INSERT INTO Profs (nom, prenom, matiere, salle)
VALUES
('Alain', 'Posteur', 'SNT', 404);

INSERT INTO classes (classe, nom, prenom)
VALUES
('TG1', 'Alain', 'Posteur')
```
    Quel est le message d'erreur renvoyé ? Pourquoi ?

    24) Quelle serait la contrainte d'unicité de la relation _Eleves_ ? 

Nous avons vu que la clé étrangère était une contrainte d'intégrité : impossible d'utiliser, dans la clé étrangère, une valeur non présente dans la clé primaire qu'elle référence.

    25) Testez cela en essayant d'insérer un éléve en 'TG5', classe qui n'existe pas.
```sql
INSERT INTO Eleves (id, nom, prenom, classe, date_naissance)
VALUES
(50, 'Peter', 'Parker', 'TG5', '02:05:04');
```
    Quel est le message d'erreur renvoyé ? Pourquoi ?
    
### 4. Autres contraintes

On décide qu'aucun attribut de peut-être nul : c'est la `contrainte de non nullité`. Pour cela, on retourne dans la fenêtre qui nous a permis de déclarer les clés primaires et étrangères et on coche les cases `NN` (NOT NULL ou NON NUL) qui correspondent à cette contrainte.

    26) Appliquez cette contrainte à tous les attributs de toutes les tables.

    27) Essayez maintenant d'insérer un n-uplet dont l'attribut a pour valeur `NULL`. Que se passe-t-il ?

L'attribut `salle` de la relation _Profs_ n'est pas utilisé comme clé primaire mais se doit d'être unique. Pour cela, on retourne dans la fenêtre qui nous a permis de déclarer les clés primaires et étrangères et on coche les cases `U` (*U*NIQUE).

    28) Rendez unique l'attribut `salle` de la relation _Profs_. Essayez ensuite d'insérer un prof en salle 101. Que se passe-t-il ? Pourquoi ?


## VII. Interrogation


### `SELECT`ion d'attribut dans une relation

    29) Testez la requête SQL suivante :
```sql
SELECT nom, prenom
FROM Classes
```

    30) Quelle requête permet de lister les salles utilisées par les professeurs principaux de la relation _Profs_ ?
    
    31) Quelle requête permet de lister les prénoms de la relation _Profs_ ?
    Combien y a-t-il de n-uplets dans le résultat ?

Nous avons imposé une contrainte d'unicité pour nos attributs qui se devaient d'être uniques et n'étaient pas des clés primaires. Ce n'est pas le cas, par exemple, de l'attribut _prenom_ de la relation _Profs_. Si on veut lister les prénoms différents (enlever les doublons), il faut utiliser l'opérateur `DISTINCT`

    32) Testez cette commande. Combien y a-t-il de n-uplets dans le résultat ?

```sql
SELECT DISTINCT prenom
FROM Profs
```

    33) Comment savoir si plusieurs élèves ont la même date de naissance ? Ecrivez les deux requêtes nécessaires.


### La clause `WHERE`

On peut sélectionner uniquement les n-uplets qui respectent la condition du `WHERE` :

```sql
SELECT noms_attributs_séparés_par_virgules
FROM nom_table
WHERE (attribut op_comp valeur) op_bool (attribut op_comp valeur)
```

La condition porte sur les valeurs des attributs :
* opérateurs de comparaison (op_comp) : `=`, `<>`, `>`, `>=`, `<`, `<=`
* opérateurs booléens (op_bool) : 
	- `AND` : combinaisons de conditions sur des attributs différentes
	- `OR` : plusieurs valeurs possibles pour un même attribut
    
Nous allons écrire quelques requêtes.

    34) Comment récupérer le nom des profs dont le prénom est 'Jean' ?
    
    35) Comment récupérer le nom du prof dont le prénom est 'Jean' et qui enseigne la NSI ?
    
    36) Comment récupérer le nom et prenom des élèves qui sont en TG3 ou en TG4 ?

*Remarque : Dans la plupart des SGBD, il existe de [nombreux domaines pour les dates](https://www.sqlfacile.com/apprendre_bases_de_donnees/les_types_sql_date). Mais SQLite n'en compte ... aucun. On ne va donc pas pouvoir classer par date.*


### `ORDER BY` : ordonner les résultats

La commande `ORDER BY` permet de changer l'ordre dans lequel s'affichent les résultats.

```sql
SELECT attribut
FROM nom_table
ORDER BY atrribut
```

Par défaut, le tri est dans l'ordre croissant. Ajouter `DESC` à la fin de la ligne du `ORDER BY` permet d'obtenir l'ordre décroissant. 

    37) Testez :
   
```sql
SELECT nom, prenom
FROM Eleves
ORDER BY nom
```
    puis :
```sql
SELECT nom, prenom
FROM Eleves
ORDER BY nom DESC
```

    38) Déterminez la liste des noms et prénoms les élèves de TG1 par ordre alphabétique croissant de l'attribut _nom_.

    39) Récupérez la liste des *différentes* classes des élèves par ordre décroissant.

### Recherche _floue_

Il est parfois utile de sélectionner des enregistrements dont un attribut commence, se termine ou contient un caractère particulier. On utilise pour cela l'opérateur `LIKE` .

    40) Testez :
    
```sql
SELECT nom, prenom 
FROM Eleves
WHERE nom LIKE 'D%'
```

    41) Quelle requête permettrait de trouver et afficher par ordre alphabétique les noms et prénoms des élèves nés en 2002 ?
    
    42) Comment récupérer les prénoms des élèves ayant un "é" dans leur prénom ?


### Fonctions d'agrégation

Les fonctions d'agrégation permettent d'effectuer des opérations sur un attribut. En voici la syntaxe :

```
SELECT FONCTION(attribut)
FROM nom_table;
```

Cela applique une des `FONCTION`s ci-dessous sur les valeurs d'un attribut :
- `COUNT` : compte le nombre de lignes sélectionnées.
- `MIN`, `MAX` : renvoie la valeur minimum ou maximum de la colonne, parmi les lignes sélectionnées
- `SUM`, `AVG` : calcule la somme ou la moyenne des valeurs **numériques** de la colonne, parmi les lignes sélectionnées

Ces fonctions sont notamment utilisées pour les études statistiques.

    43) Testez cette requête. Que fait-elle ?
    
```sql
SELECT COUNT(nom)
FROM eleves
WHERE date_naissance LIKE '02%'
```

    44) Quelle requête renverra le nombre d'élèves des classes TG1 ou TG2 ?

    45) Quelle requête renverra la date de naissance de l'élève le plus jeune de la classe ?

    46) Quelle requête renverra le nom, le prénom et la date de naissance de l'élève le plus vieux de la classe ?


### Jointure

On appelle `jointure` l'opération consistant à rapprocher, selon une condition, des n-uplets de deux (ou plusieurs) relations d'une base afin d'en créer une nouvelle qui contient l'ensemble des n-uplets obtenus en concaténant ceux des différentes relations vérifiant la condition de rapprochement.

Un exemple est toujours plus parlant :

On se rappelle qu'on a enlevé l'attribut _classe_ de la relation _Profs_ car on pouvait la retrouver en utilisant la relation _Classes_. Voici comment :
 
```sql
SELECT Profs.nom, Profs.prenom, Profs.matiere, Profs.salle, Classes.classe
FROM Profs
JOIN Classes
ON Profs.nom = Classes.nom
```

    47) Testez !

Notez que :
* Les attributs sont précédés du nom de la relation à laquelle ils appartiennent
* Cela permet de différencier deux attributs de deux relations différentes qui portent le même nom, comme ici les attributs _nom_ et _prenom_ communs aux relations _eleves_ et _profs_.
* On peut tout à fait joindre plus que deux tables. Il suffit d'ajouter un `JOIN` supplémentaire, et le `ON` qui indique l'attribut en commun.

Un `JOIN` est toujours suivi d'un `ON` servant à préciser l'attribut en commun des deux tables.

    48) Pour chacune des questions suivantes, indiquez la requête utilisée :

* Listez les noms et prénoms de tous les élèves combiné au nom de leur professeur principal.
* Listez les noms, prénoms et classes des élèves dont le professeur principal enseigne la NSI.
* Combien y aura t-il d'élèves en salle 209 ?
* Quels sont la date de naissance, le nom et le prénom de l'élève le plus âgé qui a choisi la spécialité Anglais ?


---

Inspiré du travail de Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BYNCSA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
