# Algorithmes sur les graphes


Nous allons voir les algorithmes qui existent sur les [graphes](../structures_de_donnees/graphes.md).

## I. Parcours en largeur

On travaille sur le graphe suivant :

![graphe](img/graphe.png)

**Q1)** *Rappels :*
* quels sont les sommets ?
* quels sont les voisins de A ?
* quels sont les voisins de C ?
* ce graphe est il orienté ?

Nous allons parcourir ce graphe en largeur d'abord.

**Pour parcourir un graphe en largeur d'abord, le principe est le suivant** :
* on explore les sommets du graphe en prenant d’abord tous les voisins d’un sommet
* ensuite on explore les voisins des voisins

Par exemple un parcours en largeur d'abord à partir du sommet F donne : *F - C - E - A - B - D*.

**Q2)** Faire un tableau des distances entre F et les autres sommets du graphe. Qu'y a-t-il de remarquable entre ce tableau et le parcours en largeur ?

**Q3)** Donnez le parcours en largeur d'abord pour le sommet D.

**Q4)** Donnez le parcours en largeur d'abord pour le sommet A.

Pour réaliser un parcours en largeur d'abord, on doit regarder tous les sommets les plus proches *avant* les autres donc il faut utiliser **une file**.

Voici l'algorithme en pseudo-code :

```
Entrées :
* G : un graphe
* depart : un sommet

Algorithme parcours_en_largeur :
    parcours = tableau vide
    f = file vide
    enfiler depart dans f
    
    tant que f n'est pas vide :
        s = defiler f
        si s n'est pas déjà dans parcours:
            ajouter s à la fin de parcours
            pour chaque voisin de s :
                enfiler le voisin dans f
                
Sortie :
* parcours
```

**Q5)** Appliquez cet algorithme au graphe précédent en prenant F comme sommet de départ. On notera sur une même ligne le sommet s actuellement parcouru et le contenu de la file f.


## II. Parcours en profondeur

On travaille toujours sur le graphe suivant :

![graphe](img/graphe.png)

Nous allons parcourir ce graphe en profondeur d'abord.

**Pour parcourir un graphe en profondeur d'abord, le principe est le suivant** :
* on explore les sommets du graphe en passant de sommet en sommet en suivant l’un des voisins
* lorsqu’il n’y a plus de sommets accessibles non encore parcourus, on revient au sommet précédent.

Par exemple un parcours en profondeur d'abord à partir du sommet F donne : *F - C - A - B - D - E*.

**Q6)** Donnez le parcours en profondeur d'abord pour le sommet A.

**Q7)** Donnez le parcours en profondeur d'abord pour le sommet D.

Pour réaliser un parcours en profondeur d'abord, on doit aller le plus loin possible sur la chemin *avant* de retourner voir les autres donc il faut utiliser **une pile**.

**Q8)** En remplaçant toutes les utilisations de la file par celle d'une pile dans l'algorithme en pseudo-code du parcours en largeur, donnez l'algorithme en pseudo-code du parcours en profondeur d'abord :

```
Entrées :
* G : un graphe
* depart : un sommet

Algorithme parcours_en_profondeur :
    parcours = tableau vide
    ...

Sortie :
* parcours
```

**Q9)** Appliquez cet algorithme au graphe précédent en prenant F comme sommet de départ. On notera sur une même ligne le sommet s actuellement parcouru et le contenu de la pile.


## III. Recherche d'un chemin

On travaille encore sur le graphe suivant :

![graphe](img/graphe.png)

**Q10)** *Rappels :*
* donnez un chemin allant de C à D.
* n'existe-t-il qu'un seul chemin ?
* quel est le chemin le plus court ? quel est donc la distance de C à D ?

Pour chercher un chemin allant d'un sommet `depart` à un sommet `arrivee`, il suffit de réaliser un parcours en **profondeur** en commençant au sommet `depart` et en s'arrêtant :
* soit quand on rencontre le sommet `arrivee` : c'est que le parcours nous a montré un chemin
* soit quand il n'y a plus de voisins disponibles : c'est qu'on est dans une impasse

**Q11)** En effectuant un parcours profondeur pour chercher un chemin de A à F par exemple, quel sommet serait une impasse ?

Voici l'algorithme en pseudo-code :

```
Entrées :
* G : un graphe
* depart : un sommet
* arrivee : un sommet

Algorithme recherche_chemin :
    parcours = tableau vide
    p = pile vide
    chemin = tableau contenant le départ
    empiler (depart, chemin) dans p
    
    tant que p n'est pas vide :
        s, chemin = depiler p
        si s est l'arrivee :
            renvoyer chemin
        si s n'est pas déjà dans parcours:
            ajouter s à la fin de parcours
            pour chaque voisin de s :
                enpiler (voisin, chemin + [voisin]) dans p

Sortie :
* None
```

**Q12)** Quelle information supplémentaire est ajoutée dans la pile pour chaque sommet par rapport au parcours profondeur ?

**Q13)** Quelles sont les deux seules lignes ajoutées par rapport au parcours profondeur ? À quoi servent-elles ?

**Q14)** Pourquoi le chemin renvoyé ne va-t-il pas toujours être le plus court chemin existant ?


## IV. Présence d'un cycle

On travaille à nouveau sur le graphe suivant :

![graphe](img/graphe.png)

**Q15)** *Rappels :*
* qu'est-ce qu'un cycle dans un graphe ?
* donnez un cycle partant du sommet A.
* quelle est la longueur minimale d'un cycle ?

Pour chercher un cycle dans un graphe, c'est exactement le même principe que pour chercher un chemin ! Seules différences :
* les sommets `depart` et `arrivee` sont les mêmes
* il faut vérifier que la longueur du chemin est au moins de 3 avant de le renvoyer

**Q16)** Reprenez l'algorithme en pseudo-code pour la recherche de chemin. La seule ligne qui change est `si s est l'arrivee :`. Par quoi faut-il la remplacer pour trouver un cycle ?

Pour vérifier s'il existe un cycle quelque part dans un graphe, il faut lancer l'algorithme de recherche d'un cycle pour tous les sommets du graphe :
* si une des recherches pour un sommet renvoie un chemin, c'est qu'il y a au moins un cycle dans le graphe
* si toutes les recherches renvoient None, c'est qu'il n'y a pas de cycle

**Q17)** Donnez l'algorithme en pseudo-code `existe_cycle` correspondant à cette idée.

## V. Implémentation

On travaille encore et toujours sur le graphe suivant :

![graphe](img/graphe.png)


**Q18)** Pour représenter un graphe en Python, on utilisera ici un dictionnaire qui contiendra la liste d'adjacence du graphe. Complétez la liste d'adjacence du graphe suivante :
```python
G = { "A" : ["B", "C"],
      "B" : ["A", "D", "E"],
      ...
    }
```

**Q19)** Pour un sommet `s` dans le graphe `G`, quelle instruction Python permet de récupérer ses voisins  ?

**Q20)** Implémentez en Python une fonction `parcours_largeur(G, depart)`.

**Q21)** Implémentez en Python une fonction `parcours_profondeur(G, depart)`.

**Q22)** Implémentez en Python une fonction `recherche_chemin(G, depart, arrivee)`.

**Q23)** Implémentez en Python une fonction `recherche_cycle(G, depart)`.

**Q24)** Implémentez en Python `existe_cycle(G)`.

## VI. Graphes orientés

On travaille sur le graphe orienté suivant :

![graphe orienté](img/graphe_oriente.png)

**Q25)** Donnez la liste d'adjacence du graphe en Python.

**Q26)** Testez tous vos algorithmes sur ce graphe. Obtenez-vous les résultats attendus ? Que peut-on donc dire des algorithmes sur les graphes ?


## Pour aller plus loin

**Q27)** Le chemin renvoyé par l'algorithme de recherche d'un chemin n'est pas toujours le plus court. Pour trouver le plus court chemin, il faut se baser sur un parcours en largeur d'abord et non en profondeur. Essayer d'implémenter une fonction `recherche_plus_court_chemin(G, depart, arrivee)` qui réalise cela.

**Q28)** Complétez la matrice d'adjacence (tableau de tableau en Python) du graphe non orienté :
```python
G_matrice = [[0,1,1,0,0,0],
             [1,0,0,1,1,0],
             ...
            ]
```


**Q29)** Écrivez une deuxième implémentation de vos algorithmes utilisant la représentation sous forme de matrice. 

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
