# Recherche textuelle

## I. Présentation

Ce qu'on appelle la **recherche textuelle** est la recherche d'un **motif** dans un **texte**. On parle également de "recherche de sous-chaîne". C'est ce qu'il se passe lorsqu'on appuie sur « Ctrl + f » dans un document.

Il existe plusieurs algorithmes de recherche textuelle. Nous verrons d'abord l'algorithme dit **naïf** puis l'algorithme de **Boyer-Moore** beaucoup plus efficace.

Pour expliquer les algorithmes, nous prendrons toujours le même exemple dans la suite :
* le texte sera NBCSNINSID
* nous chercherons le motif NSI

Le but d'un algorithme de recherche textuelle est de **renvoyer la position du premier caractère du motif dans le texte**. Si le motif n'est pas présent, -1 est généralement renvoyé.

1) Que doit renvoyer un algorithme de recherche textuelle pour le texte et le motif ci-dessus ?

*Remarque : il existe aussi des variantes consistant à :*
* renvoyer un booléen qui indique si le motif est présent ou non dans le texte
* compter le nombre d'occurrences du motif dans le texte
* ...


## II. Algorithme naïf

### 1. Principe

L'algorithme naïf parcourt le texte de gauche à droite de caractère en caractère. 

Pour chaque position, il regarde si le premier caractère du motif correspond, si c'est le cas, il regarde le second… :
* si tous les caractères du motif sont trouvé alors on a trouvé le motif
* sinon, il faut recommencer en décalant la recherche d'un caractère vers la droite.

Regardons sur notre exemple pour comprendre.

* Nous cherchons la correspondance en position i = 0 :
```
i:0123456789…
  NBCSNINSID
  NSI
j:012
```
Le caractère en j = 0 correspond (N = N), on regarde celui en j = 1, il ne correspond pas (B ≠ S).

* On décale alors la position de recherche en i = 1 :
```
i:0123456789…
  NBCSNINSID
   NSI
j: 012
```
Le caractère en j = 0 ne correspond pas (B ≠ N).

* On décale alors la position de recherche en i = 2 :
```
i:0123456789…
  NBCSNINSID
    NSI
j:  012
```

* ...
* On continue ainsi jusqu'à la position i = 6 :
```
i:0123456789…
  NBCSNINSID
        NSI
j:      012
```
Le caractère en j = 0 correspond (N = N), en j = 1 correspond (S = S) et celui en j = 2 correspond (I = I). On a trouvé le motif, on renvoie alors la position trouvée : 6

2) Donnez les étapes intermédiaires (entre i=2 et i=6).

3) Précisez combien de comparaisons ont été effectuées au total.

4) Expliquez la recherche dans le texte ABRACADABRA :
	* du motif ADAB
	* du motif CAR

5) Donnez le coût de cet algorithme (on appelle `n` la taille du texte et `m` la taille du motif) :
* dans le meilleur des cas
* dans le pire des cas

*Indice : Le meilleur des cas serait par exemple la recherche du motif ABC dans le texte DDDDDDDDDDDDDDDD...DDDDDD. Le pire des cas serait par exemple la recherche du motif DDDD...DDDA dans le texte DDDDDDDDDDDDDDDD...DDDDDD.*

### 2. Implémentation

6) Programmez une fonction `cherche(texte, motif)` qui applique cet algorithme. Vérifiez son bon fonctionnement sur les exemples précédents.

7) Modifiez légèrement la fonction précédente pour créer une fonction `cherche_cout(texte, motif)` qui renvoie un tuple contenant la position trouvée et le nombre de comparaisons effectuées. Vérifiez son bon fonctionnement sur le premier exemple (de la question 3).

## III. Algorithme de Boyer-Moore-Horspool

Nous verrons ici l'algorithme de Boyer-Moore-Horspool qui est une simplification (moins efficace) de l'algorithme de Boyer-Moore pour la recherche textuelle.

### 1. Principe

L'algorithme de Boyer-Moore-Horspool améliore l'algorithme naïf en effectuant un décalage intelligent en cas d'échec. 

* On ne se déplace pas que d'1 caractère vers la droite lorsque le motif n'a pas été trouvé. Le décalage va dépendre de la lettre qui a échouée. Ainsi, le texte est parcouru plus rapidement et la recherche est plus efficace.
* La deuxième idée est de faire la comparaison avec le motif *de la droite vers la gauche* de façon à tirer encore plus avantage du décalage.

Regardons cela sur notre exemple.

* Nous cherchons la correspondance en position i = 0 :
```
i:0123456789…
  NBCSNINSID
  NSI
j:012
```
On commence par regarder le motif en j = 2. Il n'y a pas de correspondance car C ≠ I. On décale alors le motif de 3 caractères vers la droite car la lettre C n'est pas dans le motif. Les positions intermédiaires ne peuvent donc pas réussir.

* On est alors en position i = 3 :
```
i:0123456789…
  NBCSNINSID
     NSI
j:   012
```
En j = 2 il y a correspondance (I = I), on regarde en j = 1, il n'y a pas correspondance (N ≠ S). Comme le N est dans le motif, on décale d'une lettre vers la droite pour faire correspondre les N.

* On est alors en position i = 4 :
```
i:0123456789…
  NBCSNINSID
      NSI
j:    012
```
En j = 2 le N et le I ne correspondent pas, on décale cette fois de 2 lettres pour faire correspondre les N.

* On se trouve en position i = 6 :
```
i:0123456789…
  NBCSNINSID
        NSI
j:      012
```
En j = 2 il y a correspondance (I = I) mais aussi en j = 1 (S = S) puis en j = 0 (N = N). Le motif est trouvé, on renvoie 6.

8) Précisez combien de comparaisons ont été effectuées au total.

9) Comparez le coût trouvé avec la recherche naïve (question 3).

10) Expliquez la recherche avec l'algorithme de Boyer-Moore dans le texte ABRACADABRA :
	* du motif ADAB
	* du motif CAR


### 2. Implémentation

Pour savoir quel décalage appliquer lorsqu'il y a un échec de correspondance, il faut effectuer un **pré-traitement sur le motif**. Ce pré-traitement est effectué une seule fois au début de la recherche. Il doit créer un dictionnaire contenant la position la plus à droite de chaque lettre composant le motif.

Par exemple pour le motif NSI le dictionnaire serait :
```python
aDroite = {"N": 0, "S": 1, "I": 2}
```
Et pour le motif ADAB :
```python
aDroite = {"A": 2, "D": 1, "B": 3}
```

11) Écrivez une fonction `calculADroite(motif)` qui crée et renvoie le dictionnaire aDroite pour un motif. Testez avec les motifs précédents.

Ensuite, nous avons besoin d'une fonction `droite(c)` qui renvoie la valeur contenue dans le dictionnaire aDroite pour le caractère c et qui renvoie -1 si le caractère n'est pas dans le dictionnaire.

12) Écrivez la fonction `droite(c)` décrite précédemment.

On appelle :
* `c` le *caractère du texte* qui ne correspond pas à la lettre du motif en position j ;
* `p` la position renvoyée pour c par la fonction `droite(c)`.

Le décalage à appliquer pourra se calculer avec les règles suivantes :
* on décale de `d = j - p` si d est positif
* on décale de `d = 1` sinon

13) Implémentez une fonction `decalage(j, c)` qui renvoie le décalage en appliquant les règles précédentes.

14) Implémentez l'algorithme de Boyer-Moore-Horspool dans une fonction `cherche_BoyerMoore(texte, motif)` en utilisant les fonctions précédentes.

15) Écrivez une fonction `cherche_BoyerMoore_cout(texte, motif)` qui renvoie un tuple contenant la position trouvée et le nombre de comparaisons effectuées. Vérifiez son bon fonctionnement sur l'exemple de la question 8.


## Pour aller plus loin

Pour comparer l'efficacité des algorithmes, il nous faudra utiliser un texte plus grand. Nous utiliserons donc le texte [Le Rouge et le noir de Stendhal](src/le_rouge_et_le_noir.txt). 

Pour commencer, nous pouvons tester l'algorithme de python avec `find` :
```python
import time

fichier = open('le_rouge_et_le_noir.txt', 'r', encoding='utf8')

texte = fichier.read()
motif = "Julien"

t = time.time()
print(texte.find(motif))
print(time.time() - t)

fichier.close()
```

16) Complétez le code ci-dessus pour calculer le temps de recherche de nos deux algorithmes. Lequel est le plus efficace ?


_________________

Inspiré du travail de Thomas Beline : [kxs](https://kxs.fr/cours/).

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
