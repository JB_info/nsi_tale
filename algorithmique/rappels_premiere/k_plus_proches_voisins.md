# Algorithmes des k plus proches voisins (knn)


## Introduction

L’algorithme des k plus proches voisins, appelé **kNN** pour *k* *n*earest *n*eighbors, est un algorithme de classification qui appartient à la famille des **algorithmes d’apprentissage automatique (machine learning)**.

Le terme de machine learning a été utilisé pour la première fois par l’informaticien américain Arthur Samuel en 1959. Les algorithmes d’apprentissage automatique ont connu un fort regain d’intérêt au début des années 2000 notamment grâce à la quantité de données disponibles sur internet : de nombreuses sociétés, commes les GAFAM (*G*oogle *A*pple *F*acebok *A*mazon *M*icrosoft) utilisent les données concernant leurs utilisateurs afin de "nourrir" des algorithmes de machine learning qui permettront à ces sociétés d’en savoir toujours plus sur nous et ainsi de mieux cerner nos "besoins" en termes de consommation ... au détriment de la vie privée.


## Principe

Tout d'abord, il nous faut avoir un ensemble de données d'apprentissage. Chaque donnée :

* appartient à une **classe**.
* est caractérisée par un certain nombre de **caractéristiques**. 


Nous avons un individu non identifié, c'est à dire de **classe inconnue**, dont on possède cependant les caractéristiques. L'objectif est de **deviner la classe de cet individu en regardant ses k plus proches voisins** (= les k voisins qui partagent le plus de caractéristiques similaires).


Prenons un exemple :

* Voici la représentation de deux classes : les carrés et les ronds. En représentant leurs caractéristiques graphiquement, on obtient la répartition ci-dessous. Un individu de classe inconnue possédant ces mêmes caractéristiques est représenté par le triangle.

![ronds & carrés](img/ronds_carres.jpg)


* En prenant `k = 3`, les 3 plus proches voisins sont 2 ronds et 1 carré : on décidera donc que l'inconnu est un rond.
* En prenant `k = 7`, les 7 plus proches voisins sont 4 ronds et 3 carrés : on décidera encore que l'inconnu est un rond.
* En prenant `k = 13`, les 13 plus proches voisins sont 6 ronds et 7 carrés : on décidera alors que l'inconnu est un carré.


_Remarques :_

* L'algorithme *k*NN pourrait être résumé par la phrase "dis moi qui sont tes voisins et je te dirai qui tu es". 
* Avec cet algorithme on fait un choix éclairé mais on ne peut pas être à 100% certain d'avoir fait le bon :
    * La définition de la _distance_ entre deux classes est particulièrement importante.
    * Le choix du _k_, c'est à dire du nombre de voisins à étudier, est également primordial.



## Etude florale

### Trois classes d'iris

Nous allons utilisé le fichier joint `iris.csv`  qui sera notre jeu de données d'apprentissage. On identifie trois espèces parmi ces fleurs, donc trois **classes**, d'iris :

* les iris setosa 
* les iris virginica
* les iris versicolor 

Les caractéristiques selon lesquelles les iris sont étudiées sont :

- la largeur _width_ des pétales.
- la longueur _length_ des pétales.

**Q1)** Téléchargez le fichier [iris.csv](iris.csv).

**Q2)** Ouvrez le fichier avec un éditeur de texte (le bloc-notes par exemple). Rappelez ce qu'est un fichier csv (format, descripteurs, séparateur).

**Q3)** Ouvrez le fichier avec un tableur (LibreOffice Calc par exemple). Que constatez-vous ?

### Lecture des données

**Q4)** Créez un fichier `iris.py`.

Tout d'abord, il faut importer la base dans un tableau en utilisant une fonction `lire_donnees`.

```python
def lire_donnees(nom_fichier_CSV):
    """
    Lit le fichier csv et construit le tableau des donnees.
    
    :param nom_fichier_CSV: (str)
    :return: (list de dict)
    """
    fichier = open(nom_fichier_CSV, "r")
    lignes = fichier.readlines()
    fichier.close()
    
    donnees = []
    for i in range(1, len(lignes)):
        ligne = lignes[i].split(";")
        
        donnee = {}
        donnee['petal_length'] = float(ligne[0])
        donnee['petal_width'] = float(ligne[1])
        donnee['species'] = ligne[2].rstrip("\n")
        
        donnees.append(donnee)

    return donnees
```

**Q5)** Copiez le code précédent dans votre fichier et exécutez le sur `iris.csv`. Qu'obtenez-vous ?

**Q6)** Commentez le code ligne par ligne pour expliquer ce qu'il fait.

### Affichage des données

Nous allons maintenant représenter graphiquement les fleurs étudiées en prenant :

* la longueur des pétales en abscisse
* la largeur des pétales en ordonnée
* une couleur différente selon la classe de l'individu.

La bibliothèque `pylab` permet de réaliser des représentations graphiques.  
La fonction `representer_donnees` nous permettra d'afficher nos données.

```python
import pylab

def representer_donnees(donnees):
    """
    Représente graphiquement les données.
    
    :param donnees: (list de dict)
    """
    couleur = {'setosa' : 'green',
               'virginica' : 'red',
               'versicolor' : 'blue'}

    pylab.title('Les iris')
    pylab.xlabel('longueur')
    pylab.ylabel('largeur')

    for donnee in donnees:
        pylab.scatter(donnee['petal_length'],
                      donnee['petal_width'],
                      color = couleur[donnee['species']],
                      linewidth = 2.0)

    pylab.text(1, 0.7, 'setosa', color = 'green')
    pylab.text(3, 1.5, 'virginica', color = 'red')
    pylab.text(6, 1.5, 'versicolor', color = 'blue')

    pylab.show()
```

**Q7)** Copiez le code précédent dans votre fichier et exécutez le sur les données récupérées de `iris.csv`. Qu'obtenez-vous ?

**Q8)** Commentez le code ligne par ligne pour expliquer ce qu'il fait.

Vous devriez obtenir cela :

![iris](img/iris.png)

On remarque que les trois classes sont plutôt bien différenciées.

### Ajout d'une iris de classe inconnue

En se promenant, on découvre trois iris dont on mesure les pétales :

* pour la première on obtient 1,3 cm sur 0,4cm
* pour la seconde on obtient 6,5cm sur 2,2 cm
* pour la troisième on obtient 2,5cm sur 0,75cm

**Q9)** Modifiez la fonction `representer_donnees` de façon à ajouter ces inconnues avec un point noir. Vous devriez obtenir ceci :

![iris + inconnues](img/iris_inconnues.png)


**Q10)** Quelle est la classe de la première fleur ?

**Q11)** Quelle est la classe de la deuxième fleur ?

**Q12)** Peut-on trancher aussi nettement pour la troisième ?

### Recherche des k plus proches voisins

Pour la dernière fleur, c'est donc plus délicat. On va devoir regarder à quelle distance elle se situe des autres. 

Pour calculer la distance entre deux points de coordonnées respectives $`(x_1, y_1)`$ et $`(x_2, y_2)`$, on applique la formule mathématique : $`d = \sqrt {(x_1 - x_2)^2 + (y_1 - y_2)^2}`$.

**Q13)** Réalisez la fonction `distance` de paramètres _x1_, _y1_, _x2_ et _y2_, quatre flottants, qui renvoie la distance entre les points de coordonnées respectives $`(x_1; y_1)`$ et $`(x_2; y_2)`$.  
*N'oubliez pas d'importer le module `math` pour pouvoir utiliser la fonction racine carrée `sqrt`.*  
*N'oubliez pas la documentation de votre fonction.*

Il va falloir maintenant regarder qui sont ses _k_ plus proches voisins. 

Commençons par simplement calculer les distances entre notre inconnue et les autres fleurs.

**Q14)** Créez une fonction `calculer_distances` de paramètres _x_inconnue_, _y_inconnue_ et _donnees_ qui renvoie une liste de tuple du genre [(distance1, classe1), ...] où chaque tuple précise la distance à la fleur de classe inconnue de coordonnées ($`x_{inconnue}, y_{inconnue})`$ et la classe de chaque fleur présente dans les données. Voilà la documentation :

```python
def calculer_distances(x_inconnue, y_inconnue, donnees):
    """
    Renvoie une liste de tuple du genre [(distance1, classe1), ...] où chaque tuple précise la classe de chaque fleur présente dans les données et la distance à la fleur de classe inconnue.

    :param x_inconnue: (float)
    :param y_inconnue: (float)
    :param donnees: (list de dict)
    :return: (list de tuple)
    """
```

Conservons maintenant les _k_ plus proches voisins

**Q15)** Ajoutez la fonction `renvoyer_k_plus_proches` ci-dessous à votre programme. Elle prend en paramètres _k_,  _x_inconnue_, _y_inconnue_ et _donnees_ qui renvoie la liste des classes des _k_ fleurs les plus proches de notre fleur inconnue de coordonnées $`(x_{inconnue}, y_{inconnue})`$.

```python
def renvoyer_k_plus_proches(k, x_inconnue, y_inconnue, donnees):
    """
    Renvoie la liste des tuples correspondant aux k fleurs les plus proches de notre fleur.
    
    :param k: (int)
    :param x_inconnue: (float)
    :param y_inconnue: (float)
    :param donnees: (list de dict)
    :return: (list)
    """
    distances = calculer_distances(x_inconnue, y_inconnue, donnees)
    distances.sort()
    
    k_plus_proches = []
    for i in range(k):
        k_plus_proches.append(distances[i][1])
        
    return k_plus_proches
```

**Q16)** Commentez ligne par ligne la fonction précédente pour expliquer ce qu'elle fait.

Il reste à faire la classification finale !

**Q17)** Créez une fonction `classifier` de paramètres _k_ ,  _x_inconnue_, _y_inconnue_ et _donnees_ qui renvoie la classe la plus représentée parmi les k plus proche fleurs  de notre fleur inconnue de coordonnées ($`x_{inconnue}, y_{inconnue})`$. Voilà le début :

```python
def classifier(k, x_inconnu, y_inconnu, donnees):
    """
    Renvoie la classe de l'inconnue.

    :param k: (int)
    :param x_inconnue: (float)
    :param y_inconnue: (float)
    :param donnees: (list de dict)
    :return: (str)
    """
    effectif = {'setosa' : 0, 'virginica' : 0, 'versicolor' : 0}
    k_plus_proches = renvoyer_k_plus_proches(k, x_inconnu, y_inconnu, donnees)
    
    # A COMPLETER :
    # - parcourir k_plus_proches pour remplir effectif
    # - renvoyer la clé de la valeur max de effectif
```

### Choisir _k_

Le choix de la valeur de _k_ est primordial :

* Comme on hésite souvent entre deux catégorie, on prend toujours un _k_ impair de façon à éviter les cas d'égalité. 

* Mais quelle valeur choisir ? 

Pour cela il faut tester l'efficacité de l'algorithme selon plusieurs valeurs de _k_ sur les données d'apprentissage.

**Q18)** Copiez le code suivant dans votre programme :

```python
def tester(k, donnees):
    """
    Teste la fonction "classifier" sur chaque fleur présente dans le jeu de données pour k voisins et renvoie le taux d'erreur.
     
    :param k: (int)
    :param donnees: (list de dict)
    :return: (float)
    """
    nbre_erreurs = 0
    nbre_donnees = 0
    for fleur in donnees :
        nbre_donnees += 1
        if classifier(k, fleur['petal_length'], fleur['petal_width'], donnees) != fleur['species'] :
            nbre_erreurs +=1
    return nbre_erreurs / nbre_donnees
```

**Q19)** En appelant la fonction tester avec les valeurs de k données, remplissez le tableau suivant :

| k             |  3   |  5   |  7   |  9   |
| ------------- | :--: | :--: | :--: | :--: |
| Taux d'erreur |      |      |      |      |

**Q20)** Quel _k_ devons nous donc utiliser ?

**Q21)** Identifiez donc la troisième fleur, pour laquelle on avait mesuré 2,5 cm sur 0,75 cm.


### Complexité

La `complexité` de l'algorithme `kNN` repose sur la complexité du tri de la table. D'après la documentation du langage Python, le tri de la table dans `sort` est en $`O(nlog_2(n))`$ (`n` est la taille de la table à trier). Dans ce cas on parle de complexité "quasi-linéaire".

## Autres exemples (sans programmer)


### Couleur d'un fruit

On cherche à prédire la couleur d’un fruit en fonction de sa largeur (`L`) et de sa hauteur (`H`).

On dispose des données d’apprentissage suivantes :

| Largeur | Hauteur | Couleur |
| :-----: | :-----: | :-----: |
|    2    |    6    |   red   |
|    5    |    6    | yellow  |
|    2    |    5    | orange  |
|    6    |    5    | purple  |
|    1    |    2    |   red   |
|    4    |    2    |  blue   |
|    2    |    1    | purple  |
|    6    |    1    |  green  |

* Ces données sont placées dans un repère (`L` en abscisse, `H` en ordonnée), indiquant pour chaque point sa couleur.

* L’objectif ici est d’étudier l’influence des voisins sur la propriété de couleur d’un fruit. Soit `U` le nouveau fruit de largeur `L = 1`, et de hauteur `H = 4`. 

1. Dessinez le repère.
2. Quelle est la couleur de `U` si l'on considère 1 voisin ?
3. Quelle est sa couleur si l'on considère 3 voisins ?
4. Plutôt que le vote majoritaire, on voudrait considérer le vote des voisins pondérés par la distance. Chaque voisin vote selon un poids `w` inversement proportionnel au carré de sa distance : $`w = \frac{1}{d^2}`$.  
   On prend 3 voisins, quelle est la couleur de `U` ?  
   Comparez vos résultats à ceux de la question 3. 



### Distance sur des données non numériques

Arrivé dans la cantina de la planète Tatooine, Han Solo décide de donner des indications à Luke pour qu’il ne provoque pas les extraterrestres belliqueux. Il repère quelques caractéristiques et vous demande de l’aider à fournir des éléments à Luke pour ne pas créer de problèmes et donc pouvoir définir un extraterrestre belliqueux.

| *Couleur* | *Taille* | *Poids* | *Yeux par pair ?* | *Belliqueux* |
|-----------|----------|---------|-------------------|--------------|
| jaune     | moyenne  | léger   | non               | non          |
| jaune     | grande   | moyen   | oui               | oui          |
| vert      | petite   | moyen   | oui               | oui          |
| jaune     | petite   | moyen   | non               | non          |
| rouge     | moyenne  | lourd   | non               | non          |
| vert      | grande   | lourd   | non               | oui          |
| vert      | moyenne  | lourd   | non               | oui          |
| jaune     | petite   | léger   | oui               | oui          |

1. Élaborez une distance pour pouvoir mettre en œuvre kNN sur cet exemple. 

_________________

Inspiré du travail de Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

Fichier `iris.csv` issu de ["The iris dataset"](https://gist.github.com/curran/a08a1080b88344b0c8a7)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
