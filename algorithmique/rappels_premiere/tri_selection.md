# Le tri par sélection 

## Algorithme

Sur une liste de *n* éléments (numérotés de 0 à *n-1 , attention un tableau de 5 valeurs (5 cases) sera numéroté de 0 à 4* et non de 1 à 5), le principe du tri par sélection est le suivant :

Pour _i_ allant de 0 à  _n_ - 2 :

- rechercher le plus petit élément de la partie de la liste allant de l'indice _i_ à l'indice _n_ - 1 en utilisant un comparateur adapté

- l'échanger avec l'élément d'indice _i_.

**Exemple : Considérons la liste  8, 5, 2, 6, 9, 3, 1, 4, 0, 7 de taille _n_ = 10.**

* Ses éléments son numérotées de 0 à _n_ - 1 = 9.
* Le comparateur est `<`

_Etape 1 : i = 0_

* le plus petit élément de la liste entre les indice _i_ = 0 et _n_ - 1 = 9 est 0, placé au rang 7.

* on échange donc le rang i = 0 avec le rang 7

	**On obtient  0, 8, 5, 2, 6, 9, 3, 1, 4, 8, 7**


_Etape 2 : i = 1_

* le plus petit élément de la liste entre les indice _i_ = 1 et _n_ - 1 = 9  est 1, placé au rang 7.

* on échange donc le rang 1 avec le rang 7.

	**On obtient  0, 1, 5, 2, 6, 9, 3, 8, 4, 8, 7**



_Etape 3 : i_ = 2

* le plus petit élément de la liste entre les indice _i_ = 2 et _n_ - 1 = 9 est 2, placé au rang 3.

* on échange donc le rang i=2 avec le rang 3 :

	**On obtient  0, 1, 2, 5, 6, 9, 3, 8, 4, 8, 7** etc .........

	

Voici une petite animation qui résume tout cela :



![animation - source wikipédia - CC by SA](https://upload.wikimedia.org/wikipedia/commons/9/94/Selection-Sort-Animation.gif)



## Terminaison et correction

* La **terminaison** de l'algorithme est le fait que l'algorithme se termine bien et, donc, ne boucle sans fin. Ce qui nous permet d'être  certain de la **terminaison**  est appelé **variant de boucle**. Il s'agit d'un nombre entier qui décroit strictement et garantit la sortie de la boucle lorsqu'il atteint 0. Ici le **variant de boucle** est la longueur de la liste qu'il reste à trier : cette longueur est un nombre entier qui diminue de 1 à chaque tour et atteindra donc fatalement 0 en sonnant la fin du tri en garantissant la **terminaison** de l'algorithme.


* La **correction** de cette algorithme est l'assurance qu'il réalise bien ce pourquoi il est fait. Ce qui garantit cette correction est appelée **invariant de boucle** : c'est une propriété qui doit rester vraie du début à la fin de l'algorithme. Ici, notre **invariant de boucle** est le fait que la partie de la liste comprise entre l'élément 0 et l'élément _i_ est toujours correctement triée.



## Coût de l'algorithme

Trier une liste de n éléments nécessite donc n - 1 étape. Mais, chaque étape nécessite un certain nombre de comparaisons. C'est ce nombre qui détermine l'efficacité du tri : plus il est faible, plus l'algorithme est efficace.



**Reprenons notre exemple :  la liste 8, 5, 2, 6, 9, 3, 1, 4, 0, 7  de taille _n_ =9.**

* Lors de l'étape 1, on recherche le plus petit élément entre les indices 0 et 9 : on réalise donc 9 comparaisons.

* Lors de l'étape 2, on recherche le plus petit élément entre les indices 1 et 9 : on réalise 8 comparaisons.

* Lors de l'étape 3, on recherche le plus petit élément entre les indices 2 et 9 : on réalise 7 comparaisons

* [...]

* Lors de l'étape 8, on on recherche le plus petit élément entre les indices 7 et 9 : on réalise 2 comparaisons

* Lors de l'étape 9, on on recherche le plus petit élément entre les indices 8 et 9 : on réalise 1 comparaisons

	

	**Le nombre de comparaisons est donc $`9 + 8 + 7 + 6 + 5 + 4 + 3 + 2 + 1 = \frac {9 * 10} {2} = 45`$ comparaisons**

	



> Lors du tri par sélection d'une liste de n éléments :
>
> * le premier élément sera comparé à _n_ - 1 éléments
> * le second élément sera comparé à _n_ - 2 éléments
> * le troisième élément sera comparé à _n_ - 3 éléments
> * ..
> * le _n_-1 et dernier élément sera comparé à _n_ - (_n_ - 1) = 1 élément 
>
> **Dans tous les cas**, le nombre de comparaisons est donc  :
>
> $` 1 + 2 + 3 + 4 + ... + (n - 3 ) + ( n - 2) + (n - 1 ) = \frac {n ( n-1)}{2}`$ 
>
> L'ordre de grandeur du nombre de comparaison est _n²_ : on dit que sa **complexité est quadratique** ou que sa complexité est **_O(n²)_**.



## Implémentation en Python

Copiez le code ci-dessous et complétez le ! 

```python
def minimum(liste):
    '''
    renvoie l'indice de l'élement minimum de la liste
    :param liste: (list)
    :return: (int)

    >>> minimum([4, 5, 7, 8, 0, 2])
    4
    >>> minimum([-4, -5, -7, -8, -2])
    3
    >>> minimum([4, 5, -7, 8, 0, 2])
    2
    '''
	# COMPLETER ICI

def echanger(liste, indice1, indice2):
    '''
    echange les élements d'indices indice1 et indice2 de la liste
    :param liste: (list)
    :param indice1: (int)
    :param indice2: (int)
	
    >>> liste = [2, 3, 4, 5, 6]
    >>> echanger(liste, 0, 3)
    >>> liste
    [5, 3, 4, 2, 6]
    '''
	# COMPLETER ICI

def tri_selection (liste):
    '''
    modifie la liste en la triant
    :param liste: (list)

    >>> liste = [8, 5, 2, 6, 9, 3, 1, 4, 0, 7]
    >>> tri_selection(liste)
    >>> liste
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    '''
	# COMPLETER ICI


###### doctest
if __name__ == "__main__":
    import doctest
    doctest.testmod()
```


_________________

Inspiré du travail de Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
