# Diviser pour régner


**Diviser pour régner** est une technique algorithmique qui permet de résoudre des problèmes de manière **plus efficace** (donc avec un meilleur coût).

Le principe de la méthode *diviser pour régner* se découpe en 3 étapes :

1. **diviser** = découper le problème en plusieurs sous-problèmes plus petits
2. **régner** = résoudre les sous-problèmes (souvent avec de la récursivité)
3. **combiner** = trouver une solution au problème initial à partir des solutions des sous-problèmes


# I. Tri fusion

## 1. Principe

Le tri fusion est encore un algorithme de tri d'un tableau. Celui-ci utilise la méthode diviser pour régner.

Voici comment il fonctionne :

1. **diviser** = si le tableau a au moins deux éléments, on le sépare en 2 demi-tableaux
2. **régner** = on applique le tri fusion récursivement sur les 2 demi-tableaux
3. **combiner** = on fusionne nos 2 demi-tableaux maintenant triés en 1 seul tableau

Un exemple en image :

![tri fusion](img/fusion.png)

**Q1)** Expliquez les différentes étapes du tri fusion du tableau ci-dessus.

**Q2)** Illustrez le tri fusion sur le tableau `[9, 1, 5, 4, 7, 2, 8, 3, 6]`.

## 2. Fusion

L'étape la plus compliquée du tri fusion est *combiner*. Pour fusionner deux tableaux triés en 1, il faut :

* si l'un des deux tableaux est vide, c'est simple : on renvoie l'autre
* sinon on compare le premier élément des deux tableaux pour trouver le plus petit
* on retire cet élément du tableau
* on fusionne récursivement le reste des deux tableaux
* on renvoie un tableau constitué de l'élément plus petit + fusion du reste des 2 tableaux

Ainsi pour fusionner `[3, 4]` et `[2, 8, 9]` :

* on compare 3 et 2, le plus petit est 2
* on retire 2 du tableau de droite
* on fusionne `[3, 4]` et `[8, 9]` :
	* on compare 3 et 8, le plus petit est 3
	* on retire 3 du tableau de gauche
	* on fusionne `[4]` et `[8, 9]` :
		* on compare 4 et 8, le plus petit est 4
		* on retire 4 du tableau de gauche
		* on fusionne `[]` et `[8, 9]` :
			* `[]` est vide donc on renvoie simplement `[8, 9]`
		* on renvoie `[4]` + `[8, 9]`
	* on renvoie `[3]` + `[4, 8, 9]`
* on renvoie `[2]` + `[3, 4, 8, 9]`

**Q3)** Expliquez comment se déroule la fusion des deux tableaux triés `[2, 5, 6, 7]` et `[1, 3, 4]`.

**Q4)** Complétez la fonction suivante, qui réalise la fusion des tableaux `tgauche` et `tdroit` :

```python
def fusion(tgauche, tdroit):
    if tgauche == []:
        return ???
    
    elif tdroit == []:
        return ???
    
    else:
        if tgauche[0] < tdroit[0]:
            mini = tgauche.pop(0)
        else:
            mini = ???
            
        fusion_du_reste = ???
        return [mini] + fusion_du_reste
```

**Q5)** Ajoutez la documentation et au moins 1 doctest.


## 3. Tri

**Q6)** Implémentez maintenant la fonction `tri_fusion` dont voici le pseudo-code :

```python
tri_fusion(tableau):
    si le tableau contient 1 élément ou moins :
        renvoyer le tableau
    sinon :
        tgauche = demi-tableau gauche
        tdroit = demi-tableau droit
        tgauche = tri_fusion(tgauche)
        tdroit = tri_fusion(tdroit)
        renvoyer la fusion de tgauche et tdroit
```

**Q7)** Ajoutez la documentation et au moins 1 doctest.

**Q8)** Identifiez dans l'algorithme ce qui correspond à l'étape "diviser", à l'étape "régner" et à l'étape "combiner".

## 4. Coût

Le **coût** en temps du tri fusion est $`O(n log_2(n))`$.

**Q9)** Expliquez d'où peut venir un tel coût.

**Q10)** Ce tri est-il plus efficace que le tri par sélection ? par insertion ?

**Q11)** Quel est par contre son inconvénient au niveau du coût en mémoire ?

# II. Étude du coût

L'objectif est d'écrire une fonction `min_et_max(T)` renvoyant le couple (minimum, maximum) du tableau T.

Un algorithme du type « diviser pour régner » permet de déterminer ce couple en exploitant le fait qu’une seule comparaison suffit pour obtenir à la fois le minimum et le maximum d'un tableau de taille 2.

Voici cet algorithme en pseudo-code:

```
ENTREE
T : tableau
ALGO
si T ne contient qu'un élément:
    renvoyer (T[0], T[0])
si T contient deux éléments:
    renvoyer le couple (minimum, maximum)
sinon:
    tgauche = demi-tableau gauche de T
    tdroit = demi-tableau droit de T
    calculer récursivement le couple (min, max) des deux tableaux
    fusionner les résultats précédents
    renvoyer le couple (minimum, maximum)
```

**Q12)** Écrire la fonction min_et_max(T) en Python *sans utiliser les fonctions min et max de Python*.

**Q13)** La tester sur le tableau T = [1, 6, -5, 8, 14, -2, -7, 21] en lançant la commande min_et_max(T).

**Q14)** Combien cette fonction utilise de comparaisons pour un tableau de 4 éléments ?

**Q15)** Combien un algorithme classique utilise de comparaisons pour un tableau de 4 éléments ?

**Q16)** Répondez aux deux questions précédentes pour des tableaux de 8, 16 et 32 éléments. Que pouvez-vous conclure sur le coût de cet algorithme ?

# III. Rotation d'image

Il est possible d'appliquer la méthode *diviser pour régner* à la rotation d'une image carrée. Nous ferons tourner l'image de 90° dans le sens trigonométrique. La méthode est illustrée sur le schéma ci-dessous :

![rotation d'image](img/rotation.png)

1. **diviser** = on coupe l'image en quatre quadrants
2. **régner** = on effectue une rotation récursive de chacun des quadrants
3. **combiner** = on applique une permutation circulaire des quadrants

**Q17)** Téléchargez cette image de la Joconde : [Joconde](img/joconde.png). Elle est carrée avec une taille égale à une puissance de 2.

Nous utiliserons la bibliothèque [PIL](https://he-arc.github.io/livre-python/pillow/index.html) que vous connaissez sans doute déjà. Les deux méthodes importantes sont :
* img.getpixel((x, y)) qui renvoie le pixel à la position (x, y)
* img.putpixel((x, y), pixel) qui colorie le pixel à la position (x, y)

**Q18)** Complétez le code suivant, qui utilise diviser pour régner afin d'effectuer une rotation de l'image de la Joconde :

```python
from PIL import Image

def tourne(img):
    """
    Lance la fonction récursive sur l'image originale.

    :param img: (Image)
    
    Fonction complète. NE PAS Y TOUCHER.
    """
    tourne_recursif(img, (0,0), img.size[0])

def echange_pixels(img, pixel1, pixel2):
    """
    Échange la couleur des deux pixels pixel_a et pixel_b.
    
    :param img: (Image)
    :param pixel1: (tuple de coordonnées)
    :param pixel2: (tuple de coordonnées)
    """
    # À compléter !
    p1 = img.getpixel(???)
    p2 = img.getpixel(???)
    img.putpixel(???, p2)
    img.putpixel(???, p1)



def echange_quadrants(img, coinHautGauche1, coinHautGauche2, n):
    """
    Échange les pixels entre les deux quadrants de taille n définis par leur coin haut gauche.

    :param img: (Image)
    :param coinHautGauche1: (tuple) coordonnées du coin supérieur gauche du quadrant a
    :param coinHautGauche2: (tuple) coordonnées du coin supérieur gauche du quadrant b
    :param n: (int) taille des deux quadrants
    """
    # À compléter !
    x1, y1 = coinHautGauche1
    x2, y2 = coinHautGauche2
    
    for i in range(n):
        for j in range(n):
            pixel1 = (???, ???)
            pixel2 = (???, ???)
            echange_pixels(img, pixel1, pixel2)
            

def tourne_recursif(img, coinHautGauche, n):
	"""
	Applique une rotation du quadrant de l'image défini par son coin supérieur gauche et sa taille n.

	:param img: (Image)
	:param coinHautGauche: (tuple) coordonnées du coin supérieur gauche du quadrant
	:param n: taille du quadrant à faire tourner
	"""
	# À compléter !
	if n > 1:
        # on coupe la taille en 2
		n = n // 2
		
		# on récupère les coordonnées des coinHautGauche des 4 cadrants
		x, y = coinHautGauche
		coinHautGauche_a = (x, y)
		coinHautGauche_b = (x, y + n)
		coinHautGauche_c = ???
		coinHautGauche_d = ???
		
		# tourne récursivement les 4 quadrants :
		tourne_recursif(img, coinHautGauche_a, n)
		tourne_recursif(img, coinHautGauche_b, n)
		tourne_recursif(img, ???, n)
		tourne_recursif(img, ???, n)
		
		# permute les 4 quadrants
		echange_quadrants(img, coinHautGauche_a, ???, n)
		echange_quadrants(img, ???, ???, n)
		echange_quadrants(img, ???, ???, n)
```


# Pour aller plus loin : Rappels sur la recherche dichotomique

Vous avez étudié en Première la recherche dichotomique, qui consiste à chercher si un élément est présent ou non dans un tableau trié.

**Q19)** Rappelez le principe de la recherche dichotomique.

**Q20)** Expliquez en quoi elle applique la méthode diviser pour régner. En particulier, précisez ce qui correspond à l'étape "diviser", à l'étape "régner" et à l'étape "combiner".

**Q21)** Implémentez la recherche dichotomique en Python :
```python
def recherche_dichotomique(element, tableau):
    """
    renvoie True si element est dans le tableau et False sinon
    :param element: (any)
    :param tableau: (list)
    :return: (bool)
    """
```

**Q22)** Ajoutez au moins 2 doctests à votre fonction.

**Q23)** Donnez le coût de la recherche dichotomique et expliquez en quoi il est meilleur qu'une recherche classique.

_________________

Inspiré du travail de Thomas Beline : [kxs](https://kxs.fr/cours/).

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
