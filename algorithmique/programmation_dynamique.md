# Programmation dynamique

La programmation dynamique est une méthode algorithmique pour résoudre des problèmes d’optimisation. Cette méthode a été introduite au début des années 1950 par Richard Bellman.

## I. Principe

### 1. Définitions

**La programmation dynamique consiste à résoudre un problème en le décomposant en sous-problèmes de plus petites tailles, en stockant les résultats intermédiaires pour ne pas avoir à refaire les calculs.**

La programmation dynamique peut se faire de 2 manières différentes :

* la technique **Top-down** (du haut vers le bas)
* la technique **Bottom-up** (du bas vers le haut)

Dans les deux cas, on cherche à trouver une solution pour un nombre `n` et on va donc devoir calculer les solutions de tous les nombres plus petits que `n` :

* pour **Top-down** on calcule `n-1`, puis `n-2`, puis ... jusque `0`.
* pour **Bottom-up** on calcule `0`, puis `1`, puis ... jusque `n-1`

Tous les résultats de ces calculs vont être **stockés** quelque part,  généralement dans un **tableau**.

**Q1)** Si on cherche la solution d'un problème pour `n = 5`, combien de résultats vont devoir être stockés dans un tableau ? Avec Top-down, dans quel ordre seront-ils stockés ? Et avec Bottom-up ?

### 2. La suite de Fibonacci (au programme de l'épreuve pratique !!!)

Pour rappel, la suite de Fibonacci est définie comme ceci :
$`\begin{cases} F(0) = 0 \\\ F(1) = 1 \\\ F(n) = F(n-1) + F(n-2) \end{cases}`$

**Q2)** Calculez F(2), F(3), F(4).

#### L'algo classique

La suite de Fibonacci se programme facilement avec la récursivité :

```python
def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)
```

**Q3)** Testez la fonction avec n = 10, n = 30 et n = 50. Que remarquez-vous ?

Pour comprendre pourquoi ce programme est long, on peut dessiner un arbre qui résume tous les appels récursifs effectués :

![fibo arbre](img/fibonacci.jpg)

On s'aperçoit tout de suite qu'il y a de nombreux calculs qui sont répétés !

![fibo répétitions](img/fibonacci_redondance.jpg)

**Q4)** Sans tout dessiner, compléter l'arbre précédent pour un calcul de `fibonacci(7)`.

#### Top-down

Pour parer à ce problème, on va donc stocker tous les calculs déjà effectués dans un tableau `memoire`.

Voici donc le programme avec la méthode Top-down :

```python
def fibonacci_dynamique(n):
    memoire = [None] * (n+1)                # on initialise une mémoire vide contenant n+1 cases
    fibonacci_memoire_top_down(n, memoire)  # on appelle la fonction qui va remplir la mémoire
    return memoire[n]                       # on renvoie la valeur de la case n de la mémoire

def fibonacci_memoire_top_down(n, memoire):
    if n == 0:
        memoire[0] = 0
    elif n == 1:
        memoire[1] = 1
    elif memoire[n] == None:
        fibonacci_memoire_top_down(n-1, memoire)
        fibonacci_memoire_top_down(n-2, memoire)
        memoire[n] = memoire[n-1] + memoire[n-2]
```

**Q5)** Testez la fonction fibonacci_dynamique avec n = 10, n = 30 et n = 50. Que remarquez-vous maintenant ?

**Q6)** Dans la fonction fibonacci_dynamique, pourquoi doit-on créer une mémoire de `n+1` cases ?

**Q7)** Si une valeur `n` a déjà été calculée, sa valeur est donc  présente dans la mémoire à la place de None. Que va alors faire la fonction fibonacci_memoire_top_down ? Y a-t-il encore des calculs répétés ?

**Q8)** À quoi servent les lignes `fibonacci_memoire_top_down(n-1, memoire)` et `fibonacci_memoire_top_down(n-2, memoire)` ?

**Q9)** Comparez le code de la fonction fibonacci_memoire_top_down avec celui de la version récursive donnée plus haut. Que remarquez-vous ?

**Q10)** Justifiez que cette fonction utilise bien une technique Top-down.

#### Bottom-up

Il est également possible d'utiliser une méthode Bottom-up pour remplir la mémoire.

Voici donc le programme avec la méthode Bottom-up :

```python
def fibonacci_dynamique(n):
    memoire = [None] * (n+1)
    fibonacci_memoire_bottom_up(n, memoire)
    return memoire[n]

def fibonacci_memoire_bottom_up(n, memoire):
    memoire[0] = 0
    memoire[1] = 1
    for i in range(2, n+1):
        memoire[i] = memoire[i-1] + memoire[i-2]
```

**Q11)** Testez la fonction fibonacci_dynamique avec n = 10, n = 30 et n = 50. Que remarquez-vous ?

**Q12)** Commentez chaque ligne de la fonction fibonacci_dynamique.

**Q13)** La fonction fibonacci_memoire_bottom_up est-elle récursive ? Comparez avec la méthode Top-down.

**Q14)** Pourquoi faut-il indiquer `2` et `n+1` dans le range de la fonction fibonacci_memoire_bottom_up ?

**Q15)** Justifiez que cette fonction utilise bien une technique Bottom-up.

**Q16)** Il est clair que la programmation dynamique est plus rapide que la récursivité classique. Mais quel est l'inconvénient au niveau du coût en mémoire ?

## II. Entraînement : la pyramide de nombre

Voici une pyramide de nombres. En partant du sommet, et en se dirigeant vers le bas à chaque étape, on doit réussir à maximiser le total des nombres traversés. Sur l'image d'exemple, ce maximum est 23 (le chemin est indiqué en rouge).

![pyramide](img/pyramide.png)

Nous allons utiliser le tableau suivant pour représenter la pyramide ci-dessus :
```python
pyramide = [[3], [7,4], [2,4,6], [8,5,9,3]]
```

La hauteur d'une pyramide est le nombre d'étages. Dans l'exemple ci-dessus, la pyramide a pour hauteur 4. 

**Q17)** Écrire une fonction `hauteur(pyramide)` qui renvoie la hauteur d'une pyramide.

Pour trouver le chemin maximum, le principe est le suivant :

* si la hauteur est 1 : le chemin maximum est l'unique valeur au sommet
* sinon : le chemin maximum est (le sommet) + (le max entre les chemins maximums des sous pyramides gauches et droites)

![pyramide recursif](img/pyramide_rec.png)

Voici donc le code :

```python
def sous_pyramide_gauche(pyramide):
    gauche = []
    for etage in range(1, hauteur(pyramide)):
        gauche.append(pyramide[etage][:-1])
    return gauche

def sous_pyramide_droite(pyramide):
    droite = []
    for etage in range(1, hauteur(pyramide)):
        droite.append(pyramide[etage][1:])
    return droite

def chemin_max(pyramide):
    if hauteur(pyramide) == 1:
        return pyramide[0][0]
    else:
        return pyramide[0][0] + max(chemin_max(sous_pyramide_gauche(pyramide)), chemin_max(sous_pyramide_droite(pyramide)))
```

**Q18)** Testez la fonction sur la pyramide [[3], [7,4], [2,4,6], [8,5,9,3]] et vérifiez que vous retrouvez bien 23.

**Q19)** Quel calcul est répété pour la pyramide précédente ?

**Q20)** Expliquez pourquoi il y aura de nombreux calculs répétés sur la pyramide de hauteur 10 suivante : [[8], [8, 2], [1, 8, 6], [7, 8, 2, 5], [3, 4, 3, 6, 4], [6, 9, 1, 4, 6, 5], [5, 3, 2, 2, 7, 3, 2], [6, 3, 7, 1, 1, 5, 3, 2], [9, 5, 3, 2, 8, 4, 2, 4, 7], [6, 3, 4, 9, 4, 2, 9, 1, 7, 8]]

**Q21)** *SANS PROGRAMMER*, expliquez comment il faudrait faire pour conserver les calculs en mémoire avec la méthode Top-down.

**Q22)** *SANS PROGRAMMER*, expliquez comment il faudrait faire pour conserver les calculs en mémoire avec la méthode Bottom-up.

**Q23)** Donnez les avantages / inconvénients en terme de coût en temps / coût en mémoire de la programmation dynamique.


## III. Retour sur le problème du rendu de monnaie

Nous avons déjà résolu le problème de rendu de monnaie avec un algorithme glouton.

*Rappel : Vous disposez de pièces de plusieurs valeurs : `pieces = [1, 2, 5, 10, 20, 50, 100, 200]`. Vous devez rendre une certaine somme `somme` à un client. Le but est de rendre le moins de pièces possibles.*

L'idée de l'algorithme est que pour rendre une certaine somme, il faudra rendre juste 1 pièce `p` supplémentaire par rapport aux sommes précédentes `somme - p`.

*Exemple pour rendre la somme 11 :*
- il faut 1 pièce de plus que le nombre nécessaire pour rendre 10 (si on rend une pièce de 1)
- il faut 1 pièce de plus que le nombre nécessaire pour rendre 9 (si on rend une pièce de 2)
- il faut 1 pièce de plus que le nombre nécessaire pour rendre 6 (si on rend une pièce de 5)
- il faut 1 pièce de plus que le nombre nécessaire pour rendre 1 (si on rend une pièce de 10)

Il faut ensuite ne garder que la `somme - p` qui est rendue en le minimum de pièce possibles.

*Exemple pour rendre la somme 11 :* on choisit le minimum entre les nombres de pièces nécessaires pour 10, 9, 6, et 1.

Voici alors l'algorithme récursif pour réaliser cela :

```python
def rendu_monnaie(pieces, somme):
    if somme == 0:                                                      # cas de base
        return 0
    else:
        possibilites = []                                               
        for p in pieces:                                                # pour chaque pièce existante
            if p <= somme:                                              # si elle est inférieure à la somme à rendre
                nombre_necessaire = rendu_monnaie(pieces, somme - p)    # on calcule récursivement le nombre de pièces nécessaires
                possibilites.append(nombre_necessaire)                  
        return 1 + min(possibilites) 
```

**Q24)** Testez cette fonction avec somme = 11, puis somme = 23 et enfin somme = 55. Que se passe-t-il ?

**Q25)** Écrivez une version Top-down de cet algorithme.

**Q26)** Écrivez une version Bottom-up de cet algorithme.


## Pour aller plus loin

**Q27)** Testez vos fonctions dynamiques du rendu de monnaie avec somme = 12525. Que se passe-t-il ? Expliquez.

**Q28)** Déduisez donc un avantage de la méthode Bottom-up.

**Q29)** La méthode Top-down a elle aussi un avantage. Saurez-vous trouver lequel ?  
*Indice : pour calculer un rendu de monnaie, a-t-on vraiment besoin de connaître les rendus de **toutes** les pièces inférieures ?*

**Q30)** Il y a quand même une limite à Bottom-up... Testez avec un très grand nombre comme 596654122. Quelle erreur obtenez-vous ? Quelle est donc la limite ?

**Q31)** Voici le programme pour la méthode Top-Down sur la pyramide :
* Testez le sur la pyramide [[3], [7,4], [2,4,6], [8,5,9,3]].
* Expliquez ce qui est affiché.
* Commentez chaque ligne pour expliquer ce qui est réalisé.

```python
def affiche(pyramide):
    for t in pyramide:
        for c in t:
            print(c, end="  ")
        print()
    print()
    
def chemin_max_dynamique(pyramide):
    memoire = []
    for h in range(hauteur(pyramide)):
        memoire.append([None] * (h+1))
    
    chemin_max_memoire_top_down(pyramide, memoire, 0, 0)
    return memoire[0][0]

def chemin_max_memoire_top_down(pyramide, memoire, i, j):
    if hauteur(pyramide) == 1:
        memoire[i][j] = pyramide[0][0]
        
    elif memoire[i][j] == None:
        chemin_max_memoire_top_down(sous_pyramide_gauche(pyramide), memoire, i+1, j)
        chemin_max_memoire_top_down(sous_pyramide_droite(pyramide), memoire, i+1, j+1)
        memoire[i][j] = pyramide[0][0] + max(memoire[i+1][j], memoire[i+1][j+1])
        
    affiche(memoire)
```

**Q32)** Mêmes questions pour la méthode Bottom-up :
```python
def affiche(pyramide):
    for t in pyramide:
        for c in t:
            print(c, end="  ")
        print()
    print()
	
def chemin_max_dynamique(pyramide):
    memoire = []
    for h in range(hauteur(pyramide)):
        memoire.append([None] * (h+1))
    
    chemin_max_memoire_bottom_up(pyramide, memoire)
    return memoire[0][0]

def chemin_max_memoire_bottom_up(pyramide, memoire):
    
    for j in range(len(pyramide[-1])):
        memoire[-1][j] = pyramide[-1][j]
        affiche(memoire)
    
    for i in range(hauteur(pyramide)-2, -1, -1):
        for j in range(len(pyramide[i])):
            memoire[i][j] = pyramide[i][j] + max(memoire[i+1][j], memoire[i+1][j+1])
            affiche(memoire)
```


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
