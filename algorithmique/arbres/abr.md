# Arbres binaires de recherche (ABR)

Nous allons découvrir un type d'arbre particulier : les **arbres binaires de recherche** (ABR). 

Ce sont des structures de données composées de clés ordonnées pour lesquelles la recherche, la suppression et l'insertion sont relativement rapides. Ils peuvent être utilisés dans des ensembles ou des dictionnaires pour effectuer des opérations rapides sur leurs éléments.



## I. Définition

Un arbre binaire de recherche est un arbre binaire dans lequel **chaque nœud possède une clé**,  telle que chaque nœud du **sous-arbre gauche ait une clé inférieure** ou égale à celle du nœud considéré, et que chaque nœud du **sous-arbre droit possède une clé supérieure** ou égale à celle-ci.

Voici un exemple d'ABR :

​	![arbre binaire de recherche](img/abr.png)

Voici sa représentation en tableau :

```python
arbre = [8, [3, [1, None, None], [6, [4, None, None], [7, None, None]]], [10, None, [14, [13, None, None], None]]]
```

Nous utilisons un tableau plutôt qu'un tuple car nous allons modifier l'arbre.

​    **1)** Est-ce bien un arbre binaire de recherche ?

​    **2)** Appliquez le parcours infixe à cet arbre, que remarquez-vous ?



## II. Recherche d'un élément

L'un des intérêts des ABR est de pouvoir chercher rapidement un élément. Voici un algorithme faisant ce travail :

```none
recherche(nœud, clé):
    si nœud est vide:
        renvoyer FAUX
    sinon:
        si nœud.clé = clé:
            renvoyer VRAI
        si nœud.clé > clé:
            renvoyer recherche(nœud.gauche, clé)
        si nœud.clé < clé:
            renvoyer recherche(nœud.droit, clé)
```

​    **3)** Proposez une implémentation en Python de cet algorithme et testez son résultat sur la recherche de des clés « 7 » et « 9 ».

On appelle `n` la taille de l'arbre. Voyons pourquoi cette recherche est rapide :

* Dans le **pire des cas**, nous cherchons une clé dans un arbre **filiforme**. Nous allons alors potentiellement devoir parcourir tous les nœuds de l'arbre pour trouver notre clé. Le coût de la recherche est donc **$`O(n)`$**. On dit que nous avons un coût **linéaire**. C'est le même coût que pour la recherche classique dans un tableau trié. 
* Dans le **meilleur des cas**, nous cherchons une clé dans un arbre **complet**. Nous allons alors, à chaque étape, réduire de moitié le nombre de nœuds qu'il reste à regarder (car on recherche soit à gauche soit à droite et qu'il y a le même nombre de nœuds à gauche qu'à droite). Le coût de la recherche est donc **$`O(log_2(n))`$**. On dit que nous avons un coût **logarithmique**. C'est le même coût que la recherche *dichotomique* dans un tableau trié.

​	**4)** On recherche la clé « 1 ». Dessinez un ABR pour lequel on a un coût de recherche de $`O(n)`$, et un ABR pour lequel on a un coût de $`O(log_2(n))`$.

​	**5)** Rappelez le principe de la recherche dichotomique dans un tableau trié.



## III. Insertion

Voici une implémentation en Python d'un algorithme d'insertion d'un nouvel élément dans un ABR :

```python
def insertionABR(arbre, element):
    if arbre[0] > element:
        if arbre[1] is not None:
            insertionABR(arbre[1], element)
        else:
            arbre[1] = [element, None, None]
            print(element, 'ajouté à gauche de', arbre[0])
            
    if arbre[0] < element:
        if arbre[2] is not None:
            insertionABR(arbre[2], element)
        else:
            arbre[2] = [element, None, None]
            print(element, 'ajouté à droite de', arbre[0])
```

​    **6)** Cet algorithme est-il récursif ?

​	**7)** Commentez ce code.

​    **8)** Que va-t-il afficher si on exécute la commande `insertionABR(arbre, 12)` ? Dessinez l'arbre modifié.

​    **9)** Que va-t-il afficher si on exécute la commande `insertionABR(arbre, 9)` ? Dessinez l'arbre modifié.

​    **10)** Que va-t-il afficher si on exécute la commande `insertionABR(arbre, 2)` ? Dessinez l'arbre modifié.

​    **11)** Proposez un algorithme non récursif (en Python ou en pseudo-code) pour insérer un élément.



## Pour aller plus loin

* Vous pouvez faire l'exercice 1 du [sujet 28](https://framagit.org/JB_info/nsi_tale/-/blob/master/bac/epreuve_pratique/annales/2021/21_NSI_28/21-NSI-28.pdf) de l'épreuve pratique qui représente les arbres sous forme de dictionnaires.
* Rappels de 1ère : programmez une fonction `recherche_dichotomique(tab, val)` qui renvoie True si `val` appartient à `tab` et False sinon, en utilisant un algorithme de recherche dichotomique.

_________________

Inspiré du travail de Thomas Beline : [kxs](https://kxs.fr/cours/).

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
