# Algorithmes sur les arbres binaires

Nous parlerons ici uniquement des arbres binaires.

​	**1)** Rappelez ce qu'est un arbre binaire.

Voici l'arbre que nous utiliserons comme exemple pour commencer :

​	![arbre exemple](img/arbre1.png)



## I. Taille d'un arbre

​    **2)** Quelle est la taille de cet arbre ?

​    **3)** Proposez un algorithme récursif permettant de calculer la taille d'un arbre (pseudo-code).



## II. Hauteur d'un arbre

​    **4)** Quelle est la hauteur de cet arbre ?

​    **5)** Proposez un algorithme récursif permettant de calculer la hauteur d'un arbre.



## III. Parcours d'un arbre

Le parcours d'un arbre correspond à l'ordre dans lequel on parcourt ses nœuds.

Voici les différentes façons de parcourir un arbre :

### 1. Parcours en largeur

​    On commence par la racine puis ses fils, puis les fils des fils etc. On avance profondeur par profondeur et on attend qu'une profondeur soit complètement parcourue avant de passer à la suivante. Sur chaque profondeur, on avance de gauche à droite.

Sur l'arbre ci-dessus cela donne : R-G-S-D-K-M-N-B-A-T-U.

​	**6)** Redessinez l'arbre et numéroter les nœuds selon ce parcours.

### 2. Parcours en profondeur

Ici, on visite le sous-arbre gauche avant le sous-arbre droit pour chaque nœud. Il existe trois types de parcours en profondeur en fonction de l'ordre de visite des fils par rapport à leur père.

SAG = sous-arbre gauche / SAD = sous-arbre droit.

#### a. Préfixe

Racine - SAG - SAD. Si SAG possède des fils, on visitera son propre fils gauche avant de passer à SAD (c'est le parcours en profondeur).

Sur l'exemple : R-G-D-B-A-K-T-S-M-N-U

​	**7)** Redessinez l'arbre et numéroter les nœuds selon ce parcours.

#### b. Infixe

SAG - racine - SAD.

Sur l'exemple : B-D-A-G-K-T-R-M-S-U-N

​	**8)** Redessinez l'arbre et numéroter les nœuds selon ce parcours.

#### c. Suffixe

SAG - SAD - racine.

 Sur l'exemple : B-A-D-T-K-G-M-U-N-S-R.

​	**9)** Redessinez l'arbre et numéroter les nœuds selon ce parcours.

### 3. Exercices

​    On considère l'arbre ci-dessous :

​	![arbre 2](img/arbre2.png)

​    **10)** Donner l'ordre des nœuds pour un parcours en largeur.

​    **11)** Donner l'ordre des nœuds pour un parcours en profondeur préfixe.

​    **12)** Donner l'ordre des nœuds pour un parcours en profondeur infixe.

​    **13)** Donner l'ordre des nœuds pour un parcours en profondeur suffixe.

​    **14)** Donnez le type de parcours d'arbre des algorithmes suivants :

```python
parcours1(nœud):
    si nœud non vide:
        parcours1(nœud.gauche)
        affiche(nœud.valeur)
        parcours1(nœud.droit)
        
parcours2(nœud):
    si nœud non vide:
        affiche(nœud.valeur)
        parcours2(nœud.gauche)
        parcours2(nœud.droit)

parcours3(nœud):
    si arbre non vide:
        parcours3(nœud.gauche)
        parcours3(nœud.droit)
        affiche(nœud.valeur)
        
f = file vide
parcours4(nœud):
    f.enfile(nœud)
    tant que f est non vide:
        x = f.defile()
        affiche x.valeur
        si x.gauche non vide:
            f.enfile(x.gauche)
        si x.droit non vide:
            f.enfile(x.droit)
```

On considère maintenant l'arbre ci-dessous :

​	![arbre 3](img/arbre3.png)

​    **15)** Donner l'ordre des nœuds pour un parcours en largeur.

​    **16)** Donner l'ordre des nœuds pour un parcours en profondeur préfixe.

​    **17)** Donner l'ordre des nœuds pour un parcours en profondeur infixe.

​    **18)** Donner l'ordre des nœuds pour un parcours en profondeur suffixe.



_________________

Inspiré du travail de Thomas Beline : [kxs](https://kxs.fr/cours/).

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)