# Paradigmes de programmation


## I. Principe

### 1. Définition

Un **paradigme de programmation est une manière d'approcher la résolution d'un problème informatique**, le « style » d'écriture d'un algorithme dans un langage de programmation.

Vous connaissez déjà deux paradigmes de programmation :
* la programmation impérative (avec les boucles...)
* la programmation objet (avec les classes...)

> **ex 1 : paradigme impératif**  
> Le paradigme impératif consiste à décrire les algorithmes en séquences d'instructions exécutées dans l'ordre par l'ordinateur : des affectations, des conditions, des boucles, etc.  
> La plupart des tous premiers langages de programmation étaient impératifs (Fortran, Basic, C...).  
> Python propose le paradigme impératif également. Rappelez la syntaxe pour :
> * l'affectation
> * la conditionnelle
> * la boucle bornée
> * la boucle non bornée
> * la définition de fonctions

> **ex 2 : paradigme objet**  
> Le paradigme objet consiste à définir et faire interagir des objets : un objet représente un concept, une idée, ou une entité du monde physique.  
> Ce paradigme est apparu dans les années 70. Le premier langage à l'implémenter est SmallTalk.  
> Python propose aussi le paradigme objet. Rappelez la syntaxe pour :
> * définir une classe
> * définir le constructeur de la classe
> * ajouter une méthode
> * ajouter un attribut
> * créer un objet de la classe

Il existe bien d'autres paradigmes :
* la programmation fonctionnelle
* la programmation évènementielle
* la programmation logique
* la programmation concurrente
* ...

Vous connaissez peut-être déjà le paradigme *évènementiel* : c'est ce paradigme que vous utilisez en JavaScript (1995) quand vous gérez les évènements d'une page Web (clic sur un bouton par exemple).

Nous verrons aussi ici le paradigme *fonctionnel*. Pour découvrir les autres paradigmes, il faudra attendre le post-bac !

### 2. Multi-paradigmes

Vous avez déjà écrit des programmes entièrement en objet, et aussi des programmes entièrement en impératif. Et vous avez même écrit des programmes qui mélangeaient les 2 : dans le projet labyrinthe par exemple, vous aviez la classe Labyrinthe (en objet) et le menu en dehors (en impératif). **Il est donc possible d'utiliser plusieurs paradigmes différents dans un même programme**.

Les langages de programmation qui acceptent plusieurs paradigmes sont dit **multi-paradigmes**. C'est le cas de Python par exemple !

> **ex 3 :**  
> Connaissez-vous d'autres langages multi-paradigmes ?

Il existe cependant aussi des langages qui n'acceptent qu'un seul paradigme. Par exemple, Haskell (1990) est un langage purement fonctionnel. Et en Java (1995), tout est objet.

### 3. Choisir le bon paradigme

On choisit généralement son langage en fonction du paradigme que l'on va utiliser. 

Et on choisit son paradigme en fonction de la situation. En théorie, **on peut utiliser n'importe quel paradigme pour écrire n'importe quel programme**. Un programme en impératif peut donc aussi être écrit en objet, ou encore en fonctionnel. En pratique par contre, il y a toujours un **paradigme plus adapté que les autres à la situation**.

Chaque paradigme a donc ses avantages et ses inconvénients. Voici par exemple un tableau récapitulatif des avantages et inconvénients des paradigmes impératif, objet et fonctionnel (qui sont les 3 que vous devez connaître pour le Grand Oral) :

|                 |                                                 Impératif                                                |                                          Objet                                          |                                            Fonctionnel                                            |
|:---------------:|:--------------------------------------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------:|
| Usages courants | traitement de données, interactions avec l'utilisateur, applications nécessitant des changements d'états | représentation des données, traitement d'entités physiques, production de comportements |             calculs mathématiques, production de résultats plutôt que de comportements            |
|  Utilisé seul ? |                              Possible, souvent intégré à un autre paradigme                              |                                       Très souvent                                      |                                              Rarement                                             |
|   Abstraction   |                                       Le plus proche du processeur                                       |                  Peut entraîner des traitements supplémentaires cachés                  | Doit être converti en impératif par le compilateur/interpréteur (la récursion peut être coûteuse) |
|   Maintenance   |                        Peut être complexe à maintenir et déboguer (effets de bord)                       |               Peut être complexe à maintenir et déboguer (effets de bord)               |                Facile à déboguer (pas d'effets de bord), Souvent facile à optimiser               |

Chaque paradigme a ses usages de prédilection. Le **choix du paradigme impacte beaucoup les performances** du programme.

On demande donc aux développeurs aujourd'hui de maîtriser plusieurs paradigmes, afin d'être capable de choisir le bon et de passer de l'un à l'autre efficacement.

> **ex 4 :**  
> Pour chaque situation suivante, choisissez le paradigme (parmi impératif, objet et fonctionnel) le plus adapté :
> * Représenter le réseau informatique du lycée
> * Implémenter le jeu du "plus ou moins" (l'ordinateur choisit un chiffre et l'utilisateur doit le deviner)
> * Calculer les dérivées de fonctions mathématiques
> * Manipuler les fichiers du disque dur de l'ordinateur
> * Suivre la disponibilité des vélos dans les stations V'Lille
> * Analyser les données des clients d'un site de vente en ligne


## II. Paradigme fonctionnel

Depuis une quinzaine d’année et l’émergence d’enjeux liés à la massification des données, le paradigme fonctionnel a fait une entrée remarquée dans les grands groupes informatiques. Citons Twitter, Netflix (Scala) et Facebook (OCaml) qui l’utilisent pour accomplir des travaux sur les données considérables qu’ils génèrent. L'interface de ces applications est en objet (Ruby pour Twitter par exemple), mais tous les calculs sur les données sont en fonctionnel.

La programmation fonctionnelle est donc **indispensable** aujourd'hui. D'ailleurs, si vous continuez vos études dans l'informatique, vous étudierez forcément le paradigme fonctionnel (avec le langage Haskell ou le langage OCaml).

Nous n'allons découvrir ici que les règles de base de ce paradigme.

### 1. Règles

Comme son nom l'indique, la programmation fonctionnelle n'**utilise que des fonctions**.

La première chose interdite en programmation fonctionnelle est l'**effet de bord**.
Une fonction possède des effets de bord lorsqu'elle **modifie une variable extérieure**.

Pour comprendre ce phénomène, voici un exemple d'une fonction avec paramètre qui incrémente une variable :

```python
# PROG 1

def incremente(a):
    a = a + 1
    return a

a = 3

print(a)
print(incremente(a))
print(a)
```

> **ex 5 :**  
> 1. Devinez ce qui va être affiché en sortie de ce programme.
> 2. Testez-le pour vérifier votre réponse.

Voici un second exemple de fonction sans paramètre qui incrémente une variable :

```python
# PROG 2

def incremente():
    global a
    a = a + 1

a = 1

print(a)
incremente()
print(a)
```

> **ex 6 :**  
> 1. Devinez ce qui va être affiché en sortie de ce programme.
> 2. Testez-le pour vérifier votre réponse.

Le programme 1 ne modifie pas la variable extérieure : elle ne possède pas d'effets de bord.
Le programme 2 modifie la variable extérieure : elle possède un effet de bord.

Première règle : la **programmation fonctionnelle interdit les effets de bord**. Cela signifie que le programme 2 déclenchera une erreur dans un langage strictement fonctionnel. Éviter les effets de bord limite les bugs et améliore la compréhension d'un programme. 

Deuxième règle : **pas d'affectation et de variables extérieures aux fonctions**.

> **ex 7 :**  
> Parmi les deux programmes suivants, lequel respecte les principes de la programmation fonctionnelle ?
> ```python
> def superieur(a, b):
>     return a > b
> 
> print(superieur(4, 3))
> ```
> ```python
> def superieur(b):
>     return a > b
> 
> a = 4
> print(superieur(3))
> ```

### 2. Fonctions

Il existe trois types de fonctions en programmation fonctionnelle :

* les fonctions de premier ordre
* les fonctions d'ordre supérieur
* les fonctions anonymes

Les **fonctions de premier ordre** sont les fonctions simples que vous savez déjà écrire.

Exemple :
```python
def addition(a, b):
    return a + b
```

Et pour appeler cette fonction :
```python
>>> addition(2, 3)
5
```

> **ex 8 :**  
> Définissez et appelez une fonction de premier ordre qui multiplie les deux entiers 3 et 5.

Les **fonctions anonymes** font la même chose que les fonctions de premier ordre mais sont destinées à un usage unique, simple et rapide. Elles sont appelées parfois "lambda-fonctions" (en référence au lambda-calcul de Church vu au chapitre précédent).

Exemple :
```python
lambda a,b : a+b
```

"lambda" signifie que la fonction est anonyme. Les paramètres sont indiqués ensuite, séparés par des virgules (a,b). Après le ":" on indique le résultat de la fonction.

Les fonctions anonymes n'ont pas de nom, donc on ne peut pas les appeler, et sont destinées à n'être utilisées qu'une seule fois sur des valeurs précises. Voici un exemple d'utilisation :
```python
>>> (lambda a,b : a+b)(2,3)
5
```

> **ex 9 :**  
> * Utilisez une fonction anonyme pour multiplier les deux entiers 3 et 5.
> * Voyez-vous l'avantage par rapport à l'exercice 8 ?

Les **fonctions d'ordre supérieur** sont les fonctions fournies par le langage qui prennent elles-mêmes d'autres fonctions en paramètre.

Les plus connues (et les plus utilisées) sont :
* map
* filter
* reduce

**map** permet d'appliquer une fonction à un tableau.

> **ex 10 :**  
> * Testez :
> ```python
> >>> list(map(len, ["Vive", "le", "fonctionnel"]))
> ?
> ```
> * Donnez l'instruction fonctionnelle pour transformer les chaînes du tableau `["4", "17", "3"]` en entiers (`[4, 17, 3]`).
> * Avec une lambda fonction maintenant, testez :
> ```python
> >>> list(map(lambda x : x**2, [1, 2, 3, 4]))
> ?
> ```
> * Donnez l'instruction fonctionnelle pour multiplier par 10 chaque nombre du tableau `[1, 2, 3, 4, 5]`.

**filter** permet de filtrer un tableau selon une condition.

> **ex 11 :**  
> * Testez :
> ```python
> >>> list(filter(lambda x : x > 0, [1, -2, 3, -4, 5]))
> ?
> ```
> * Donnez l'instruction fonctionnelle pour ne garder que les nombres pairs du tableau `[1, 2, 3, 4, 5]`.

**reduce** permet de calculer une valeur à partir d'un tableau.

> **ex 12 :**  
> * Testez :
> ```python
> >>> from functools import reduce
> >>> reduce(lambda x,y : x+y, [1, 2, 3])
> ?
> ```
> * Donnez l'instruction fonctionnelle pour calculer le produit du tableau `[1, 2, 3, 4]`.

Il existe bien d'autres fonctions d'ordre supérieur, mais "map", "filter" et "reduce" sont les plus courantes et les seules que vous devez connaître.

Ces trois fonctions sont disponibles dans tous les langages qui proposent le paradigme fonctionnel. Parfois, il faut les importer d'une bibliothèque (comme pour reduce en Python).


## Pour aller plus loin :

Les premiers langages de programmation étaient à l'époque conçu pour un usage unique et donc un paradigme unique. Mais les langages actuels supportent de plus en plus de paradigmes différents. Par exemple, avec Python, vous pouvez faire de l'impératif, de l'objet, du fonctionnel, ... Et dans un même programme Python, vous pouvez utiliser les 3 à la fois !

Ce qu'on attend de plus en plus des informaticiens aujourd'hui, c'est de maîtriser des paradigmes ; le langage, ça vient plus tard. Quand on a un problème à résoudre, un programme à écrire, etc. on choisit d'abord le paradigme le plus approprié *puis* on sélectionne un langage qui offre ce paradigme.

Voici une liste non-exhaustive des principaux langages de programmation : 

* Fortran (1954)
* Algol (1958)
* Basic (1964)
* Lisp (1958)
* Prolog (1972)
* Cobol (1959)
* Pascal (1970)
* C (1972)
* C++ (1985)
* C# (2001)
* Perl (1987)
* Python (1991)
* JavaScript (1995)
* PHP (1994)
* Ruby (1995)
* Ada (1980)
* Java (1995)
* Haskell (1990)
* OCaml (1996)

> **ex 13 :**  
> Pour chacun d'entre eux, cherchez le ou les paradigme(s) qu'il supporte.

*Pour vous aider, vous pouvez aller voir [ici](https://fr.wikipedia.org/wiki/Comparaison_des_langages_de_programmation_multi-paradigmes#R%C3%A9sum%C3%A9s_des_langages).*

> **ex 14 :**  
> Il est possible (et même très courant en fonctionnel) de combiner plusieurs fonctions.
> Testez :
> ```python
> >>> reduce(lambda x,y : x+y, map(int, ["1", "2", "3"]))
> ?
> ```
> Utilisez les fonctions map, filter et reduce pour :
> * trouver la somme des valeurs positives d'un tableau
> * multiplier les valeurs non nulles d'un tableau
> * additionner les longueurs des chaînes de caractères d'un tableau
> * trouver la taille de la plus grande chaîne d'un tableau
> * trouver le nombre de chiffres de chaque nombre d'un tableau
> * trouver le nombre total de "e" des chaînes commençant par un "a" d'un tableau (par exemple pour `["avocat", "erreur", "aide", "nsi", "amener"]` on doit trouver 3)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
