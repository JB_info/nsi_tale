# Récursivité



![récursivité vache qui rit](img/vache.jpg)

Une fonction récursive est une fonction qui fait appel à elle-même. Le premier langage de programmation qui a implémenté la récursivité était Lisp en 1958.



## I. Un exemple pour commencer

### 1. Version itérative

Le but ici est de calculer $`somme(n) = 0  + 1 + 2+ ... + n`$ où $`n \in \N`$

​	**1)** La première technique est d'utiliser une boucle. Il s'agit d'une fonction **itérative**. Allez -y :

```python
def somme(n):
    '''
    renvoie la somme 0+1+2+3+...+n avec n entier positif ou nul
    : param n
    type int
    : return la somme
    type int
    '''
    
```

### 2. Version récursive

Il y a une autre façon de voir les choses. On peut définir la somme de la façon suivante :

$`somme(n) = \left\{ \begin{array}{ll}     0 {~si~} n = 0\\     n + somme(n -1){~sinon.} \end{array}\right.`$

De cette façon : 

* `somme(0) = 0`​

* `somme(1) = somme(0) + 1 = 0 + 1 = 1`​

* `somme(2) = somme(1) + 2 = 1 + 2 = 3`​

  **2) ** Continuez :

* `somme(3) =`​

* `somme(4) =`

* `somme(5) =`



La définition de `somme(n)` dépend de `somme(n - 1)`​ : c'est une définition **récursive**.

Pour pouvoir calculer` somme(n)`, il faut impérativement connaître le **cas de base** : `somme(0) = 0`​ qui garantit la terminaison de l'algorithme.

Cette façon nouvelle de voir les choses s'implémente assez facilement en Python :

```python
def somme(n):
    '''
    renvoie la somme 0+1+2+3+...+n avec n entier positif ou nul
    : param n
    type int
    : return la somme
    type int
    '''
    if n == 0 : # on commence par le cas de base
        return 0
    else : # puis le cas récursif
        return somme(n - 1) + n
```

​	**3)** Testez cette fonction avec différentes valeurs de n :

```python
>>> somme(5)
???
>>> somme(100)
???
```

​	**4)** Prenons maintenant une petite valeur de `n`, par exemple 3, et voyons ce qu'il se passe en exécutant le code pas à pas. (utilisez le *débogueur* de Thonny puis résumez les étapes dans un schéma)

### 3. Profondeur de récursion

​	**5)** Voyons ce qu'il se passe si on donne une valeur de n hors domaine de définition :

```python
>>> somme(-5)
???
>>> somme(3.4)
???
```

Notre définition n'est valable que pour $`n \in \N`$ . Pour d'autres valeurs, le cas de base pourrait ne jamais être atteint, ce qui empêche la récursion de se terminer. Python _boucle_ donc jusqu'à atteindre une _profondeur_ de récursion trop importante qui déclenche une exception. 

​	**5) Bonus** On peut trouver et modifier la valeur maximale de la profondeur :

```python
>>> import sys
>>> sys.getrecursionlimit()
???
>>> sys.setrecursionlimit(4000) 
>>> sys.getrecursionlimit()
???
```

### 4. Rappels : assertion

Pour éviter les erreurs de domaine, on peut contrôler le type et la plage dans laquelle se situe n, en utilisant des assertions :

```python
def somme(n):
	'''
    renvoie la somme 0+1+2+3+...+n avec n entier positif ou nul
    : param n
    type int
    : return la somme
    type int
    '''
    assert(type(n) == int),'le paramètre doit être un entier'
    assert(n >= 0),'le paramètre doit être positif ou nul'
    if n == 0 :
        return 0
    else : 
        return somme(n - 1) + n
```

​	**6)** Testez à nouveau :

```python
>>> somme(-5)
???
>>> somme(3.4)
???
```

​	**7)** À quoi servent les assertions dans une fonction ?



## II. Une bonne définition récursive

### 1. Règles

Lorsque la définition récursive est bien posée, il est en général assez simple de l'implémenter en Python. Il faut toutefois respecter certaines règles pour réussir cette définition :

* Il faut être certain que la récursion va s'arrêter, c'est à dire être certain de tomber à un moment sur le cas de base.
* Il faut s'assurer que les paramètres de la fonction sont bien dans le domaine de définition prévu.
* Il faut également vérifier qu'il y a une définition pour chaque valeur du domaine de définition.



​	**8)** Dans chaque cas, trouvez ce qui ne va pas dans la définition récursive pour $`n \in N`$

1. $`f(n) = \left\{ \begin{array}{ll}     32 {~si~} n = 0\\     f(n +1) - 4 {~sinon.} \end{array}\right.`$

2. $`f(n) = \left\{ \begin{array}{ll}     1 {~si~} n = 0\\     n + f(n - 2) {~sinon.} \end{array}\right.`$

3. $`f(n) = \left\{ \begin{array}{ll}     1 {~si~} n = 0\\     n + f(n - 1) {~si~n>1.} \end{array}\right.`$

### 2. Exercices

Trouvez une définition récursive pour les problèmes ci-dessous puis programmez les en Python.

​	**9)** Calculer $`x^n`$ pour tout `x` réel et `n` entier positif ou nul.

​	**10)** Calculer $`n!`$ pour tout entier `n` positif ou nul. *(La factorielle d'un entier naturel n est le produit des nombres entiers strictement positifs inférieurs ou égaux à n. Par convention on a 0! = 1.)*

​	**11)** Calculer $`n \times p`$ pour tout entier `n` et `p` en ne faisant que des additions.

​	**12)** Cherchez une définition de `renverser(chaine)`dont le but est de *renverser* une chaîne de caractère ('abcde' devient 'edcba') .

​	**13)** Cherchez une définition dont le but est de réaliser la figure ci-dessous avec le paramètre de profondeur _n_ > 0  égal au nombre de côté(s) de la spirale et un second paramètre _l > 0_ égal à la longueur du côté à tracé. *([rappels turtle](https://kxs.fr/cours/turtle/))*

![spirale](img/spirale.jpg)

$`spirale(n, l) = \left\{ \begin{array}{ll}   tracer~un~segment~de~longueur~l   {~si~} n = 1\\     .... {~si~n>1.} \end{array}\right.`$	



## III. Cas plus complexes

### 1. Multiples cas de base

Reprenons l'exemple  $`somme(n) = 0  + 1 + 2+ ... + n`$ où $`n \in \N`$.

* Nous avons vu que si _n_ vaut 0 alors _somme(n)_ vaut 0.

* Si _n_ vaut 1 alors _somme(n)_ = 0 + 1 = 1 : on pourrait prendre un deuxième cas de base pour éviter ce calcul un peu ridicule : si _n_ = 1 alors _somme(n)_= 1. La définition deviendrait alors :

    $`somme(n) = \left\{ \begin{array}{ll}     0 {~si~} n = 0\\ 1 ~si~n=1\\    n + somme(n -1){~sinon.} \end{array}\right.`$


​	**14)**  Redonnez une définition récursive avec deux cas de base de  $`x^n`$ pour tout `x`​ réel et `n` entier positif ou nul.

​	**15)** Un [palindrome](https://fr.wikipedia.org/wiki/Palindrome) est un mot dont l'ordre des lettres reste le même qu'on le lise de droite à gauche ou de gauche à droite ('121, 'été, 'radar', 'unroccornu' ou 'engagelejeuquejelegagne' sont des palindromes). Cherchez une définition récursive de `est_palindrome(mot)` qui renvoie `True` si le mot est un palindrome et `False` sinon.



### 2. Cas récursif multiple

Dans certains cas, la définition s'appelle plusieurs fois. Voici par exemple la définition de la célèbre [suite de Fibonacci](https://fr.wikipedia.org/wiki/Suite_de_Fibonacci) définie pour tout entier $`n \in \N`$ par :

$`U_n = \left\{ \begin{array}{ll}     1 ~si~ n = 0    \\ 1 ~si~n=1 \\ U_{n-1} + U_{n-2} ~sinon \end{array}\right.`$

​	**16)** Calculez _à la main_ les 5 premiers termes de la suite.

L'implémentation en Python serait :

```python
def u(n):
    '''
    renvoie la valeur du terme de rang n de la suite de fibonacci
    :param n
    type int
    : return
    type int
    >>> u(0)
    1
    >>> u(1)
    1
    >>> u(2)
    2
    >>> u(3)
    3
    >>> u(4)
    5
    '''
    assert(type(n) == int),' n est entier'
    assert(n >= 0),' n est positif ou nul'
    if n == 0 :
        return 1
    if n == 1 :
        return 1
    else :
        return u(n - 1) + u( n - 2)
```

​	**17)** Testez :

```python
>>> u(5)
???
>>> u(6)
???        
```

​	**18)** Le [triangle de Pascal](https://fr.wikipedia.org/wiki/Triangle_de_Pascal) permet de déterminer de façon récursive les coefficients binomiaux qui sont utilisés dans de nombreux domaines. Ce coefficient, que l'on notera `C(n, p)`, dépend de deux paramètres entiers positifs ou nuls `n` et `p`. Voici le début du triangle :

|         | p = 0 | *p* = 1 | *p* = 2 | *p* = 3 | *p* = 4 | **p = 5** | **p = 6** |
| ------- | ----- | ------- | ------- | ------- | ------- | --------- | --------- |
| *n* = 0 | 1     |         |         |         |         |           |           |
| *n* = 1 | 1     | 1       |         |         |         |           |           |
| *n* = 2 | 1     | 2       | 1       |         |         |           |           |
| *n* = 3 | 1     | 3       | 3       | 1       |         |           |           |
| *n* = 4 | 1     | 4       | 6       | 4       | 1       |           |           |
| n = 5   |       |         |         |         |         |           |           |
| n = 6   |       |         |         |         |         |           |           |

* trouvez l'astuce et complétez les lignes manquantes.
* quelles conditions y-a-t-il sur _n_ ? sur _p_ ?
* trouvez la définition récursive de `C(n, p)` *puis* codez là en Python.

### 3. Récursions imbriquées

Voici une fonction que l'on doit à [John Mc Carthy](https://fr.wikipedia.org/wiki/John_McCarthy) (le créateur du langage Lisp) :

$`f(n) = \left\{ \begin{array}{ll} n-10 ~si~ n > 100    \\  f(f(n +11)) ~sinon \end{array}\right.`$

Remarquez que l'appel récursif est imbriqué à l'intérieur d'un autre appel récursif. Il est difficile ici d'être certain que le cas de base permet la terminaison de la récursion.

​	**19)**

* Calculez à la main $`f(101), f(100), f(99)`$
* Implémenter cette fonction en Python
* Calculer en utilisant la console, $`f(80),f(50), f(25)`$
* Testez les valeurs de $`f(n)`$ pour tout $`n \leq 100`$.



## IV. Exercices (DM)

Devoir maison en binômes (2 exercices chacun). Téléchargez le fichier suivant : [prenom1_prenom.py](./prenom_1_prenom2.py). C'est ce fichier qui est à compléter et à rendre via l'application Casier sur l'ENT.

À rendre : un fichier python par binôme avec vos deux prénoms. Le fichier doit contenir les réponses aux questions en *commentaires* (ligne commençant par `#`). Les fonctions à programmer doivent être *documentées* (docstring), *testées* (doctests), et équipées d'*assertions*. Des rappels de la syntaxe des docstrings et doctests sont dans le chapitre sur la [programmation objet](./poo.md). Vous êtes également jugé sur la *propreté* du code : code aéré, lisible, nommage des variables et des fonctions adéquat, commentaires appropriés, code optimisé (pas de code "inutile", pas de répétitions, utilisation de boucles et fonctions quand nécessaire...).

Pour travailler sur le même fichier python chacun depuis chez vous, vous pouvez par exemple utiliser le site [codeskulptor](https://py3.codeskulptor.org/). Il faut qu'un des membres du binôme crée un identifiant de session (bouton "Join", laissez la barre vide et cliquez sur "New"). Puis il peut envoyer l'ID de session à son binôme (trouvable en haut à gauche de la fenêtre). Enfin le binôme peut rejoindre la session en cliquant sur "Join" et en entrant l'ID. Une fois le travail fini, il est possible de sauvegarder le fichier python sur son ordinateur en cliquant sur "Save".

### Ex 1 : PGCD /2

Le _pgcd_ de deux entiers positifs _a_ et _b_ (avec $`a \geq b`$) est le plus grand diviseur commun à _a_ et à _b_. Il est très utile en mathématiques, pour simplifier des fractions par exemple.

Pour le déterminer, on utilise la définition récursive suivante, où reste(_a_, _b_) est le reste de la division euclidienne de _a_ par _b_ :



$`pgcd(a, b) = \left\{ \begin{array}{ll} b~si~reste(a,~b)~=~0    \\ pgcd(b,~reste(a,~b))~sinon  \end{array}\right.`$



Implémentez la fonction `pgcd(a, b)` en Python.



### Ex 2 : Tri par sélection /8

Nous avons étudié en première le tri par sélection d'un tableau. 

* rappelez-en le principe. *(si nécessaire allez voir cette [vidéo](https://www.youtube.com/watch?v=8u3Yq-5DTN8))*

* écrivez une définition récursive de ce tri.

* implémentez cette définition en Python.  Nous utiliserons ici un tableau d'entiers ou de chaînes de caractères.


```python
def tri_selection(tableau):
```



### Ex 3 : Chiffres romains /6

Nous allons réaliser maintenant un programme permettant d'évaluer un nombre romain, mais auparavant on va introduire les chiffres romains: 			

- M = 1000
- D =  500
- C =  100
- L =   50
- X =   10
- V =    5
- I =    1

Voici quelques exemples de l'écriture des nombres romains

- 1 s'écrit I.
- 2 s'écrit II.
- 3 s'écrit III.
- 4 s'écrit IV.
- 5 s'écrit V.
- 6 s'écrit VI.
- 7 s'écrit VII.
- 8 s'écrit VIII.
- 9 s'écrit IX.
- 10 s'écrit X.



La règle : 

* si un symbole est seul, on le traduit simplement :
    * X vaut 10.
* s'il y a deux symboles :
    * si le premier est inférieur au symbole suivant, alors on enlève ce symbole au suivant :
        * IX : I est inférieur à X donc on calcule X - I = 10 - 1 = 9
        * XC : X est inférieur à C donc on calcule C- X = 100 - 10 = 90
    * sinon, on les ajoute :
        * XI = 10 + 1 = 11
        * CX = 100 + 10 = 110

* s'il y a plus de deux symboles, on regarde les deux premiers et on les ajoute à ceux qui restent.
    * XCVI= 100 - 10 + VI = 100 - 10 + 5 + 1 = 96
    * MCMXC = ?

Donnez une définition récursive de `romain(chaine)` et implémentez la en Python. On pourra se servir des exemples comme doctests.



### Ex 4 : Autour de Koch /4

La [courbe de Koch](https://fr.wikipedia.org/wiki/Flocon_de_Koch) est un dessin réalisé par récursion de paramètres _l_, la longueur du segment de base, et _n_ la profondeur de récursion.

Voici une animation montrant le tracé d'une telle courbe avec un niveau de profondeur 0 puis 1 puis 2..

![courbe de Koch](https://upload.wikimedia.org/wikipedia/commons/b/bf/Koch_anime.gif)



Ecrivez une définition récursive de la courbe puis implémentez là en Python. Pas de doctests nécessaires ici.

On peut faire évoluer la courbe de Koch en [flocon de Koch](https://fr.wikipedia.org/wiki/Flocon_de_Koch) en définissant une fonction annexe qui appelle trois fois la courbe de Koch en tournant dans le bon sens. On obtient alors la construction suivante :

![flocon de Koch](https://upload.wikimedia.org/wikipedia/commons/f/fd/Von_Koch_curve.gif)



### Ex 4 BONUS :

Une autre évolution possible consiste à remplacer les triangles équilatéraux par des carrés. En serez-vous capable ?



![courbe de Koch avec carrés](https://upload.wikimedia.org/wikipedia/commons/a/a6/Quadratic_Koch_curve_type1_iterations.png)



### Pour aller plus loin :

Les anagrammes d'un mot sont la liste, sans doublon, de tous les mots qu'on peut écrire en permutant toutes les lettres du mot, que le mot ait un sens, ou non.

Par exemple, les anagrammes de :

* 'oui' sont 'oui', 'uoi', 'uio', 'oiu', 'iou' et 'iuo'.
* '121' sont '121', '211' et '112'

Commençons, à la main, par déterminer les anagrammes de '12' puis '123' puis '1234'. Essayez de dégager une stratégie.

Pour obtenir la liste des anagrammes, on va utiliser trois fonctions :

* la première fonction, itérative (c'est à dire utilisant des boucles), sera `inserer_un_caractere(caractere, liste_mots)` qui renverra la liste de tous les mots obtenus en insérant le `caractere` à toutes les positions possibles dans tous les mots présents dans la `liste_mots`.
* la seconde est la fonction récursive `anagrammes(mot)` qui utilisera la précédente pour renvoyer la liste des anagrammes (avec d'éventuels doublons) :

    * déterminer son cas de base.
    * donnez sa définition récursive.
    * codez la en Python !
* la dernière fonction sera `elimine_doublon(liste_mots)` et renverra une liste de mots en éliminant les doublons présents dans la liste passée en paramètre.



_________________

Cours rédigés par Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)