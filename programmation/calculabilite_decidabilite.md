# Calculabilité, décidabilité


**Q1)** À votre avis, peut-on tout programmer ?

Notez votre réponse, vous verrez à la fin de ce cours si vous aviez raison ou non !


## I. Programme en tant que donnée

### 1. Un premier programme simple

Les codes que nous manipulons ressemblent souvent à cela :
```python
def accueil(n):
    for k in range(n):
        print("bonjour")
```

Le programme s'appelle `accueil`, et pour fonctionner il a besoin d'un paramètre, qui sera ici un nombre entier `n`.

**Q2)** Quelle serait la sortie du programme en prenant 5 en entrée ?

Voici comment nous pouvons représenter notre machine `accueil`, son paramètre d'entrée (5) et sa sortie :

![accueil](img/m1.png)

### 2.  Un programme comme paramètre d'un programme

Maintenant, enregistrons le code suivant dans un fichier `test.py` :
```python
def accueil(n):
    for k in range(n):
        print("bonjour")

accueil(5)
```

Pour exécuter ce code, nous devons taper dans un terminal l'instruction suivante :
`python3 test.py`, ce qui donnera 
![](img/term.png)

Le programme utilisé est alors `python3`, qui prend comme paramètre le programme `test.py`. Ce paramètre `test.py` est un ensemble de caractères qui contient les instructions que le programme `python3` va interpréter. 

L'illustration correspondante sera donc :

![](img/m2.png)
 

Mais nous pouvons aller encore plus loin : l'instruction `python3 test.py` est tapée dans mon Terminal Linux, qui lui-même est un programme appelé `Terminal`.

Et donc :
![](img/m3.png)


Conclusion :  
**Il n'y a donc aucun obstacle à considérer un programme comme une simple donnée, pouvant être reçue en paramètre par un autre programme.**

**Q3)** Comment s'appelle l'architecture des ordinateurs qui stocke programmes et données dans la même mémoire ?

### 3. Interpréteur / compilateur

Nous avons dit plus haut qu'en exécutant l'instruction `python3 test.py` :
![](img/term.png)

Le programme `python3` *interprète* le programme `test.py`. Mais qu'est-ce que ça veut dire ?

Certains langages de programmation comme Python (1991) ou Java (1995), sont directement exécutés par un programme : l'interpréteur. L'interpréteur de Python s'appelle par exemple `python3` et doit être installé sur l'ordinateur. N'importe quel code source est donc exécutable sur n'importe quelle machine possédant un interpréteur. Voici le schéma de fonctionnement :

![](img/interpreteur.png)

**Q4)** Expliquez pourquoi un interpréteur est un bon exemple de programme dont les entrées ne distinguent pas donnée et programme.

Il existe des langages de programmation qui nécessitent d'être compilés, comme le C (1972) ou Caml (1985). C'est à dire qu'ils ne sont pas exécutés par un interpréteur comme Python mais doivent d'abord être transformés en code binaire exécuté par le système d'exploitation. Le code compilé appelé binaire ne peut être exécuté que sur les machines de même architecture. Si on veut exécuter le code sur des machines différentes, il faut le recompiler. Voici le schéma de fonctionnement : 

![](img/compilateur.png)


**Q5)** Cherchez rapidement les avantages/inconvénients les langages interprétés par rapport aux langages compilés.


## II. Problème de l'arrêt

### 1. Un exemple

Considérons le programme suivant :

```python
def countdown(n):
    while n != 0 :
        print(n)
        n = n - 1
    print("fini")
```

**Q6)** Quel sera la sortie du programme avec en entrée 10 ?

En l'observant attentivement, je peux prévoir que `countdown(10)` affichera les nombres de 10 à 1 avant d'écrire "fini". Puis le programme s'arrêtera.

**Q7)** Mais que va provoquer `countdown(10.8)` ?

Comme la variable `n` ne sera jamais égale à 0, le programme va rentrer dans une boucle infinie, il ne s'arrêtera jamais. Mauvaise nouvelle.

**Q8)** J'ai pu prévoir ceci en regardant attentivement le code de mon programme. J'ai «remarqué» qu'une variable `n` non entière provoquerait une boucle infinie. Est-ce qu'un programme d'_analyse de programmes_ aurait pu faire cela à ma place ?

Notez votre réponse, vous verrez si vous aviez raison ou non !


### 2. Une machine pour prédire l'arrêt ou non d'un programme

Après tout, un programme est une suite d'instructions (le code source), et peut donc être, comme on l'a vu, le paramètre d'entrée d'un autre programme qui l'analyserait.

Un tel programme (appelons-le `halt`) prendrait en entrées :
- un paramètre `prog` (le code source du programme)
- un paramètre  `x`, qui serait le paramètre d'entrée de `prog`.

 L'instruction `halt(prog, x)` renverrait `True` si `prog(x)` s'arrête, et `False` si `prog(x)` ne s'arrête pas.
 

![](img/halt1.png)



*Exemple* : 
- `halt(countdown, 10)` renverrait `True`.
- `halt(countdown, 10.8)` renverrait `False`. 

![](img/halt2.png)


**Q9)** Dessinez un schéma similaire pour `halt(countdown, 3)` et `halt(countdown, -5)`


### 3. Amusons-nous avec ce programme `halt`

Considérons le programme :

```python
def sym(prog):
    if halt(prog, prog) == True :
        while True :
            pass
    else :
        return 1
```

On peut remarquer que le programme `halt` est appelé avec comme paramètres `prog, prog`, ce qui signifie que `prog` se prend lui-même en paramètre. On rappelle que ce n'est pas choquant, un code source étant une donnée comme une autre.

![](img/halt3.png)

Ce programme `sym` reçoit donc en paramètre un programme `prog`, et :
- va rentrer dans une boucle infinie si `prog(prog)` s'arrête.
- va renvoyer 1 si  `prog(prog)` ne s'arrête pas.


### 4. Un léger problème ...

Puisqu'un programme peut prendre en paramètre son propre code source, que donnerait l'appel à `sym(sym)` ?

Deux cas peuvent se présenter, suivant si `halt(sym, sym)` renvoie `True` ou `False` :

![](img/halt4.png)

* **cas n°1** : `halt(sym, sym)` renvoie `True`, ce qui signifie que `sym(sym)` devrait s'arrêter. Mais dans ce cas-là, l'exécution de `sym(sym)` rentre dans une boucle infinie. C'est une contradiction.

* **cas n°2** : `halt(sym, sym)` renvoie `False` ce qui signifie que `sym(sym)`  rentre dans une boucle infinie. Mais dans ce cas-là, l'exécution de `sym(sym)` se termine correctement et renvoie la valeur 1. C'est une contradiction.

**Q10)** Ré-expliquez avec vos mots quel est le problème.


### 5. Conclusion

Nous venons de prouver que **notre programme `halt`**, censé prédire si un programme `prog` peut s'arrêter sur une entrée `x`, **NE PEUT PAS EXISTER**.

Ce résultat théorique, d'une importance cruciale, s'appelle **le théorème de l'arrêt**.

![](img/turing16.jpg)


Ce résultat a été démontré par [Alan Turing](https://fr.wikipedia.org/wiki/Alan_Turing) en 1936.  
Pour sa démonstration, il présente un modèle théorique de machine capable d'exécuter des instructions basiques sur un ruban infini, les [machines de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing).

**Q11)** Cherchez rapidement ce qu'est une machine de Turing.

À la même époque, le mathématicien [Alonzo Church](https://fr.wikipedia.org/wiki/Alonzo_Church) démontre lui aussi ce théorème de l'arrêt, mais par un moyen totalement différent, en inventant le [lambda-calcul](https://fr.wikipedia.org/wiki/Lambda-calcul).

**Q12)** Cherchez rapidement quel concept vu cette année découle du λ-calcul de Church.


## III. Calculabilité, décidabilité

### 1. Décidabilité

Turing et Church mettent ainsi un terme au rêve du mathématicien allemand [David Hilbert](https://fr.wikipedia.org/wiki/David_Hilbert), qui avait en 1928 posé la question de l'existence d'un algorithme capable de répondre «oui» ou «non» à n'importe quelle question.

Cette question, appelée **problème de décision**, est définitivement tranchée par le problème de l'arrêt : un tel théorème ne peut pas exister, puisque par exemple, aucun algorithme ne peut répondre «oui» ou «non» à la question «ce programme va-t-il s'arrêter ?».

Ce résultat démontre que toutes les questions sémantiques (non évidentes) au sujet d'un programme sont indécidables :
- «ce programme va-t-il s'arrêter ?» (le théorème de l'arrêt)
- «ce programme va renvoyer la valeur 12 ?» 
- «ce programme va-t-il un jour renvoyer un message d'erreur ?»
- ...

**Q13)** En suivant cette logique, le problème suivant est-il décidable : «ce programme est-il correct ?» ?

Au grand malheur des profs d'informatique, il n'existera donc jamais de logiciel pour corriger le code des élèves... 😞


Le problème de l'arrêt est dit **indécidable** car la fonction qui le résout (notre brave programme `halt`) n'est pas **calculable**. 

### 2. Notion de calculabilité

Qu'y a-t-il derrière cette notion de calculabilité ?

Le *calcul* mathématique peut se réduire à une succession d'opérations élémentaires. Une fonction mathématique sera dite calculable s'il existe une suite finie d'opérations élémentaires permettant de passer d'un nombre x à son image f(x).  
Idem, un algorithme peut se réduire à une suite d'opérations élémentaires, un algorithme sera dit calculable s'il existe une suite finie d'instructions élémentaires permettant de le réaliser.

**Q14)** Reformulez avec vos mots ce qu'est un algorithme calculable.

Certains algorithmes peuvent être calculables, *ou ne pas l'être* : c'est notamment le cas de notre fonction du problème de l'arrêt.

**Q15)** Et maintenant, à votre avis, peut-on donc tout programmer ?


### 3. Langages Turing-complets

On retrouve cette notion de calculabilité dans les [machines de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing). Cette machine (théorique) permet de simuler tout ce qu'un programme informatique (une suite d'instructions) est capable d'exécuter.

Turing a démontré que l'ensemble des algorithmes programmables sur sa machine était équivalent à l'ensemble des fonctions calculables avec le λ-calcul.

**Q16)** À votre avis donc, puis-je transformer n'importe quel programme récursif en programme itératif ? Et inversement ?

On peut dire que la calculabilité ne dépend pas du langage utilisé : le fait que nous ayons utilisé Python au début n'a pas d'influence sur notre démonstration. Nous savons depuis les machines de Turing que tous nos langages de programmation sont **Turing-complets** : ils sont tous capables de faire la même chose (avec plus ou moins de facilité !).

Scratch, C, Python, Java, Basic, Haskell, [Brainfuck](https://fr.wikipedia.org/wiki/Brainfuck)... tous ces langages sont théoriquement équivalents : **la calculabilité ne dépend pas du langage utilisé**.

**Q17)** Puis-je transformer mon programme Python en absolument n'importe quel langage ?

Même si vous ne programmez plus en Python dans la suite de vos études, tout ce que vous connaissez déjà pourra donc vous resservir !


### 4. Calculable, oui, mais facilement ?

L'étude de la calculabilité d'un algorithme ne se limite pas à un choix binaire : «calculable» vs «non calculable».  
Parmi les fonctions calculables, certaines peuvent l'être rapidement, et d'autre beaucoup moins.

On retrouve alors la notion bien connue de **coût** algorithmique, qui permet de classifier les algorithmes suivant leur efficacité.

**Q18)** Rappelez les coûts de tous les algorithmes suivants :
* parcours d'un tableau simple
* recherche dichotomique
* tri par sélection
* tri par insertion
* tri fusion
* recherche dans un ABR


## Pour aller plus loin -> 1 million de $ à gagner ci-dessous

On peut regrouper les problèmes suivant le coût de l'algorithme qui permet de les résoudre.

#### 1. La classe P

◮ On dira que sont de *«classe P»* tous les problèmes dont l'algorithme de recherche de solution est de *coût polynomial*.

Que retrouve-t-on dans la classe P ? Tous les problèmes dont la solution est un algorithme de complexité linéaire, quadratique, logarithmique... Tout mais surtout pas un algorithme de complexité exponentielle.

Pour le résumer très grossièrement, un problème de classe P est un problème que l'on sait résoudre en temps raisonnable (même grand). 
- le problème du tri d'une liste est dans P.
- le problème de la factorisation d'un grand nombre (sur lequel repose la sécurisation des communications) n'est *à priori pas* dans P.
- le problème de la primalité («ce nombre est-il premier ?») a longtemps été considéré comme n'étant pas dans P... jusqu'en 2002, où a été découvert le [test de primalité AKS](https://fr.wikipedia.org/wiki/Test_de_primalit%C3%A9_AKS), de complexité polynomiale (d'ordre 6). Ce test est donc maintenant dans P.


#### 2. La classe NP

◮ On dira que sont de *«classe NP»* tous les problèmes dont l'algorithme de recherche de solution est *Non-déterministe Polynomial*.

Que veut dire la formulation «non-déterministe polynomial» ? Cela fait référence à ce que serait capable de faire une machine de Turing (donc, n'importe quel ordinateur) travaillant de manière *non-déterministe*, donc capable d'explorer simultanément plusieurs solutions possibles. On peut imaginer un arbre dont le parcours se ferait simultanément dans toutes les branches, et non en largeur ou profondeur comme nous l'avons vu.

Sur une machine non-déterministe, si la solution à un problème se trouve en temps polynomial, alors ce problème appartient à la classe NP. 

Très bien, mais les machines non-déterministes... cela n'existe pas réellement. Comment caractériser concrètement cette classe de problème ?

Si la solution peut être trouvée de manière polynomiale par une machine non-déterministe, une machine déterministe *qui aurait de la chance* en partant directement vers la bonne solution la trouverait elle aussi de manière polynomiale. On simplifie souvent cela en disant «la vérification de la solution est polynomiale». Cela nous donne cette définition plus accessible de la classe NP :

◮ On dira que sont de *«classe NP»* tous les problèmes dont l'algorithme de *vérification* de solution est *polynomial*.

Pour le résumer très grossièrement, un problème de classe NP est un problème dont on sait vérifier facilement si une solution proposée marche ou pas :

- la résolution d'un sudoku est dans NP : si quelqu'un vous montre un sudoku rempli, vous pouvez très rapidement lui dire si sa solution est valable ou pas.
- la factorisation d'un nombre est dans NP : si quelqu'un vous propose `4567 \ times 6037` comme décomposition de 27570979, vous pouvez très rapidement lui dire s'il a raison. (oui.)
- le problème du sac à dos (vu en première) est dans NP : une proposition de butin peut facilement être examinée pour savoir si elle est possible ou non.

Malheureusement, aucun de ces problèmes cités n'a (à ce jour) d'algorithme de *résolution* meilleur qu'exponentiel...

#### 3. P = NP, ou pas ?

Tous les problèmes de P ont une solution qui peut être *trouvée* de manière polynomiale. Donc évidemment, la vérification de cette solution est aussi polynomiale. Donc tous les problèmes de P sont dans NP. On dit que P est inclus dans NP, que l'on écrit *P ⊂ NP*.

Voici une capture d'écran de l'excellente vidéo [Nos algorithmes pourraient-ils être BEAUCOUP plus rapides ? (P=NP ?)](https://www.youtube.com/watch?v=AgtOCNCejQ8) de David Louapre :

![](img/louapre.png)

On y retrouve (en vert) la classe P, qui contient les algorithmes de tri.
En blanc, la classe NP, qui contient les problèmes de factorisation, du sudoku, du sac-à-dos...

Si quelqu'un trouve un jour un algorithme de polynomial de factorisation, alors le problème de factorisation viendra se ranger dans P. (accessoirement, le RSA sera sans doute détruit par cette découverte, sauf si l'ordre de complexité est très grand)

Mais certains de ces problèmes dans NP ont une propriété remarquable : la résolution polynomiale d'un seul d'entre eux ferait ramener la *totalité* des problèmes NP dans P. On dit que ces problèmes sont NP-complets (marqués en rouge ci-dessus).  
Concrètement, si vous trouvez une solution polynomiale de résolution du sudoku, vous entrainez avec lui dans P tous les autres problèmes NP, et vous aurez ainsi prouvé que P = NP.  
Accessoirement, vous gagnerez aussi le prix d'[un million de dollars](https://fr.wikipedia.org/wiki/Probl%C3%A8mes_du_prix_du_mill%C3%A9naire) promis par la fondation Clay à qui tranchera cette question... **prix que vous partagerez bien évidemment avec votre professeur de NSI ;)**


Actuellement, à part le grand [Donald Knuth](https://fr.wikipedia.org/wiki/Donald_Knuth), la plupart des chercheurs qui travaillent à ce problème sont plutôt pessimistes, et pensent que P ≠ NP. 
Cela signifie qu'ils pensent que certains problèmes ne pourront jamais avoir une solution polynomiale. 

Alors, P = NP ou P ≠ NP ? Réponse peut-être un jour...

---

Source : [G. Lassus](https://github.com/glassus/nsi)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
