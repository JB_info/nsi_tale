```python
# ------------------------------ Mémento Python (rappels de 1ère) ------------------------------

# Python a été créé en 1991 par Guido van Rossum.

# On utilise Python via un IDE (environnement de développement), comme Thonny par exemple.
# Un IDE est généralement constitué d'une zone "éditeur" et d'une zone "console".
# On écrit nos programmes dans l'éditeur.
# Pour consulter la valeur d'une variable ou le résultat d'une fonction, on utilise la console.

# Un commentaire est une ligne de code qui ignorée par l'interpréteur (elle n'est pas exécutée).
# Un commentaire commence par le symbole dièse (#).
# Il est primordial de mettre des commentaires dans son code pour expliquer ce que l'on fait.


# -------------------- Types de base --------------------

# On obtient le type d'une valeur avec la fonction `type` :
type(2) # int
type(4.2) # float
type(True) # bool

# ---------- int ----------

# int = nombres entiers (positifs et négatifs)
# exemples :    7    42    0    -1    -250

# Opérations :
18 + 3 # 21 (addition)
18 - 3 # 15 (soustraction)
18 * 3 # 54 (multiplication)
18 / 3 # 6.0 (division flottante)
18 // 3 # 6 (quotient de la division entière)
18 % 3 # 0 (modulo, reste de la division entière)
18 ** 3 # 5832 (puissance)

# ---------- float ----------

# float = nombres flottants (réels positifs et négatifs)
# exemples :    4.2    411.36    2.0    -1.5    -42.0

# Opérations :
3.6 + 2.0 # 5.6 (addition)
3.6 - 2.0 # 1.6 (soustraction)
3.6 * 2.0 # 7.2 (multiplication)
3.6 / 2.0 # 1.8 (division flottante)
3.6 // 2.0 # 1.0 (quotient de la division entière)
3.6 % 2.0 # 1.6 (modulo, reste de la division entière)
3.6 ** 2.0 # 12.96 (puissance)

# ---------- bool ----------

# bool = booléens
# 2 valeurs :    True    False

# Opérations :
True or False # True (ou -> l'un des deux est vrai)
True and False # False (et -> les deux sont vrais)
not True # False (non -> inverse de la valeur)

# ---------- None ----------

# None = valeur particulière qui symbolise l'absence de valeur

# ---------- conversions ----------

# Il est possible de convertir une valeur d'un type à un un autre
int("1") # 1
int(1.2) # 1
float(3) # 3.0
str(1) # "1"
str(1.2) # "1.2"


# -------------------- Affectations --------------------

# L'affectation permet d'associer une valeur à une variable
# Le nom d'une variable ne peut contenir que les caractères a...z A...Z 0....9 _
# et ne peut pas débuter par un chiffre.
# Le symbole pour l'affectation est =
nombre = 2
vrai = True


# -------------------- Itérables --------------------

# Un itérable est une valeur que l'on peut parcourir.
# Exemples d'itérable : chaines de caractères, tuples, tableaux, dictionnaires, intervalles

# On obtient le nombre d'éléments d'un itérable avec la fonction `len` :
len("nsi") # 3
len((1, 2, 3, 4)) # 4
len([1, 2, 3, 4, 5]) # 5
len({"cle1" : 1, "cle2" : 2}) # 2
len(range(4)) # 4

# ---------- chaines de caractères ----------

# str = chaines de caractères (encadrées par des " ou des ')
# exemples :    "a"    'b'    "n s i"    '1nf0rm@t1qu€'    ''
# caractère spécial : "\n" = retour à la ligne

# Opérations
"n" + "si" # "nsi" (concaténation)
"oh" * 3 # "ohohoh" (duplication)

# Accès aux caractères d'une chaine : nom_de_la_chaine[indice]
# Les indices vont de 0 à la longueur de la chaîne - 1
chaine = "nsi"
chaine[0] # "n"
chaine[1] # "s"
chaine[2] # "i"

# ---------- tuples ----------

# tuples = suites d'éléments non modifiables encadrés par des ( ) séparés par des ,
# exemples :    (1, 2)    (5, )    ("n", "s", 1)    ()

# Opérations
('n', ) + ('s', 1) # ('n', 's', 1) (concaténation)
(1, ) * 3 # (1, 1, 1) (duplication)

# Accès aux éléments d'un tuple : nom_du_tuple[indice]
# Les indices vont de 0 à la longueur du tuple - 1
tup = ("n", "s", 1)
tup[0] # "n"
tup[1] # "s"
tup[2] # 1

# ---------- tableaux ----------

# tableaux = suites d'éléments modifiables encadrés par des [ ] séparés par des ,
# exemples :    [1, 2]    [5]    ["n", "s", 1]    []

# Opérations
['n'] + ['s', 1] # ['n', 's', 1] (concaténation)
[1] * 3 # [1, 1, 1] (duplication)

# Accès aux éléments d'un tableau : nom_du_tableau[indice]
# Les indices vont de 0 à la longueur du tuple - 1
tab = ["n", "s", 1]
tab[0] # "n"
tab[1] # "s"
tab[2] # 1

# Modification des éléments d'un tableau : nom_du_tableau[indice] = nouvelle valeur
tab = [0, 1, 2, 3]
tab[1] # 1
tab[1] = 7
tab[1] # 7

# Méthodes sur les tableaux :
# ajout d'un élément
tab = [1, 2, 3]
tab.append(4)
tab # [1, 2, 3, 4]
# suppression du dernier élément
tab = [1, 2, 3]
tab.pop()
tab # [1, 2]
# tri du tableau (modifie le tableau, ne renvoie rien)
tab = [2, 1, 4, 3]
tab.sort()
tab # [1, 2, 3, 4]
# inversion du tableau (modifie le tableau, ne renvoie rien)
tab = [1, 2, 3, 4]
tab.reverse()
tab # [4, 3, 2, 1]

# Création d'un tableau par compréhension :
tab = [x for x in range(4)] # [0, 1, 2, 3]

# ---------- dictionnaires ----------

# dictionnaire (dict) = ensemble d'associations clé->valeur
# exemples :    {"clé1" : 1, "clé2" : 2, "clé3" : 3}    {1 : 'un'}
#               {'prenom' : 'hermione', 'nom' : 'granger', 'age' : 15}    {}

# Accès à une valeur à partir de sa clé : nom_du_dictionnaire[clé]
# attention : pas d'indices dans un dictionnaire !
dico = {'prenom' : 'hermione', 'nom' : 'granger', 'age' : 15}
dico['prenom'] # 'hermione'
dico['age'] # 15
dico['nom'] # 'granger'
# Modification de la valeur associée à une clé : nom_du_dictionnaire[clé] = nouvelle_valeur
dico = {'prenom' : 'hermione', 'nom' : 'granger', 'age' : 15}
dico['age'] # 15
dico['age'] = 16
dico['age'] # 16

# Méthodes sur les dictionnaires :
# donne toutes les clés
dico.keys()
# donne toutes les valeurs
dico.values()

# ---------- intervalles ----------

# intervalles = suite d'entiers

# Construction d'un intervalle :
# range(fin) : entiers de 0 inclus à fin exclus
range(3) # 0, 1, 2
# range(debut, fin) : entiers de debut inclus à fin exclus
range(3, 7) # 3, 4, 5, 6
# range(debut, fin, pas) : entiers de debut inclus à fin exclus en allant de pas en pas
range(1, 11, 2) # 1, 3, 5, 7, 9

# ---------- conversions ----------

# Tableau -> Chaine : `join`
':'.join(['a','b','c']) # 'a:b:c'
','.join(['1','2','3']) # '1,2,3'
# Chaine -> Tableau : `split`
'1,2,3'.split(',') # ['1', '2', '3']
"j'aime la nsi !".split() # ["j'aime", 'la', 'nsi', '!']


# -------------------- Comparaisons --------------------

# Opérateurs pour comparer deux valeurs :    <    >    <=    >=    ==    !=
2 == 1 + 1 # True
2 != 1 + 1 # False
3 < 3 # False
3 <= 3 # True
# attention : ON NE COMPARE PAS DEUX FLOTTANTS
0.1 + 0.2 == 0.3 # False

# Opérateur pour l'appartenance d'un élément à un itérable : `in`
3 in [1, 2, 3, 4] # True
'a' in ('n', 's', 'i') # False


# -------------------- Instructions conditionnelles (if) --------------------

# Une instruction conditionnelle (ou "test") permet d'effectuer différents blocs de code
# en fonction d'une condition donnée.

# if condition1:
#     bloc à effectuer si condition1 vraie
# elif condition2:
#     bloc à effectuer si condition 1 fausse mais condition2 vraie
# else:
#     bloc à effectuer si toutes les autres conditions sont fausses

# exemple :
if age <= 18:
	statut = "mineur"
elif age > 65:
    statut = "retraité"
else:
    statut = "adulte"
    
    
# -------------------- Boucle bornée (for) --------------------

# Une boucle bornée permet de répéter un bloc de code un nombre connu de fois.

# for valeur in iterable:
#     bloc à effectuer sur chaque valeur

# exemples :
compteur = 0
for nombre in [1, 2, 3, 4]: # nombre vaut 1, puis 2, puis 3, puis 4
    compteur = compteur + 1
    
compteur = 0
for i in range(10): # i vaut 0, puis 1, puis 2, puis ..., puis 8, puis 9
    compteur = compteur + 1

# -------------------- Boucle non bornée (while) --------------------

# Une boucle non bornée permet de répéter un bloc de code tant qu'une condition est vraie.

# while condition:
#     bloc à effectuer si condition vraie

# exemple :
somme = 0
nombre = 0
while nombre != 10: # condition avec au moins une variable qui change dans la boucle (ici nombre)
    somme = somme + nombre
    nombre = nombre + 1 # ne pas oublier de faire varier la variable de condition
    
# -------------------- Bibliothèques --------------------

# Une bibliothèque (ou "module") fournit d'autres fonctions.
# Il faut l'importer pour pouvoir l'utiliser.

# from bibliotheque import fonction1, fonction2
# -> accès direct
# ou
# import bibliotheque
# -> accès via bibliotheque.fonction1

# exemples:
from math import sqrt
sqrt(4) # 2.0

import math
math.sqrt(4) # 2.0


# -------------------- Fichiers --------------------

fichier = open("nom_fichier.extension", mode) # mode = 'r' (lecture) ou 'w' (écriture)
fichier.readline() # lit une ligne (renvoie une chaine de caractères)
fichier.readlines() # lit toutes les lignes (renvoie un tableau)
fichier.read() # lit tout le fichier (renvoie une chaine de caractères)
fichier.write("texte") # écrit "texte" dans le fichier
fichier.close() # ferme le fichier


# -------------------- Fonctions --------------------

# ---------- Définir une fonction ----------

# def nom_fonction(param1, param2):
#     """
#     Brève description de la fonction
#     
#     :param1: (type) description de param1
#     :param2: (type) description de param2
#     :return: (type) description de ce que renvoie la fonction
#     
#     >>> un_test
#     le_résultat_attendu
#     >>> un_autre_test
#     le_résultat_attendu
#     """
#     code de la fonction
#     return resultat

# ---------- Appeler une fonction ----------

# Pour consulter le résultat d'une fonction, on effectue un appel dans la console :
# >>> nom_fonction(valeur1, valeur2)
# resultat

# exemple :
# dans l'éditeur :
def somme(nombre1, nombre2):
    """
    Ajoute deux nombres.
    
    :param nombre1: (int) un nombre
    :param nombre2: (int) un second nombre
    :return: (int) la somme de nombre1 et nombre2
    
    >>> somme(4, -1)
    3
    >>> somme(2, 3)
    5
    """
    addition = a + b
    return addition

# dans la console :
>>> somme(1, 6)
7
```



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
