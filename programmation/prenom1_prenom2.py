# DM sur la récursivité

# NOM PRENOM 1
# NOM PRENOM 2


# ----------------------------------------------------------------------------------------------------------------------- 
# Ex 1 : PGCD

def pgcd(a, b):
    """
    documentation et doctests ici
    """
    # assertions :
    assert ...
    
    # programme :
    A COMPLETER
    
    
# -----------------------------------------------------------------------------------------------------------------------     
# Ex 2 : Tri par sélection

# Principe du tri (explications en français, sans code) :
# A COMPLETER

# Définition récursive :
# tri(tableau de taille n) =
# A COMPLETER

def tri_selection(tableau):
    """
    documentation et doctests ici
    """
    # assertions :
    assert ...
    
    # programme :
    A COMPLETER
    
    
# -----------------------------------------------------------------------------------------------------------------------    
# Ex 3 : Chiffres romains

# Définition récursive :
# romain (chaine de n symboles) =
# A COMPLETER

# dictionnaire des valeurs de chaque symbole romain
dico_valeurs = {"M" : 1000, "D" : 500, "C" : 100, "L" : 50, "X" : 10, "V" : 5, "I" : 1}

def romain(chaine):
    """
    documentation et doctests ici
    """
    # assertions :
    assert ...
    
    # programme :
    A COMPLETER
    

# ----------------------------------------------------------------------------------------------------------------------- 
# Ex 4 : Autour de Koch

# Définition récusive:
# koch(l, n) =
# A COMPLETER

import turtle

def courbe_koch(l, n):
    """
    documentation (pas de doctests ici)
    """
    # assertions :
    assert ...
    
    # programme :
    A COMPLETER


# code pour exécuter les doctests :
if __name__ == '__main__':
    import doctest
    doctest.testmod()