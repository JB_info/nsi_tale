# Programmation orientée objet



La **programmation orientée objet** (POO, au programme de Terminale) est considérée comme le **paradigme** de programmation le plus efficace et est massivement utilisée aujourd’hui. Un paradigme est une *approche logique qu'un programmeur va adopter pour résoudre son problème*. Tout programmeur se doit de connaître la POO. Elle permet de structurer et organiser un programme de façon à faciliter sa compréhension et à le rendre plus maintenable. Pour comprendre la POO et bien cerner ces avantages, il faut commencer par un exemple.

Nous allons réaliser un programme très simple qui crée deux personnages effectuant un combat. Ils ont chacun un nom et une force, celui qui a la force la plus grande gagne le combat. Pour donner le résultat d'un combat, on affiche le nom du vainqueur.

Nous verrons seulement les concepts les plus basiques de la POO. Les aspects les plus puissants de la POO (encapsulation, héritage…) ne sont pas au programme de la terminale NSI.

Pour commencer, nous allons faire quelques révisions indispensables sur la programmation en Python. Nous ferons évoluer notre programme jusqu'à la programmation orientée objet.



## I. Programmation impérative

Dans un premier temps nous allons utiliser le paradigme de programmation **impératif**. Pas de panique : c’est ce que vous savez faire. Il y a plusieurs façons de faire ce programme, nous allons commencer par la moins efficace.

### Méthode 1

​	**1)** Complétez le code ci-dessous pour afficher le nom du vainqueur après avoir fait un test pour déterminer le vainqueur :

```python
p1_nom = "Murzol"
p1_force = 50
p2_nom = "Grunak"
p2_force = 40
```

### Méthode 2

Nous allons maintenant faire un code un peu plus « propre » en utilisant un tableau pour chaque personnage et une fonction pour faire le combat.

​	**2)** Complétez le code ci-dessous pour réaliser ce qui est demandé (attention, on n'affiche plus le nom du vainqueur, on le renvoie) :

```python
p1 = ["Murzol", 50]
p2 = ["Grunak", 40]

def combat(p1, p2):
…
```

Qu’avons-nous amélioré ici : 

- une seule variable par personnage ;
- possibilité d’ajouter des caractéristiques simplement ;
- possibilité de faire un autre combat sans nouveau code (fonction).

### Méthode 3

Nous allons maintenant améliorer notre code en utilisant des dictionnaires au lieu des tableaux.

​	**3)** Complétez le code ci-dessous pour réaliser toujours le même programme :

```python
p1 = {"nom":"Murzol", "force":50}
p2 = {"nom":"Grunak", "force":40}

def combat(p1, p2):
…
```

Qu’avons-nous amélioré ici : 

- code plus lisible ;
- programmation plus facile car le nommage des caractéristiques est explicite.



## II. Rappel sur les docstrings en Python

Nous faisons une petite pause dans l'évolution de notre programme pour parler des **docstrings**. Les docstrings sont la documentation de votre programme. Ils ne changent absolument pas le fonctionnement du programme mais permettent aux utilisateurs et développeurs de votre programme de mieux le comprendre.

La documentation est souvent négligée par les développeurs car on veut souvent aller vite et nous avons l'impression que cela ne sert à rien. **Erreur !** La documentation permet d'éviter de nombreux bugs car le code est plus compréhensible. De plus, il est fort probable que ça soit vous qui essayerez de comprendre ce code dans plusieurs semaines ou mois. La documentation vous permettra de gagner beaucoup de temps.

En effet, lorsqu'on écrit un programme tout est frais et logique dans notre tête, mais plusieurs mois après, il peut être difficile de comprendre notre propre code sans explications. Pour une personne extérieure cela peut même être impossible. Voilà pourquoi il est indispensable de documenter son code avec des commentaires et des docstrings !

Que cela soit un rappel ou non, voici le fonctionnement des docstrings.

### Qu'est-ce qu'une docstring ?

Une docstring est une chaîne de caractères avec trois doubles quotes `"""` situées au début d'un module, d'une fonction, d'une classe (on verra cela plus tard) ou d'une méthode (on verra aussi cela plus tard). Elle explique ce que fait le code et donne ce que retourne une fonction ou une méthode.

Une docstring s'écrit ainsi :

- elle est encadrée par trois apostrophes **'''** (ou bien par trois guillemets **"""**)
- la première ligne donne une brève description de ce que fait la fonction
- pour chaque paramètre, on crée une ligne qui contient **:param nom_du_parametre: (type du paramètre)** suivi d'une brève description du paramètre
- pour le résultat on crée une ligne qui contient **:return: (type du résultat)** suivi d'une brève description du résultat

Regardons donc un exemple pour la fonction combat :

```python
def combat(p1, p2):
    """
    Effectue un combat entre deux personnages et renvoie le nom du vainqueur.
    
    :param p1: (dict) le premier personnage
    :param p2: (dict) le second personnage
    :return: (str) le nom du vainqueur
    """
…
```

Cette chaîne de caractères sera ignorée lors de l'exécution du programme. Par contre elle sera utilisée lorsqu'on demande de l'aide sur une fonction avec `help()`.

​	**4)** Ajoutez la docstring à votre fonction `combat()` et vérifiez qu'elle apparait lorsque vous appelez `help(combat)`.

### Sources et compléments

- [Docstrings conventions](https://www.python.org/dev/peps/pep-0257/) : la page officelle de Python sur les docstrings



## III. Rappel sur les tests en Python

Que vous l'ayez vu en première ou non, il est temps en terminale de savoir effectuer des tests en Python.

### Pourquoi faire des tests ?

Un test permet de dire à Python le résultat attendu d'une fonction (ou autre) pour des paramètres donnés. Cela permet de vérifier qu'une fonction fait bien ce qui lui est demandé. C'est particulièrement utile, lorsqu'on modifie légèrement un programme complexe, pour s'assurer que le reste du code n'est pas « cassé » suite à ce changement. Ainsi, on lance les tests et si tout fonctionne, on peut raisonnablement penser que nous n'avons pas ajouté d'erreurs.

### Comment faire des tests ?

Il existe plusieurs façons de faire des tests en Python. On peut utiliser des bibliothèques telles de [pytest](https://docs.pytest.org/) ou [unitest](https://docs.python.org/fr/3/library/unittest.html). Ces deux bibliothèques sont assez puissantes mais demandent des connaissances un peu plus avancées que les **doctests** que nous allons utiliser.

Les doctests s'intègrent dans les docstrings de Python. Même si cela peut paraître déroutant au début, cela reste la méthode la plus simple pour effectuer des tests en Python. Pour comprendre commençons par un exemple :

```python
def somme(a, b):
    """
    # Ici c’est la documentation de la fonction
    Ajoute deux nombres.
    
    :param a: (int) un nombre
    :param b: (int) un second nombre
    :return: (int) la somme de a et b
    
    # Le test avec son résultat
    >>> somme(2, 2)
    4
    """
    return a + b

# On demande à Python de réaliser les tests
if __name__ == "__main__":
    import doctest
    doctest.testmod()
```

​	**5)** Testez ce programme. Il ne doit rien se passer.

​	**6)** Remplacez maintenant le contenu de la fonction par `return a + b + 1` et relancez le test. Un message doit alors annoncer que le test a échoué.

Les doctests se composent ainsi :

```python
"""
>>> une_instruction
le_résultat_attendu
"""
```

On peut en mettre plusieurs et les séparer par d'autres textes. Il faut juste sauter une ligne après un test si on veut mettre du texte :

```python
def somme(a, b):
    """
    Ajoute deux nombres..
    
    :param a: (int) un nombre
    :param b: (int) un second nombre
    :return: (int) la somme de a et b
    
    >>> somme(2, 2)
    4
    >>> somme(0, 5)
    5
    
    Je peux ajouter un texte et ensuite faire un nouveau test
    >>> somme(-1, 1)
    0
    
    Et encore un texte
    """
    return a + b
```

Les doctests participent ainsi à la documentation en donnant des exemples concrets d'utilisation de la fonction.

​	**7)** Ajoutez des doctest à la fonction `combat()` de votre programme.

### Sources et compléments

- [Documentation officielle de doctest](https://docs.python.org/3/library/doctest.html) : la page officelle de Python sur les docstrings



## IV : Programmation objet

### Classe de base

Il est temps de voir la POO. Nous allons créer une **classe** Personnage qui va contenir tout ce dont un personnage a besoin. Les classes permettent de construire les objets de la programmation orientée objet.

Voici le code permettant de déclarer une classe Personnage :

```python
class Personnage:

    def __init__(self, nom, force):
        self.nom = nom
        self.force = force
```

Quelques explications :

- le nom d’une classe doit toujours commencer par une majuscule ;
- les fonctions définies à l’intérieur d’une classe sont appelées **méthodes** ;
- une méthode possède nécessairement un paramètre appelé self en premier. C’est une référence d’instance, nous verrons cela plus bas. Ce paramètre n’apparaît pas lors de l’appel de la méthode ;
- une classe possède des **attributs** (ou variables d’instance) qui doivent être appelés avec le préfixe self. La classe Personnage ci-dessus possède donc deux attributs `nom` et `force` ;
- une classe possède une méthode spéciale __init__() nommée constructeur qui est appelée lorsqu’on crée une instance de cette classe.

Comment utiliser cette classe ? Il faut l’instancier ! Ce n’est pas très compliqué :

```python
p1 = Personnage("Murzol", 50)
```

`p1` est alors une instance de la classe Personnage : c'est à dire un objet créé à partir de la classe Personnage. Les arguments `"Murzol"` et `50` ont été envoyés au constructeur qui les a affecté aux bons attributs (self est un paramètre « fantôme »).

Nous pouvons alors accéder aux attributs de notre instance (même si c’est déconseillé de faire ainsi) :

```python
print(p1.nom)
print(p1.force)
```

Qu’avons-nous amélioré ici ?

- Le code est plus sûr : tous les personnages auront les mêmes caractéristiques par construction ;
- Le code est plus sûr : comme il est très déconseillé de modifier les attributs d’une classe, il y a peu de chance que le nom et la force soient modifiés par erreur.

### Ajout d’une méthode

Pour découvrir encore un avantage de la POO, nous allons créer une méthode qui va permettre d’afficher les caractéristiques du personnage.

​	**8)** Écrivez une méthode `affiche()` qui va permettre d’afficher pour `p1` ses caractéristiques sous cette forme :

```python
nom : Murzol | force : 50
```

On fera appel à cette méthode avec la commande `p1.affiche()`.

Qu’avons-nous amélioré ici ?

- le code est mieux structuré : la méthode d’affichage est dans la classe Personnage. Il sera plus facile de la retrouver.

### Ajout d’un docstring

Comme nous l'avons vu pour les fonctions, il est recommandé d’ajouter une description de la classe en utilisant une chaîne de caractères directement sous le nom de la classe. Cette chaîne de caractère doit utiliser trois guillemets `"""`. Par exemple on pourra mettre ceci :

```python
class Personnage:
    """
    Personnage quelconque avec un nom et une force
    """
```

Ce n’est pas indispensable mais c’est une bonne habitude à prendre. Et pourquoi ne pas mettre simplement un commentaire ? Car de cette façon votre docstring sera affichée en cas d’appel à `help(Personnage)` pour afficher l’aide.

​	**9)** Ajoutez la docstring ci-dessus et affichez l’aide de la classe.

Qu’avons-nous amélioré ici ?

- le code est plus facilement maintenable.

### Ajout d’une méthode de combat

Pour faire combattre nos deux personnages, nous allons créer une méthode combat qui prend en paramètre self (comme toujours) et une deuxième instance contre qui faire le combat. Comme avant, cette méthode doit renvoyer le nom du vainqueur.

​	**10)** Complétez la définition de la fonction combat ci-dessous :

```python
def combat(self, combattant):
```

​	**11)** Testez cette méthode avec deux instances `p1` et `p2` pour Murzol et Grunak.

Qu’avons-nous amélioré ici ?

- le code est mieux structuré : la méthode de combat est dans la classe Personnage. Il sera plus facile de la retrouver.



## V. Exercices

### Exercice 1

​	**1)** Créez une classe `Point` qui possède deux attributs `x` et `y` correspondant aux coordonnées du point. Ces deux attributs doivent être affectés par le constructeur.

​	**2)** Ajoutez une méthode `affiche()` qui affiche les coordonnées du point comme ci-dessous (ici, x = 2 et y = 3) :

```none
Point | x : 2 | y : 3
```

​	**3)** Ajoutez une méthode `translater(tx, ty)` qui ajoute `tx` à `x` et `ty` à `y`.

### Exercice 2

​	**1)** Créez une classe `Compte` modélisant un compte en banque. La classe possède deux attributs `somme` et `taux` correspondant à la somme placée sur le compte et au taux d'intérêt. Ces deux attributs doivent être affectés par le constructeur.

​	**2)** Ajoutez une méthode `affiche()` qui affiche la somme et le taux comme ci-dessous :

```none
Compte | somme : 2000€ | taux : 2%
```

​	**3)** Ajoutez une méthode `depot(x)` qui ajoute `x` à la somme sur le compte.

​	**4)** Ajoutez une méthode `retrait(x)` qui enlève `x` à la somme sur le compte. Elle pourra renvoyer un erreur si la somme sur le compte devient négative et annuler alors l'opération.

​	**5)** Ajoutez une méthode `interets()` qui calcule les intérêts perçus en un an et les ajoute à la somme placée. Pour rappel les intérêts se calculent avec la formule $`interets = \frac{taux \times somme}{100}`$.

## VI. Pour aller plus loin (Hors programme)

Les notions les plus « puissantes » et donc plus complexes de la programmation orientée objet ne sont pas au programme du lycée. Cette partie va vous présenter deux notions de la POO pour que les plus rapides d'entre vous puissent apercevoir la richesse de la POO. Ne faites cette partie que si vous avez terminé les exercices.

Il y a trois notions fondamentales de la POO qui ne sont pas au programme du lycée :

- l'encapsulation : cela regroupe un ensemble de règles d'accès aux attributs d'une classe. Cette notion qui n'est pas implémentée nativement en Python permet de rendre le code plus sûr ;
- l'héritage : il permet de créer une classe fille, dérivant (héritant) d'une classe mère, avec des méthodes et attribut supplémentaires ;
- le polymorphisme : il permet schématiquement d'avoir des méthodes avec le même nom qui font des choses différentes.

Nous allons aborder très simplement l'héritage et le polymorphisme.

### Création de nouveau types de personnages

Nous allons créer deux types de personnages, les guerriers et les magiciens qui vont hériter de la classe `Personnage` et utiliser le polymorphisme pour adapter la méthode `affiche()`.

Voici la classe magicien :

```python
class Magicien(Personnage):
    """
    Personnage avec des pouvoirs magiques
    """

    def __init__(self, nom, force, magie):
        Personnage.__init__(self, nom, force)
        self.magie = magie

    def affiche(self):
        print("nom : " + self.nom + " | force : " + str(self.force) + " | magie : " + str(self.magie))
```

Cette nouvelle classe Magicien hérite de la classe Personnage car on a utilisé Magicien(Personnage). On dira que Magicien est une classe fille et Personnage une classe mère. Cela veut dire qu’elle possède les mêmes attributs et méthodes que la classe Personnage. On peut néanmoins lui ajouter des attributs comme avec l’instruction `self.magie = magie`. L’instruction `Personnage.__init__(self, nom, force)` permet de faire appel au constructeur de la classe mère Personnage (et donc d’initialiser le nom et la force ici).

Nous redéfinissons la méthode affiche() pour faire apparaître la magie. C’est ce qu’on appelle le *polymorphisme*.

​	**9)** Testez la classe Magicien en utilisant le code suivant :

```python
p1 = Personnage("Murzol", 50)
p1.affiche()
p3 = Magicien("Gandalf", 100, 500)
p3.affiche()
p3.combat(p1)
```

​	**10)** À vous de créer une classe Guerrier héritant de Personnage. Le guerrier possède un attribut « clan » en plus des attributs de la classe Personnage.

---

Cours rédigés par Thomas Beline : [kxs](https://kxs.fr/cours/)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)