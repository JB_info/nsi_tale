import random
from turtle import *
from time import sleep
from math import sqrt

# ------------------------------------------------------------------------

# VOUS AUREZ BESOIN DE TOUTES CES POSITIONS ET TAILLES POUR LE DESSIN :
# (ne pas modifier cette partie)

# taille du crayon
crayon = 5
# tailles pour le mot
taille_lettre = 24
taille_espace = taille_lettre // 2
# position (x, y) en bas à gauche du mot
x_mot_bg = -300
y_mot_bg = +50
# position (x, y) en bas à gauche du pendu
x_pendu_bg = -300
y_pendu_bg = -250
# tailles de la potence
bas = 200
corde = 40
gauche = bas + corde
haut = gauche // 2
# tracer le triangle en haut à gauche de la potence
x_triangle = x_pendu_bg
y_triangle = y_pendu_bg + bas
taille_triangle = sqrt(2) * corde
angle_triangle = 45
# tailles des traits du pendu
rayon_tete = 30
taille_bras_jambe = sqrt(2) * rayon_tete
taille_corps = rayon_tete * 2
# position pour le cercle de la tête
x_tete = x_pendu_bg + haut
y_tete = y_pendu_bg + bas - 2 * rayon_tete
# position pour le trait du corps
x_corps = x_tete
y_corps = y_tete
# position pour les bras
x_bras = x_tete
y_bras = y_tete - 10
# position pour les jambes
x_jambe = x_tete
y_jambe = y_tete - 2 * rayon_tete
# position pour le visage
rayon_oeil = 4
x_oeil_gauche = x_tete - 10
y_oeil_gauche = y_triangle - 25
x_oeil_droit = x_tete + 10
y_oeil_droit = y_oeil_gauche
rayon_sourire = -rayon_tete
taille_sourire = 2 * rayon_tete
x_sourire = x_oeil_gauche - 5
y_sourire = y_oeil_gauche - 20

# ------------------------------------------------------------------------

# POUR CHAQUE FONCTION, AIDEZ VOUS DE LA DOCUMENTATION
# POUR ECRIRE SON CODE

def mot_aleatoire_dictionnaire():
    """
    * Ouvre le fichier "dictionnaire.txt"
    * lis toutes les lignes
    * en choisis une au hasard (vous pouvez utiliser random.choice(lignes))
    * enlève le \n à la fin de la ligne choisie (vous pouvez utiliser ligne.rstrip("\n"))
    * ferme le fichier
    * renvoie la ligne
    
    :return: (str)
    """
    # COMPLETER ICI


def creer_masque(mot):
    """
    Crée un masque pour le mot,
    c'est-à-dire une chaîne de caractères
    où toutes les lettres sont remplacées par un tiret.    
    Par exemple un masque pour "TEST" est "----".
    
    :param mot: (str)
    :return: (str)
    """
    # COMPLETER ICI


def initialise_affichage(mot):
    """
    * Cache la tortue
    * Initialise la taille du crayon
    * Se déplace à la bonne position puis dessine autant de traits que de lettres dans le mot
    * Se déplace à la bonne position puis dessine la potence
    
    Vous pouvez utiliser setheading(0) pour revenir à une direction EST.
    """
    hideturtle()
    pensize(crayon)
    # COMPLETER ICI


def demande_lettre():
    """
    * demande à l'utilisateur d'entrer une lettre (vous pouvez utiliser lettre = textinput(titre, question))
    * si la lettre est None, fermer Turtle puis quitter (exit())
    * si l'utilisateur a saisi plus qu'une lettre ou a entré un caractère non valide, repose la question
    * transforme la lettre en majuscule
    * renvoie la lettre
    
    :return: (str)
    """
    lettres_valides = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    # COMPLETER ICI
    

def modifier_masque(mot, masque, lettre):
    """
    Modifie le masque pour y faire apparaître toutes les lettres.
    Par exemple le masque "T--T-R" du mot "TESTER" avec la lettre "E" est modifié en "TE-TER".
    """
    # COMPLETER ICI
    

def ajouter_lettre(mot, lettre):
    """
    Ajoute la lettre à la fenêtre Turtle au dessus des _ correspondants dans le mot.
    Par exemple pour le mot "TESTER" et la lettre "E",
    il faut écrire dans Turtle les deux "E"
    au dessus du deuxième et du cinquième trait.
    
    * se déplace au dessus du 1er trait
    * pour chaque lettre du mot
    * si cette lettre correspond à celle cherchée, écrire la lettre à la position actuelle
    * se déplacer vers la droite à la position suivante
    
    Pour écrire en turtle : write(lettre, False, "left", ("Arial", 24 ,"normal"))
    
    :param mot: (str)
    :param lettre: (str)
    """
    up()
    goto(x_mot_bg + 3, y_mot_bg + 3) # permet de centrer les lettres par rapport au trait dessous
    down()
    
    # COMPLETER ICI


def ajouter_trait(nb_erreurs):
    """
    Ajoute un trait supplémentaire au pendu
    en fonction du nombre d'erreurs.
    
    -> pour dessiner le sourire, se déplacer à la bonne position puis utiliser :
    * left(-rayon_sourire)
    * circle(rayon_sourire, taille_sourire)
    
    :param nb_erreurs: (int)
    """
    if nb_erreurs == 1:
        up()
        goto(x_tete, y_tete)
        down()
        circle(rayon_tete)
        
    elif nb_erreurs == 2:
        up()
        goto(x_corps, y_corps)
        down()
        # COMPLETER ICI
        
    # COMPLETER ICI
      

def jouer():
    """
    Lance une partie de pendu.
    
    * Choisit un mot aléatoire
    * Crée le masque
    * Initialise l'affichage
    * tant que le nombre d'erreur < 7 et que le masque est différent du mot :
        * demander la lettre
        * si la lettre est dans le mot :
            * modifier le masque
            * afficher la lettre
        * sinon
            * ajouter un trait au pendu correspondant au nouveau nombre d'erreurs
    * attendre 3 secondes (sleep(3))
    * si le masque est égal au mot, afficher un message de félicitations
    * sinon, afficher un message de défaite précisant quel était le mot à trouver
    
    Exemple de message de fin :
    pencolor("green")
    write("GAGNÉ ! Bien joué, le mot était « " + mot + " ».", False, "left", ("Arial", 24 ,"normal"))
    """
    mot = mot_aleatoire_dictionnaire()
    masque = creer_masque(mot)
    initialise_affichage(mot)
    
    # COMPLETER ICI
        

# ------------------------------------------------------------------------

# LANCE LE JEU

jouer()
        