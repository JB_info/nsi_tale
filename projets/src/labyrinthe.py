# Par J. BENOUWT
# Licence CC-BY-NC-SA



import time
import pygame
from pygame.locals import *



class Labyrinthe:
    """
    Classe pour représenter un labyrinthe.
    """
    
    
    def __init__(self, labyrinthe):
        """
        Crée un objet labyrinthe à partir du tableau passé en paramètre.
        :param labyrinthe: (list) tableau représentant un labyrinthe
        
        >>> laby = Labyrinthe(laby_0)
        >>> laby.hauteur
        5
        >>> laby.largeur
        4
        >>> laby.position
        (0, 0)
        >>> laby.sortie
        (3, 4)
        >>> laby.liste_adjacence
        {(0, 0): [(1, 0)], (0, 2): [(1, 2)], (1, 0): [(0, 0), (1, 1)], (1, 1): [(1, 0), (1, 2)], (1, 2): [(0, 2), (2, 2), (1, 1), (1, 3)], (1, 3): [(1, 2), (1, 4)], (1, 4): [(1, 3)], (2, 2): [(1, 2), (3, 2)], (3, 1): [(3, 2)], (3, 2): [(2, 2), (3, 1), (3, 3)], (3, 3): [(3, 2), (3, 4)], (3, 4): [(3, 3)]}
        >>> laby.taille_case
        50
        """
        # attributs hauteur et largeur
        self.hauteur, self.largeur = len(labyrinthe), len(labyrinthe[0])
        
        # construit la liste d'adajacence du graphe
        self.liste_adjacence = {}
        
        for x in range(self.largeur):
            for y in range(self.hauteur):
                
                if labyrinthe[y][x] in [" ", "D", "S"]:
                    self.liste_adjacence[(x, y)] = []
                
                    if x-1 > -1 and labyrinthe[y][x-1] in [" ", "D", "S"]:
                            self.liste_adjacence[(x, y)].append((x-1, y))
                    if x+1 < self.largeur and labyrinthe[y][x+1] in [" ", "D", "S"]:
                            self.liste_adjacence[(x, y)].append((x+1, y))
                    if y-1 > -1 and labyrinthe[y-1][x] in [" ", "D", "S"]:
                            self.liste_adjacence[(x, y)].append((x, y-1))
                    if y+1 < self.hauteur and labyrinthe[y+1][x] in [" ", "D", "S"]:
                            self.liste_adjacence[(x, y)].append((x, y+1))
                           
                # attributs position du personnage et emplacement de la sortie
                if labyrinthe[y][x] == "D":
                    self.position = x, y
                elif labyrinthe[y][x] == "S":
                    self.sortie = x, y
        
        # taille des cases pour l'interface graphique
        self.taille_case = 50


    def dessine(self):
        """
        Méthode principale qui gère tous les dessins dans l'interface graphique.
        """
        pygame.init()
        pygame.display.set_caption("LABYRINTHE")
        
        # crée une fenêtre toute blanche à la taille du labyrinthe
        self.window = pygame.display.set_mode((self.taille_case*self.largeur, self.taille_case*self.hauteur))
        self.window.fill((255, 255, 255))
        
        # récupère l'image du mur et la dimensionne
        mur = pygame.transform.scale(pygame.image.load('mur.jpg').convert_alpha(), (self.taille_case, self.taille_case))
        
        # place tous les murs
        for x in range(self.largeur):
            for y in range(self.hauteur):
                if (x, y) not in self.liste_adjacence.keys():
                    self.window.blit(mur, (x*self.taille_case, y*self.taille_case))
                    
        # récupère les images du personnage et de la sortie et les place dans la fenêtre
        perso = pygame.transform.scale(pygame.image.load('personnage.jpg').convert_alpha(), (self.taille_case, self.taille_case))
        sortie = pygame.transform.scale(pygame.image.load('sortie.jpg').convert_alpha(), (self.taille_case, self.taille_case))
        self.window.blit(perso, (self.position[0]*self.taille_case, self.position[1]*self.taille_case))
        self.window.blit(sortie, (self.sortie[0]*self.taille_case, self.sortie[1]*self.taille_case))
        
        # met à jour l'affichage
        pygame.display.update()
        
        
    def dessine_chemin(self, chemin):
        """
        Méthode permettant de dessiner dans la fenêtre le chemin passé en paramètre.
        :param chemin: (list) tableau contenant les coordonnées successives du chemin
        """
        # pour chaque case du chemin
        for i in range(len(chemin)-1):
            # dessine une ligne rouge allant de cette case à la suivante
            pygame.draw.line(self.window, (255, 0, 0, 255),
                             (chemin[i][0]*self.taille_case+self.taille_case//2, chemin[i][1]*self.taille_case+self.taille_case//2),
                             (chemin[i+1][0]*self.taille_case+self.taille_case//2, chemin[i+1][1]*self.taille_case+self.taille_case//2),
                             self.taille_case//10)
            
        # met à jour l'affichage
        pygame.display.update()
        
        
    def deplace_personnage(self, position):
        """
        Déplace le personnage à la position donnée.
        :param position: (tuple) la nouvelle position
        """
        # déplace le personnage dans la nouvelle case
        perso = pygame.transform.scale(pygame.image.load('personnage.jpg').convert_alpha(), (self.taille_case, self.taille_case))
        self.window.blit(perso, (position[0]*self.taille_case, position[1]*self.taille_case))
        # remet l'ancienne case en blanc
        blanc = pygame.Surface((self.taille_case,self.taille_case), pygame.SRCALPHA)
        blanc.fill((255, 255, 255, 255))
        self.window.blit(blanc, (self.position[0]*self.taille_case, self.position[1]*self.taille_case))
        # met à jour la position
        self.position = position
        # met à jour l'affichage
        pygame.display.update()
        
        
    def jeu(self):
        """
        Lance un jeu de labyrinthe.
        """
        # dessine le labyrinthe
        self.dessine()
        continuer = True
        
        # tant qu'on ne ferme pas la fenêtre
        while continuer:
            for event in pygame.event.get():
                # si l'utilisateur clique sur la croix pour fermer la fenêtre
                if event.type == QUIT:
                    continuer = False
                    
                # si l'utilisateur appuie sur une touche
                elif event.type == pygame.KEYDOWN:
                    # affiche le code de la touche appuyée
                    print(event.key)
                    
                    ##                    
                    # IL FAUDRA AJOUTER DU CODE ICI
                    ##
                    
                    if self.position == self.sortie:
                        # on affiche un message de féliciations
                        font = pygame.font.SysFont('Comic Sans MS', 50)
                        self.window.blit(font.render('BRAVO !', False, (255, 0, 0)), (self.largeur*self.taille_case//2 - 2*self.taille_case, self.hauteur*self.taille_case//2 - self.taille_case))
                        pygame.display.update()
                        # on attend 5 secondes avant de fermer la fenêtre
                        time.sleep(5)
                        continuer = False
                        
        # quitte l'interface graphique                  
        pygame.quit()
        
        
        
# quelques labyrinthes
laby_0 = ["D ##",
         "# # ",
         "    ",
         "# # ",
         "# #S"]

laby_1 = ["D # ###   ",
         "# #   # # ",
         "    #   # ",
         " ##### ###",
         " #   #   #",
         "   # # ###",
         "#### #    ",
         "#    ## ##",
         "  ####   #",
         "#      # S"]

laby_2 = ["  # ###  D",
         "# #   # # ",
         "    #   # ",
         " ##### ###",
         " #   #   #",
         "   # # ###",
         "#### #    ",
         "#    ## ##",
         "  ####   #",
         "#     S#  "]

laby_3 = [" # D  #      #    #   #     # ",
         " # ## # #### # ## # # # ### # ",
         "    # #    #    #   # # # #   ",
         "### # ## # # ## #######   # ##",
         "    #    #      #    #  #     ",
         " ####### # ### ## ## # ###### ",
         "     #       #    #  #        ",
         "# ## # ##### ###### ## # # ###",
         "  #  # #     #      #  # #  # ",
         " ## ## # ##### ### ## ## ##   ",
         "  ###  #         # #  #   ## #",
         "# #   ## # #####     #### #   ",
         "#   ###  #   # # ###    # ### ",
         "# ###   #### # #   ## #   #   ",
         "  #   ####   # #        # # ##",
         " ## ###    # # ### # ## #    S",
         " #    # ## #       #  # # ## #",
         " # ## #  # #### # ###   #  #  ",
         "   #  ## # #    # # # #### ## ",
         "####     #   ####   #    # #  "]
    
    
# doctests
if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose = True)