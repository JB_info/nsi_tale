# Projet 3 : Répertoire



Nous allons écrire un programme Python qui manipule une base de données. L'objectif est de créer un répertoire contenant les noms, prénoms et numéros de téléphone de personnes.

Avant de se lancer dans le projet, il faut apprendre à manipuler une BDR depuis Python (et non plus depuis DB Browser).



## Manipulations avant le projet : SQL et Python

Vous allez découvrir comment interroger une base de données depuis un programme écrit en python. Dans ce cas, le SGBD est le serveur et le programme est le client. La plupart des langages (le cobol, le langage C, le langage java...) permettent d'utiliser des requêtes SQL pour importer des données enregistrées de façon persistante dans une base de données. On parle de *embedded SQL*. Dans notre cas, nous allons utiliser le langage **Python** et le SGBD **SQLite**.

(Tout ce dont vous aurez besoin se trouve ci-dessous, mais vous pourrez si nécessaire consulter la [documentation de SQLite](https://docs.python.org/3/library/sqlite3.html). )



### 1. Connexion à la base depuis un programme

Pour interagir avec notre base de données, nous utiliserons le module  `sqlite3`.

```python
>>> import sqlite3
```

Pour cet exemple, nous allons créer un petit carnet d'adresse, croisé avec des données sur les villes de France.

- La méthode `connect` prend en paramètre le chemin vers la base de données et retourne un objet de type `Connection`. Si la base n'existe pas, cette instruction crée une base vide.

```python
>>> conn = sqlite3.connect('carnet_adresse.db') 
```

- Il ne faudra pas oublier de fermer la connexion **à la fin du programme, pas maintenant**.

```python
>>> conn.close()
```

- Une fois la connexion établie, on peut créer un curseur (un objet de type `Cursor`) par la méthode `cursor()` de `Connection`.

```python
>>> cur = conn.cursor()
```



### 2. Instructions `create`, `update`, `delete`

- Un curseur dispose d'une méthode `execute()` qui prend en paramètre une chaîne de caractères représentant une requête SQL. Exemple : `CREATE`, `INSERT`, `UPDATE`.

```python
>>> cur.execute("CREATE TABLE Personne (nom text primary key, prenom text, age int, ville text);")
>>> nom = 'Jesapél'
>>> prenom = 'Groot'
>>> age = 118
>>> ville = 'planetX'
>>> cur.execute("INSERT INTO personne VALUES (?, ?, ?, ?);", (nom, prenom, age, ville))
```

- La méthode  `commit()` de la classe `Connection` met à jour la base de données pour les autres utilisateurs. Sans appel à cette méthode, une autre connexion à la base de donnée ne verra pas vos modifications. Par exemple, les modifications ne seront pas visibles dans DB Browser.

```python
>>> conn.commit()
```

- L'instruction suivante est possible, mais déconseillée ([risque d'injection SQL](https://fr.wikipedia.org/wiki/Injection_SQL)). Il vaut mieux utiliser la notation avec les `(?, ?, ?, ?)` du premier exemple.

```python
>>> cur.execute("INSERT INTO Personne VALUES ('Jesapél', 'Groot', 118, 'PlanetX');")    
```

- Modifier avec `UPDATE` .

```python
>>> nouvel_age = 119
>>> cur.execute("UPDATE Personne SET age = ? where prenom = 'Groot';", (nouvel_age,))
```

- Pour supprimer avec `DELETE` :

```python
>>> prenom = 'Groot'
>>> cur.execute("DELETE FROM Personne WHERE prenom = ?;", (prenom,))
```

**Attention** : Un tuple d'un unique élément doit être écrit avec une virgule après cet élément.

Insérez les personnes suivantes avant de continuer :

```python
>>> nom = 'Jesapél'
>>> prenom = 'Groot'
>>> age = 118
>>> ville = 'planetX'
>>> cur.execute("INSERT INTO personne VALUES (?, ?, ?, ?);", (nom, prenom, age, ville))
>>> nom = 'Star'
>>> prenom = 'Lord'
>>> age = 36
>>> ville = 'Earth'
>>> cur.execute("INSERT INTO personne VALUES (?, ?, ?, ?);", (nom, prenom, age, ville))
>>> nom = 'Thor'
>>> prenom = 'Odinson'
>>> age = 217
>>> ville = 'Asgard'
>>> cur.execute("INSERT INTO personne VALUES (?, ?, ?, ?);", (nom, prenom, age, ville))  
```

N'oubliez pas le commit :

```python
>>> conn.commit()
```

### 3. Consultation des données : le `SELECT`

La méthode `execute()` du curseur permet également de faire des sélections de données. Dans ce cas, il faut récupérer le résultat de la requête pour pouvoir le traiter dans le programme. Chaque ligne de la table ou de la combinaison de tables interrogée(s) est représentée sous la forme d'un tuple.

-  On peut récupérer la "première" ligne (un seul tuple) avec la méthode `fetchone`. Une fois qu'un premier tuple est extrait, cette méthode renvoie le tuple suivant. Si toutes les lignes ont été lues ou que la requête ne renvoie pas de ligne, `None` est renvoyé.

```python
>>> cur.execute("SELECT * FROM Personne;")
>>> first_personne = cur.fetchone() # récupère la première personne
```

* On peut récupérer tous les résultats avec la méthode `fetchall`. Le résultat est une liste de tuples, qui peut être vide si aucun résultat n'est retourné.

```python
>>> cur.execute("SELECT * FROM Personne;")
>>> personnes = cur.fetchall() # récupère toutes les personnes
```

* La commande `cur.execute("requete")` exécute la requête indiquée entre apostrophe, si cette requête est valide, bien entendu. A partir de là, vous pouvez, en Python, exécuter toutes les requêtes SQL vues et testée avec DB Browser en SQLite : `SELECT`, `ORDER BY`, `DISTINCT`, les agrégats (`SUM`, `MIN`, `MAX`, `COUNT`, `AVG` ...), les jointures (`JOIN` ... `ON`...), etc.

### 4. Résumé

```python
import sqlite3

# Connection à la base de données
conn = sqlite3.connect('base.db')

# Création du curseur qui permet d'interagir avec la base
cur = conn.cursor()

# Envoi d'une requête SQL
cur.execute("commande_sql")

# Pour récupérer une ligne de la requête (peut être utilisée plusieurs fois)
# La ligne est un tuple
ligne = cur.fetchone()

# Pour récupérer toutes les lignes de la requête
# C'est une liste de tuples
lignes = cur.fetchall()

# Pour appliquer les modifications (après un INSERT ou UPDATE)
conn.commit()

# Pour fermer la connection
conn.close()
```

Vous êtes maintenant prêts pour le projet !



## Présentation du projet

Ce projet se fera si possible en **binôme**.

L'objectif de ce projet est de créer un répertoire contenant les noms, prénoms et numéros de téléphone de personnes. Le stockage des contacts du répertoire se fera dans une base de données.

Pour ne pas complexifier, le programme fonctionnera sans interface graphique avec un menu permettant de choisir des actions :

```
Que voulez-vous faire ?
1 - Ajouter un contact
2 - Supprimer un contact
3 - Afficher tous les contacts
4 - Chercher un contact
5 - Quitter
Choix :
```

Voici les **fonctionnalités que votre projet doit implémenter** :

- un menu (identique à celui ci-dessus) pour choisir les différentes actions
- ajout d'un contact dans la base de données *qui ne déclenche pas d'erreur si une contrainte n'est pas respectée*
- suppression d'un contact avec *demande de confirmation* et *qui ne déclenche pas d'erreur si le contact n'existe pas*
- affichage de tous les contacts *par ordre alphabétique des noms ou des prénoms* (avec menu demandant à l'utilisateur l'ordre souhaité)
- recherche parmi les contacts par *nom **et** prénom*
- quitter l'application



Vous apporterez un soin particulier à votre code en respectant les **règles de bonne conduite habituelles**. Votre programme

1. doit être lisible, aéré et clair ;
2. doit être découpé en petits composants faisant peu de choses, mais les faisant bien ;
3. ne doit pas avoir du code répété ;
4. doit séparer calculs et interface homme/machine ;
5. doit être documenté ;
6. doit être testé ;
7. doit être commenté.



**Amélioration possibles** :

* ajout de critères de recherche (ex: numéros commençant par 06 / noms commençant par 'B' / prénoms contenant 'en'...)
* ajout d'une seconde relation associant à un numéro de téléphone, le pays et le département du contact
* modification d'un contact déjà existant
* interface graphique
* toute autre idée d'amélioration



---

Inspiré de Pixees et du DIU - université de Lille.

__________

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
