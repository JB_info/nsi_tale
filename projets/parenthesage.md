# Projet 2 : Parenthésage



## I. Seulement avec des parenthèses

On considère une chaîne de caractères comportant des parenthèses ouvrantes `(`et fermantes `)`. On se propose de concevoir une fonction qui décèle les anomalies d'écriture. 

Pour vérifier si une expression est correctement parenthésée, une méthode consistant à utiliser une pile est exposée ci-dessous:

- on commence par construire une pile de parenthèses ouvrantes vide.
- pour tout élément de la chaîne de caractère à tester :
  - si cet élément est une parenthèse ouvrante alors on l’empile dans notre pile.
  - si cet élément est une parenthèse fermante :
    - si la pile est vide, alors l’expression est incorrecte (trop de fermantes) et on renvoie faux.
    - sinon, on dépile notre pile de parenthèse(s) ouvrante(s).
- Quand la chaîne a été parcourue entièrement, :
  - la pile doit être vide. si c'est le cas, on renvoie vrai
  - si ce n'est pas le cas, l’expression est incorrecte (trop d'ouvrantes) et on renvoie faux.



​	**1)** Programmez une fonction `est_correcte_parentheses(expression)` qui renvoie une erreur `True` si le parenthésage est correct et `False` sinon. Documentez la fonction et proposez au moins 2 doctests.



## II. Accolades, crochets et parenthèses

On se propose cette fois de réaliser une fonction `est_correcte` qui vérifie les parenthèses, crochets et accolades d'une expression mathématique. Le principe algorithmique est le même que précédemment sauf qu'il s'agit en plus de vérifier que le caractère dépilé correspond en version ouverte au caractère rencontré :

* si on rencontre une accolade ou une parenthèse ou un crochet ouvrant alors on l'empile dans la pile des _ouvrantes_.

* si on rencontre une accolade ou une parenthèse ou un crochet fermant :

  * on dépile la pile des ouvrantes dans une variable.
  * si le caractère ouvrant que l'on vient de dépiler ne correspond pas au fermant que l'on vient de rencontrer, alors on renvoie faux.



​	**2)** Programmez une fonction `est_correcte(expression)` qui renvoie une erreur `True` si le parenthésage complet est correct et `False` sinon. Documentez la fonction et proposez au moins 2 doctests.



## III. En HTML

En HTML ,en se propose cette fois de vérifier le bon balisage du code.

Pour simplifier le problème, on ignorera les balises orphelines et on n'utilisera que quelques balises usuelles :

```txt
<html> </html>
<body> </body>
<p> </p>
<table> </table>
<tr> </tr>
<td> </td>
<ul> </ul>
<li> </li>
```



​	**3)** Proposez un algorithme en pseudo code qui permet de vérifier le bon empilement de ces balises en vous inspirant des questions précédentes.

​	**4)** Programmez une fonction `est_correcte_html(code_html)` dont le paramètre est une chaîne de caractères contenant le code HTML à vérifier, qui renvoie `True` si le balisage est correcte et `False` sinon. Documentez la fonction et proposez au moins 2 doctests.

​	**5)** Programmez une fonction `est_correct_fichier(nom_fichier_html)` dont le paramètre est le nom d'un fichier HTML, qui ouvre ce fichier, lit son contenu et vérifie le parenthésage. Documentez la fonction.



## À rendre

Une archive (`prenom1_prenom2.zip`) contenant :

* votre fichier python `module_lineaire.py` avec les classes Maillon, Liste_chainee, Pile et File
* votre fichier `parenthesage.py` avec les quatre fonctions du projet
* un fichier texte contenant votre réponse à la question 3)
* un fichier html permettant de tester votre fonction de la question 5)

N'oubliez pas que la qualité des commentaires, la propreté du code, et l'optimisation des fonctions sont pris en compte.

**Précisez les noms des deux membres du binôme** en haut de chacun des quatre fichiers *et* dans le nom de l'archive.

Comme d'habitude, l'archive est à déposer via l'application Casier sur l'ENT.



## Pour aller plus loin :

Si vous avez terminé, vous pouvez travailler sur l'une des améliorations suivantes :

* prendre en compte l'existence de balises auto-fermantes comme `<img/>` ou `<br/>`
* proposer un menu qui demande le nom du fichier à l'utilisateur
* vérifier le parenthésage d'un fichier CSS (feuilles de style)
* afficher le nom de toutes les balises non correctement parenthésées
* toute autre idée d'amélioration



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
