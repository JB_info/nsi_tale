# Projet 4 : Tours de Hanoï



## Règle du jeu

Les tours de Hanoï est un casse_tête qui consiste à déplacer tous les disques empilés sur une tour de départ vers une tour de destination, en se servant d'une tour intermédiaire avec pour seule règle le fait de ne pouvoir poser un disque que sur une tour vide ou un disque plus grand.

Une petite photo d'un jeu en bois avec 8 disques sur la tour de départ :

![8 disques](https://upload.wikimedia.org/wikipedia/commons/0/07/Tower_of_Hanoi.jpeg)



Voici, pour 4 disques, les étapes de la résolution :



![4 disques](https://upload.wikimedia.org/wikipedia/commons/6/60/Tower_of_Hanoi_4.gif)



>
>
> Résolvez _à la main_ les tours de Hanoï lorsque la première tour ne comporte que trois disques.
>
>



## Définition récursive

Il existe différentes méthodes pour résoudre ce problème. Nous allons utiliser une définition récursive.

Considérons une situation où la tour de départ compte _n > 1_ disques_ à déplacer vers la tour de destination :![situation de départ](img/etape1.png)

Pour déplacer les _n_ disques d'une tour de départ vers une tour de destination, via une tour intermédiaire, il y a deux cas de figure.

**Soit n = 1** : Dans ce cas, la tour de départ ne comporte qu'un seul disque qu'il suffit de déplacer directement vers la tour de destination.

**Soit n > 1**:  Dans ce cas, il _suffit_, de réaliser les mouvements suivants :

* Déplacer les _n - 1_ premier(s) disque(s) de la tour de départ vers la tour intermédiaire

    ![déplacer n - 1 disques](img/etape2.png)

* Déplacer le dernier disque de la tour de départ vers la tour de destination :

    ![déplacer dernier disques](img/etape3.png)

    

* Déplacer les _n - 1_ disques de la tour intermédiaire vers la tour de destination.![déplacer n - 1 disques vers la destination](img/etape4.png)



A chaque étape, nous avons une tour de `départ`, une tour de `destination` et une tour `intermédiaire`. Il ne s'agit pas des mêmes tours à chaque fois. 

>
>
>Donner en pseudo-code une définition récursive pour déplacer _n_  disques de la tour de _départ_ vers la tour de _destination_, via la tour _intermédiaire_ si nécessaire :
>
>$`deplacer( n, depart, destination, intermediaire) = \left\lbrace \begin{array}{}  cas~d'arrêt~si~ n = 1 \\ \left\lbrace \begin{array}{}  Etape~1  \\ Etape~2  \\ Etape~3 \end{array} \right. \end{array} \right. sinon`$
>
>



## Implémentation

### Structure de données



* Chaque tour est un empilement de disque(s) : la structure de pile est donc incontournable ! Nous aurons besoin de notre `module_lineaire`.

* Le jeu comporte trois tours : nous allons utiliser un tableau de 3 piles.

* Pour chaque pile, un disque sera représenté par un entier correspondant à la taille du disque. Ainsi ,pour une pile de 3 disques cela donnera :

    ```python
    1 
    2
    3
    ```


### La classe `Hanoi`

Nous coderons la _classe_ `Hanoi` dans un module que l'on appellera `module_hanoi.py`.



> Définissez et construisez la classe `Hanoi` qui possède deux attributs :
>
> * la _hauteur_, passée en paramètre dans le constructeur, correspond au nombre total de disques sur la tour de départ, numérotés de 1 (pour le plus petit disque) à _hauteur_ (pour le plus grand).
> * la liste `tour` est une liste de 3 piles, une par tour. Sur la première tour sont empilés autant de disques que le stipule la _hauteur_. Les deux autres piles sont vides.



Les méthodes de cette classe sont :

* `depile(self, depart, destination)` qui dépile la tour de départ dans la tour de destination.

* `affiche(self)` qui affiche les 3 tours, par exemple, sous la forme suivante où chaque entier représente la taille du disque :

    ```txt
     Tour no 0 : 
    1 
    4 
    
     Tour no 1 : 
    2 
    3 
    
     Tour no 2 : 
    5 
    6 
    7 
    8 
    ```

    

### Résolution du problème

Créez un programme `hanoi.py` et importez-y le module `module_hanoi`.

Nous avons besoin de trois fonctions :

* `afficher_coup(tours, depart, destination)` qui affiche la situation après un déplacement de départ vers destination en précisant le coup qui vient d'être joué.
* `deplacer(tours, nbre_disques, depart, destination, intermediaire)` qui déplace _nbres_disques_ de la situation de départ jusqu'à la situation finale, via l'_intermédiaire_ si nécessaire,  en utilisant la définition récursive.
* `resoudre(hauteur)` qui initialise le départ et lance la résolution pour une tour de départ de hauteur précisée.

Le paramètre `tours` est un objet de la classe Hanoï.


Voici, par exemple, le déroulé de la résolution du jeu avec 3 disques au départ :

```python
>>> resoudre(3)
____________________départ________________________
 
 Tour no 0 : 
1 
2 
3 

 Tour no 1 : 

 Tour no 2 : 

________de 0 vers 2 _________
 
 Tour no 0 : 
2 
3 

 Tour no 1 : 

 Tour no 2 : 
1 

_________de 0 vers 1 _________
 
 Tour no 0 : 
3 

 Tour no 1 : 
2 

 Tour no 2 : 
1 

________de 2 vers 1 _________
 
 Tour no 0 : 
3 

 Tour no 1 : 
1 
2 

 Tour no 2 : 

________de 0 vers 2 _________
 
 Tour no 0 : 

 Tour no 1 : 
1 
2 

 Tour no 2 : 
3 

________de 1 vers 0 _________
 
 Tour no 0 : 
1 

 Tour no 1 : 
2 

 Tour no 2 : 
3 

________de 1 vers 2 _________
 
 Tour no 0 : 
1 

 Tour no 1 : 

 Tour no 2 : 
2 
3 

________de 0 vers 2 _________
 
 Tour no 0 : 

 Tour no 1 : 

 Tour no 2 : 
1 
2 
3 


```

### Améliorations (bonus)

Si vous avez terminé, vous pouvez essayer d'améliorer l'affichage des tours. Par exemple, vous pouvez :

* essayer d'afficher les trois tours côte à côte, encadré par des '|' et des '-'
* utiliser la bibliothèque [Turtle](https://docs.python.org/fr/3/library/turtle.html)
* ... toute autre idée d'affichage !

Vous pouvez également créer une interface permettant à l'utilisateur de jouer aux tours de Hanoï.



_________________

Projet de Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
