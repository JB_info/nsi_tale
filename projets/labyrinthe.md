# Projet 5 : Labyrinthes

Nous allons créer des labyrinthes comme ceci :

![labyrinthe](img/labyrinthe.png)

## 1. Jouer au labyrinthe

1) Pour commencer, créez un dossier pour votre projet, il y aura plusieurs fichiers à mettre dedans.

2) Ensuite, ajoutez le module_lineaire dans votre dossier. Vous en aurez besoin plus tard.

Pour pouvoir créer l'interface graphique comme dans l'image ci-dessus, il va nous falloir 3 images :
* une pour représenter un mur
* une pour représenter la sortie
* une pour représenter le personnage

3) Téléchargez trois images de votre choix et mettez les dans votre dossier. Elles doivent impérativement s'appeler `mur.jpg`, `sortie.jpg` et `personnage.jpg`.

Nous allons créer l'interface graphique en utilisant la bibliothèque *pygame*, qui est spécialement conçue pour... les jeux.

4) Téléchargez le [fichier_suivant](src/labyrinthe.py) dans votre dossier.

5) Lisez attentivement la documentation et les commentaires pour comprendre comment le code fonctionne.

6) Pour lancer un labyrinthe, entrez cette instruction dans la console : `Labyrinthe(laby_0).jeu()`. Vous ne pouvez pas manipuler le personnage mais tout doit s'afficher correctement.

7) Essayez de lancer les 3 autres labyrinthes. En comparant les tableaux en bas du fichier et les interfaces, déduisez ce que représentent :
* un "#"
* un " "
* un "D"
* un "S"

8) Créez votre propre labyrinthe `mon_laby` et ajoutez le en dessous des autres.

Pour pouvoir déplacer le personnage, il faut trouver à quel *code* correspondent chaque touche utile.

Nous allons utilisez les flèches du clavier pour déplacer le personnage.

9) Relancez l'instruction `Labyrinthe(laby_0).jeu()` puis appuyez sur la flèche du haut sur votre clavier. Un code apparaît dans la console : notez le quelque part.

10) De la même manière, récupérez les codes pour les 3 autres directions. Vous pouvez ensuite mettre le print en commentaire pour éviter que les codes s'affichent tout le temps.

11) Dans la méthode `jeu`, à l'endroit où il écrit "# IL FAUDRA AJOUTER DU CODE ICI", ajoutez le code suivant en remplaçant les ??? par le code de la touche correspondante :
```python
# si on appuie sur la flèche en haut
if event.key == ??? and self.position[1] > 0 and (self.position[0], self.position[1] - 1) in self.liste_adjacence:
    self.deplace_personnage((self.position[0], self.position[1] - 1))
# si on appuie sur la flèche en bas
elif event.key == ??? and self.position[1] < self.hauteur -1 and (self.position[0], self.position[1] + 1) in self.liste_adjacence:
    self.deplace_personnage((self.position[0], self.position[1] + 1))
# si on appuie sur la flèche gauche
elif event.key == ??? and self.position[0] > 0 and (self.position[0] - 1, self.position[1]) in self.liste_adjacence:
    self.deplace_personnage((self.position[0] - 1, self.position[1]))
# si on appuie sur la flèche droite
elif event.key == ??? and self.position[0] < self.largeur -1 and (self.position[0] + 1, self.position[1]) in self.liste_adjacence:
    self.deplace_personnage((self.position[0] + 1, self.position[1]))
```

12) Relancez un labyrinthe et essayez de déplacer le personnage !

13) Réalisez un menu qui demande à l'utilisateur de choisir parmi les labyrinthes existants puis le lance.


## 2. Mode triche

Nous allons donner à l'utilisateur la possibilité de tricher et d'afficher le chemin allant du personnage à la sortie.

Pour pouvoir faire cela, il faut s'imaginer que le labyrinthe est un *graphe*.

Les cases vides du labyrinthe (y compris la sortie et celle où se trouve le personnage) sont les sommets du graphe.
On trace une arête entre deux sommets si les deux cases sont voisines.

Par exemple pour le labyrinthe `laby_0` il y a un sommet (0,0) qui est relié à un sommet (1,0) car la case du coin en haut à gauche (0,0) est vide et est voisine à droite de la case vide (1,0).

14) Regardez les doctests de la méthode `__init__`. Quel attribut vous permet de récupérer le graphe associé au labyrinthe ?

15) Ajoutez à la classe une méthode `trouve_chemin(self, depart, arrivee)` qui renvoie un chemin allant d'un sommet *depart* à un sommet *arrivee* en utilisant un algorithme sur les graphes.  
*(les sommets sont donc tous des coordonnées et non plus des lettres comme dans le chapitre sur les graphes)*.

On va utiliser la touche espace pour activer le mode triche.

16) Trouvez le code de la touche espace et ajoutez le mode triche au même endroit que la gestion des flèches. Il faudra donc :
* trouver le chemin du personnage à la sortie avec votre méthode précédente
* dessinez le chemin dans l'interface graphique avec la méthode `dessine_chemin`

17) Testez !

## 3. Triche plus efficace

18) Essayez le mode triche sur le `laby_2` sans bouger le personnage au départ. Que remarquez-vous ?

19) Ajoutez à la classe une méthode `trouve_chemin_plus_court(self, depart, arrivee)`.

Vous avez dû remarquer qu'il y avait plusieurs embranchements dans le labyrinthe qui se recoupent. Pour éviter de refaire les calculs quand on recroise plusieurs fois le même sommet, on peut aussi utiliser la programmation dynamique.

Voici l'algorithme récursif sur lequel on va se baser pour écrire une version dynamique de la recherche de chemin :

```
trouve_chemin_recursif(self, depart, arrivee, chemin) :
    si le départ est égal à l'arrivée
        renvoyer le chemin + [arrivee]

    pour chaque voisin du sommet de départ:
        si le voisin n'est pas déjà dans le chemin:
            calculer recursivement le reste_du_chemin allant du voisin à l'arrivée et en ajoutant [depart] au chemin
            si le reste_du_chemin n'est pas None:
                renvoyer le reste_du_chemin

    renvoyer None
```


20) Ajoutez à la classe une méthode `trouve_chemin_recursif(self, depart, arrivee, chemin)`.

Si vous souhaitez tester votre code, il faut lancer la méthode en prenant un chemin initial vide (c'est-à-dire [ ]).

Nous pouvons maintenant utiliser la méthode Top-down de la programmation dynamique pour améliorer le coût en temps de cet algorithme.

21) Ajoutez à la classe une méthode `trouve_chemin_dynamique(self, depart, arrivee)` qui initialise la mémoire nécessaire aux calculs et fait appel à la méthode de la question suivante.

22) *Attention cette méthode est plus difficile. Essayez peut-être de faire les suivantes avant ?* Ajoutez à la classe une méthode `trouve_chemin_memoire(self, depart, arrivee, chemin, memoire)` qui remplit la mémoire.


## 4. Générateur de labyrinthe

23) Écrivez une *fonction* `generer_labyrinthe(hauteur, largeur)` qui construit un labyrinthe aléatoire. Pour vous aider, il y aura donc 3 grandes étapes :
* créer un tableau de tableaux composé de " " et "#"
* ajouter un "D" et d'un "S" sur deux cases aléatoires
* vérifier qu'il existe bien un chemin du départ à l'arrivée

24) Modifiez votre menu pour intégrer l'option de génération d'un labyrinthe aléatoire (en demandant à l'utilisateur la hauteur et la largeur souhaitée).


## Ce que vous devez rendre

#### Pour finir, votre projet doit donc :

* afficher un menu pour choisir un labyrinthe existant (parmi les 4 fournis ou le votre) ou en générer un aléatoirement
* afficher le labyrinthe choisi
* permettre de déplacer un personnage
* disposer de quatre algorithmes de recherche de chemin (classique, plus court, récursif et dynamique)
* permettre d'activer un mode triche affichant un chemin jusqu'à la sortie (le mode triche peut faire appel à l'algorithme que vous préférez)
* être commenté
* être documenté
* avoir des doctests pour chacune des 4 fonctions de recherche de chemin
* respecter les bonnes pratiques (lisible, aéré, clair, noms de variables appropriés...)
* être optimisé (pas de répétitions, pas d'instructions inutiles, découpage en petits morceaux de code, séparer calculs et ihm...)

Vous rendez une **archive (.zip)** de votre dossier (contenant les trois images, le programme Python et module_lineaire).


#### AMELIORATIONS POSSIBLES :

* *améliorer le mode triche* pour que si l'utilisateur appuie à nouveau sur la touche le chemin s'efface
* améliorer le mode triche pour afficher un menu qui *demande à l'utilisateur quel algorithme* de recherche de chemin il souhaite utiliser
* effectuer une *version Bottom-up* (arrivée vers le départ) de la recherche de chemin dynamique
* trouver un chemin encore plus rapidement en explorant en *priorité le sommet voisin le plus proche de la sortie*
* ...toute autre idée amélioration !


__________

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
