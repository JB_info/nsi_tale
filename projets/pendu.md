# Projet : Pendu

![pendu](img/pendu.jpg)

## Règles du jeu

#### Principe

Le jeu du pendu se joue à 2.

Les deux joueurs sont par exemple Dean et Eve.

Eve pense à un mot. Elle dessine la potence et une rangée de tirets, autant que de lettres dans le mot.

Dean annonce une lettre.

La lettre fait-elle partie du mot ?

* Si oui Eve l'inscrit à sa place, sur un tiret, autant de fois que la lettre se trouve dans le mot.
* Sinon Eve dessine le premier trait du pendu.

On continue le jeu jusqu'au moment où :

* Dean gagne la partie en trouvant toutes les lettres du mot ou en le devinant correctement.
* Eve gagne la partie en complétant le dessin du pendu.

#### Comment dessiner les traits du pendu

Au début de la partie on dessine la potence :

![pendu dessins](img/pendus/0.png)

Dean s'est trompé de lettre.
Eve dessine la tête du pendu :

![pendu dessins](img/pendus/1.png)

2ème erreur de Dean :
![pendu dessins](img/pendus/2.png)


3ème erreur de Dean :
![pendu dessins](img/pendus/3.png)


4ème erreur de Dean :
![pendu dessins](img/pendus/4.png)


5ème erreur de Dean :
![pendu dessins](img/pendus/5.png)


6ème erreur de Dean :
![pendu dessins](img/pendus/6.png)


Une erreur de trop ! Dean a perdu et Eve termine le dessin du pendu (la tête) :
![pendu dessins](img/pendus/7.png)


## Travail à réaliser

Dans notre version, c'est l'ordinateur qui va tirer au hasard un mot du dictionnaire et afficher avec [Turtle](https://docs.python.org/fr/3/library/turtle.html) des tirets (chacun représentant une lettre).  
L'utilisateur devra saisir une lettre, et 
* si elle est correcte, elle s'affichera au dessus du ou des tiret(s) correspondant(s)
* sinon, un trait sera dessiné en plus sur le pendu

Si le pendu est entièrement dessiné, l'utilisateur a perdu. Si l'utilisateur parvient à trouver toutes les lettres du mot, il a gagné.

Téléchargez d'abord le fichier contenant le [dictionnaire](src/dictionnaire.txt).

Pour vous aider, un fichier contenant la base du code à réaliser vous est fourni :  téléchargez le [fichier suivant](src/pendu.py).


## Améliorations possibles

Si vous avez terminé, vous pouvez :
* réaliser un "mode facile" où l'utilisateur a le droit à plus d'erreurs (5 en plus en comptant le dessin de la potence)
* réaliser un menu demandant de choisir entre le mode facile et le classique
* réaliser une version du jeu où l'utilisateur choisi un mot et l'ordinateur essaie de le deviner en proposant des lettres
* réaliser une version du jeu sans Turtle, avec des affichages comme ceci par exemple :
  ```python
  """
  |/    |
  |     O
  |   /-+-\
  |     +
  |    / \
  |   R I P
  |"""
  ```
* réaliser un "mode triche" qui propose à l'utilisateur des mots du dictionnaire pouvant correspondre au motif
* réaliser un mode à deux joueurs
* ... toute autre idée d'amélioration !

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
