# COURS DE TERMINALE NSI

![logo NSI](nsi_logo.png)

Le programme : [BO](https://cache.media.eduscol.education.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf)

Le bac : [épreuve écrite](bac/epreuve_ecrite/presentation.md) - [épreuve pratique](bac/epreuve_pratique/presentation.md) - [grand oral](bac/grand_oral/presentation.md)

Bases de Python : [Rappels de Première](programmation/memento.md)

Progression de l'année :
1. [Histoire](histoire/histoire.md)
2. [Programmation objet](programmation/poo.md)
3. Projet 1 : [jeu de la vie](projets/jeu_de_la_vie.md)
4. [Graphes](structures_de_donnees/graphes.md)
5. [Arbres](structures_de_donnees/arbres.md)
6. [Récursivité](programmation/recursivite.md)
7. [Listes](structures_de_donnees/structures_lineaires/listes.md)
8. [Piles](structures_de_donnees/structures_lineaires/piles.md)
9. [Files](structures_de_donnees/structures_lineaires/files.md)
10. Structures linéaires : [exercices](structures_de_donnees/structures_lineaires/exercices.md) & [bilan](structures_de_donnees/structures_lineaires/bilan.md) 
11. Projet 2 : [parenthésage HTML](projets/parenthesage.md)
12. [Systèmes sur puce](architectures_materielles/soc.md)
13. [Dictionnaires](structures_de_donnees/dictionnaires.md)
14. [Arbres - algorithmes](algorithmique/arbres/parcours.md)
15. [Arbres - implémentation](algorithmique/arbres/implementation.md)
16. [Arbres binaires de recherche](algorithmique/arbres/abr.md)
17. [SGBD](bases_de_donnees/sgbd.md)
18. [Modèle relationnel](bases_de_donnees/modele_relationnel.md)
19. [SQL](bases_de_donnees/sql.md)
20. [Exercices modèle relationel + SQL](bases_de_donnees/exercices.md)
21. Projet 3 : [répertoire téléphonique](projets/repertoire.md)
22. [Processus](architectures_materielles/processus.md)
23. Algorithmes vus en Première (rappels / DM) : [Tri par sélection](algorithmique/rappels_premiere/tri_selection.md) - [Tri par insertion](algorithmique/rappels_premiere/tri_insertion.md) - [Gloutons](algorithmique/rappels_premiere/algo_gloutons.md)
24. Réseau (routage) : [cours](architectures_materielles/routage.md) puis [exercices](architectures_materielles/routage_exercices.md)
25. Rappels [kNN](algorithmique/rappels_premiere/k_plus_proches_voisins.md) de Première
26. [Diviser pour régner](algorithmique/diviser_pour_regner.md)
27. Projet 4 : [tours de Hanoï](projets/tours_de_hanoi.md)
28. Entraînement à l'épreuve pratique : [Méthodologie](bac/epreuve_pratique/entrainement/methodologie.pdf) puis [Dérouler un algorithme pour repérer les erreurs](bac/epreuve_pratique/entrainement/derouler_algo.pdf).
29. [Sécurisation des communications](architectures_materielles/securisation.md)
30. [Programmation dynamique](algorithmique/programmation_dynamique.md)
31. [Algorithmes sur les graphes](algorithmique/parcours_graphes.md)
32. Projet 5 : [labyrinthes](projets/labyrinthe.md)
33. [Recherche textuelle](algorithmique/recherche_textuelle.md)
34. [Calculabilité, décidabilité](programmation/calculabilite_decidabilite.md)
35. [Paradigmes de programmation](programmation/paradigmes.md)
37. Projet 6 : [space invaders ?](https://www.youtube.com/watch?v=MU4psw3ccUI)

Projets supplémentaires :
* [Pendu (portes ouvertes)](projets/pendu.md)
* [Jeu de dé](https://framagit.org/JB_info/nsi_1ere/-/blob/main/projets/jeu_de_des.md)
* [Puissance 4](https://framagit.org/JB_info/nsi_1ere/-/blob/main/projets/puissance4.md)
* [Filtres images](https://framagit.org/JB_info/nsi_1ere/-/blob/main/projets/filtres_images.md)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
