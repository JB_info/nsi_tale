# Épreuve pratique

L'épreuve pratique compte pour **8 points** sur 20 de la note de NSI.

Elle dure **1 heure** et comporte **2 exercices** (chacun sur 4 points).

Elle se déroule au cours du deuxième trimestre, à la même période que l'épreuve écrite.

## 1. Premier exercice

Le premier exercice consiste à programmer un **algorithme figurant explicitement au programme**, ne présentant pas de difficulté particulière, dont on fournit une spécification. Il s'agit donc de restituer un algorithme rencontré et travaillé **en Première ou en Terminale**.

Voici quelques exemples de tels algorithmes :

* recherche d'une valeur dans un tableau
* recherche du maximum/minimum d'un tableau
* calcul de la moyenne d'un tableau
* recherche du nombre d'occurrences dans un tableau / dans une chaîne
* recherche dichotomique dans un tableau trié
* conversions (binaire vers décimal ou inversement)
* tri par insertion
* tri par sélection
* algorithme glouton de rendu de monnaie

* taille / hauteur d'un arbre binaire
* ...



## 2. Deuxième exercice

Pour le second exercice, un programme est fourni à l'élève. Cet exercice ne demande pas l'écriture complète d'un programme, mais permet de valider des compétences de programmation suivant des modalités variées : l'élève doit, par exemple, **compléter un programme « à trous »** afin de répondre à une spécification donnée, ou encore compléter un programme pour le **documenter**, ou encore compléter un programme en ajoutant des **assertions**, etc.

L'algorithme n'a pas forcément été vu en cours, mais fait référence à des notions abordées. Voici quelques exemples de telles notions :

* programmation objet
* tableaux
* piles, files
* dictionnaires
* méthode diviser pour régner
* arbres binaires de recherche
* binaire (conversions / image binaire)
* algorithmes gloutons
* ...



## 3. Sujets

Il y a une liste officielle de 40 sujets d'épreuve pratique. Vous ne pouvez tomber que sur l'un de ces sujets.

Voici les **sujets pour le bac 2022** :

| Sujet | Exercice 1 | Exercice 2 |
| -- | -- | -- |
| [sujet 1](2022/22_NSI_01) | nombre d'occurrences | rendu monnaie, algo glouton récursif |
| [sujet 2](2022/22_NSI_02) | moyenne pondérée | triangle de Pascal |
| [sujet 3](2022/22_NSI_03) | codage par différence | arithmétique grâce à un arbre binaire |
| [sujet 4](2022/22_NSI_04) | recherche d'entiers consécutifs | composante d'image binaire |
| [sujet 5](2022/22_NSI_05) | minimum et maximum d'un tableau | paquet de carte, POO |
| [sujet 6](2022/22_NSI_06) | indice du maximum et maximum d'un tableau | recherche sous-chaine, ADN |
| [sujet 7](2022/22_NSI_07) | conversion décimal vers binaire | tri bulle |
| [sujet 8](2022/22_NSI_08) | recherche élément dans un tableau | insertion d'un élément dans un tableau trié |
| [sujet 9](2022/22_NSI_09) | problème 3n+1, suite | mots parfait |
| [sujet 10](2022/22_NSI_10) | occurrence de caractères dans une phrase | fusion de liste triées |
| [sujet 11](2022/22_NSI_11) | recherche dichotomique | codage de César |
| [sujet 12](2022/22_NSI_12) | moyenne d'un tableau | tri tableau de 0 et 1 |
| [sujet 13](2022/22_NSI_13) | rendu monnaie, algorithme glouton | file avec liste chainée |
| [sujet 14](2022/22_NSI_14) | mots à trou | envois de messages, cycles |
| [sujet 15](2022/22_NSI_15) | nombre de répétitions (occurrences) dans un tableau | conversion décimale vers binaire |
| [sujet 16](2022/22_NSI_16) | indice du maximum et maximum d'un tableau | extraction des entiers positifs d'une pile |
| [sujet 17](2022/22_NSI_17) | nombre de mots dans une phrase | parcours et recherche dans un ABR |
| [sujet 18](2022/22_NSI_18) | minimum de deux tableaux liés | nombre palindrome |
| [sujet 19](2022/22_NSI_19) | multiplication avec addition et soustraction | recherche dans un sous-tableau trié |
| [sujet 20](2022/22_NSI_20) | ou exclusif de deux tableaux | carré magique |
| [sujet 21](2022/22_NSI_21) | multiplication avec addition et soustraction | recherche par dichotomie |
| [sujet 22](2022/22_NSI_22) | renverser une chaîne de caractères | nombres premiers, crible d'Ératosthène |
| [sujet 23](2022/22_NSI_23) | likes dans un réseau social, dictionnaires | notation postfixe des expression arithmétiques |
| [sujet 24](2022/22_NSI_24) | maximum d'un tableau | parenthésage, piles |
| [sujet 25](2022/22_NSI_25) | animaux dans un refuge, dictionnaires | trouver l'intrus dans un tableau |
| [sujet 26](2022/22_NSI_26) | indice du minimum d'un tableau | tri d'un tableau de 0 et 1 |
| [sujet 27](2022/22_NSI_27) | taille d'un arbre binaire, dictionnaires | tri par sélection (itératif) |
| [sujet 28](2022/22_NSI_28) | moyenne d'un tableau | conversion décimal vers binaire |
| [sujet 29](2022/22_NSI_29) | suite de Fibonacci sans récursivité | listes liées de notes et d'élèves, maximum |
| [sujet 30](2022/22_NSI_30) | fusion de deux tableaux triés | conversion chiffres romains vers décimal |
| [sujet 31](2022/22_NSI_31) | nombre d'occurrence dans un tableau (float ou int) | rendu monnaie, algorithme glouton |
| [sujet 32](2022/22_NSI_32) | indice d'un élément dans un tableau | adresses IP, POO |
| [sujet 33](2022/22_NSI_33) | conversion binaire vers décimal, tableaux | tri par insertion |
| [sujet 34](2022/22_NSI_34) | maximum d'occurrences dans une chaine de caractères | image, pixels, négatif |
| [sujet 35](2022/22_NSI_35) | moyenne d'un tableau avec assertions | recherche par dichotomie dans un tableau trié |
| [sujet 36](2022/22_NSI_36) | indice de dernière occurrence d'un entier dans un tableau | distance entre points |
| [sujet 37](2022/22_NSI_37) | vérification du tri d'un tableau | comptage des votes dans un tableau |
| [sujet 38](2022/22_NSI_38) | tri par sélection | jeu du plus ou moins |
| [sujet 39](2022/22_NSI_39) | moyenne d'un tableau | zoom sur dessin |
| [sujet 40](2022/22_NSI_40) | recherche dans un tableau, tableau d'indices | moyenne pondérée, dictionnaires |


## 4. Annales

Les sujets des années précédentes sont disponibles [ici](annales.md).
