# Annales des épreuves pratiques

Vous trouverez ici les 30 sujets des épreuves pratiques des années précédentes, avec à chaque fois le thème des deux exercices.

## 2022

Les sujets sont [ici](2022/)

Certains sujets n'ont pas changé.
Correspondances avec les sujets de 2021 :

| n° sujet 2022 | n° sujet 2021 |
| ---------- | ---------- |
| 1 | 8 |
| 2 | 9 |
| 3 | nouveau |
| 4 | 25 |
| 5 | 15 |
| 6 | 12 |
| 7 | 11 |
| 8 | 18 |
| 9 | 29 |
| 10 | 23 |
| 11 | 9 |
| 12 | 2 |
| 13 | 6 |
| 14 | nouveau |
| 15 | 21 |
| 16 | 10 |
| 17 | nouveau |
| 18 | 20 |
| 19 | 30 |
| 20 | nouveau |
| 21 | 3 |
| 22 | nouveau |
| 23 | nouveau |
| 24 | nouveau |
| 25 | nouveau |
| 26 | 17 |
| 27 | 28 |
| 28 | 16 |
| 29 | 7 |
| 30 | nouveau |
| 31 | 22 |
| 32 | 24 |
| 33 | 5 |
| 34 | 26 |
| 35 | 4 |
| 36 | 1 |
| 37 | nouveau |
| 38 | 13 |
| 39 | 27 |
| 40 | 14 |




## 2021

| Sujet                              | **Exercice 1**                                               | **Exercice 2**                                               |
| ---------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [sujet 1](annales/2021/21_NSI_01)  | indice de la dernière occurrence d'une valeur dans un tableau non vide | plus courte distance d'un point à un autre point d'une liste de points du plan |
| [sujet 2](annales/2021/21_NSI_02)  | moyenne des valeurs d'un tableau d'entiers                   | tri d'un tableau de zéros et de un, par échanges             |
| [sujet 3](annales/2021/21_NSI_03)  | multiplication de deux entiers relatifs par additions ou soustractions | recherche dichotomique d'une valeur dans un tableau trié (itératif) |
| [sujet 4](annales/2021/21_NSI_04)  | calcul de la moyenne d'un tableau non vide d'entiers         | recherche dichotomique d'une valeur dans un tableau trié     |
| [sujet 5](annales/2021/21_NSI_05)  | calcul d'un entier à partir de son écriture binaire sous forme de liste de 0 et de 1 | tri par insertion itératif                                   |
| [sujet 6](annales/2021/21_NSI_06)  | rendu de monnaie glouton avec trois pièces                   | implémentation d'une classe File à partir d'une classe Maillon |
| [sujet 7](annales/2021/21_NSI_07)  | calcul du terme de rang n de la suite de Fibonacci (programmation dynamique) | recherche du maximum d'une liste de notes et de la liste des élèves l'ayant obtenue |
| [sujet 8](annales/2021/21_NSI_08)  | nombre d'occurrences d'un caractère dans un mot              | rendu de monnaie glouton récursif                            |
| [sujet 9](annales/2021/21_NSI_09)  | calcul d'une moyenne pondérée à partir d'une liste de couples (note, coefficient) | construction du triangle de Pascal d'ordre n sous la forme d'un tableau triangulaire |
| [sujet 10](annales/2021/21_NSI_10) | détermination du maximum d'une liste d'entiers et de la position de sa première occurrence | extraction des éléments positifs d'une pile sous forme d'une pile sans changement de l'ordre ni altération de la pile en entrée |
| [sujet 11](annales/2021/21_NSI_11) | obtention de l'écriture binaire d'un entier sous forme de liste de zéros et de un et de son nombre de chiffres | tri bulle                                                    |
| [sujet 12](annales/2021/21_NSI_12) | détermination du maximum d'une liste d'entiers et de la position de sa première occurrence | recherche d'un motif dans une chaîne                         |
| [sujet 13](annales/2021/21_NSI_13) | tri par sélection                                            | jeu interactif de plus ou moins (un entier choisi aléatoirement est à deviner par l'utilisateur en 10 tentatives maximum) |
| [sujet 14](annales/2021/21_NSI_14) | liste des occurrences d'une valeur dans un tableau           | calcul de la moyenne pondérée des notes obtenues par élève (dictionnaire de dictionnaires) |
| [sujet 15](annales/2021/21_NSI_15) | recherche de la plus petite et de la plus grande valeur dans un tableau (renvoie un dictionnaire) | modélisation d'un paquet de 32 cartes à l'aide d'une classe Carte et d'une classe PaquetDeCarte |
| [sujet 16](annales/2021/21_NSI_16) | calcul de la moyenne d'un tableau de flottants               | détermination de l'écriture binaire d'un entier positif sous forme de chaîne de caractères |
| [sujet 17](annales/2021/21_NSI_17) | indice de la première occurrence du minimum                  | tri d'un tableau de zéros et de un, par échanges             |
| [sujet 18](annales/2021/21_NSI_18) | indice de la première occurrence d'une valeur dans un tableau | insertion d'une valeur dans une copie d'un tableau trié      |
| [sujet 19](annales/2021/21_NSI_19) | recherche dichotomique d'une valeur dans un tableau non vide | codage d'un message avec le code César et un décalage arbitraire |
| [sujet 20](annales/2021/21_NSI_20) | recherche du minimum d'une liste de températures et de l'année correspondante | tester si une chaîne ou l'écriture d'un nombre en base 10 est un palindrome |
| [sujet 21](annales/2021/21_NSI_21) | nombre d'occurrences d'une valeur dans une liste             | détermination de l'écriture binaire d'un entier positif sous forme de chaîne de caractères |
| [sujet 22](annales/2021/21_NSI_22) | nombre d'occurrences d'une valeur dans une liste             | rendu de monnaie glouton itératif                            |
| [sujet 23](annales/2021/21_NSI_23) | dictionnaire du nombre d'occurrences des caractères dans un texte | fusion de deux listes triées                                 |
| [sujet 24](annales/2021/21_NSI_24) | indice de la dernière occurrence d'une valeur dans un tableau éventuellement vide | adressage IP à l'aide d'une classe                           |
| [sujet 25](annales/2021/21_NSI_25) | construction de la liste des couples d'entiers relatifs successifs consécutifs dans une liste d'entiers | "coloriage" de tous les pixels appartenant à la même composante "connexe" d'un pixel donné d'une image binaire |
| [sujet 26](annales/2021/21_NSI_26) | recherche du caractère le plus fréquent dans une chaîne de caractères | négatif et conversion en noir et blanc par seuillage d'une image en niveaux de gris |
| [sujet 27](annales/2021/21_NSI_27) | calcul de la moyenne d'une liste d'entiers                   | affichage en ligne pixelisé d'une image en noir et blanc et agrandissement |
| [sujet 28](annales/2021/21_NSI_28) | calcul de la taille d'un arbre binaire modélisé par un dictionnaire | tri par sélection                                            |
| [sujet 29](annales/2021/21_NSI_29) | suite de Syracuse d'un entier donné jusqu'à atteindre la valeur 1 | codes alphabétiques, mots parfaits, dictionnaires            |
| [sujet 30](annales/2021/21_NSI_30) | multiplication de deux entiers relatifs par additions ou soustractions | recherche dichotomique d'une valeur dans un tableau trié (récursif) |


