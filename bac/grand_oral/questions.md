# Exemples de questions pour le Grand Oral

Est reprise ici la liste des exemples de questions du document officiel du grand oral NSI. Certaines peuvent néanmoins être sujettes à discussion. **Ce ne sont que des exemples, vous ne devez pas choisir une question parmi celles-ci**.


## Exemples officiels de questions

Les questions sont classées par thème :

#### L'histoire de l'informatique

* Femmes et numérique : quelle histoire ? quel avenir ?
* Ada Lovelace, pionnière du langage informatique.
* Alan Turing, et l’informatique fut.
* Quelle est la différence entre le web 1.0 et le web 2.0 ?

#### Langages et programmation

* P = NP, un problème à un million de dollars ?
* Tours de Hanoï : plus qu’un jeu d’enfants ?
* Les fractales : informatique et mathématiques imitent-elles la nature ?
* De la récurrence à la récursivité.
* Les bugs : bête noire des développeurs ?
* Comment rendre l’informatique plus sûre ?

#### Données structurées et structures de données

* L’informatisation des métros : progrès ou outil de surveillance ?
* Musique et informatique : une alliance possible de l’art et de la science ?

#### Algorithmique

* Comment créer une machine intelligente ?
* Comment lutter contre les biais algorithmiques ?
* Quels sont les enjeux de la reconnaissance faciale (notamment éthiques) ?
* Quels sont les enjeux de l’intelligence artificielle ?
* Transformation d’images : Deep Fakes, une arme de désinformation massive ? La fin de la preuve par l’image ?
* Qu’apporte la récursivité dans un algorithme ?
* Quel est l’impact de la complexité d’un algorithme sur son efficacité ?

#### Bases de données

* Données personnelles : la vie privée en voie d’extinction ?
* Comment optimiser les données ?

#### Architectures matérielles, systèmes d’exploitation et réseaux

* L’ordinateur quantique : nouvelle révolution informatique ?
* La course à l’infiniment petit : jusqu’où ?
* Peut-on vraiment sécuriser les communications ?
* Quelle est l’utilité des protocoles pour l’internet ?
* Cyberguerre : la 3e guerre mondiale ?

#### Interfaces Hommes-Machines (IHM)

* Smart cities, smart control ?
* La réalité virtuelle : un nouveau monde ?
* La voiture autonome, quels enjeux ?

#### Impact sociétal et éthique de l'informatique

* Comment protéger les données numériques sur les réseaux sociaux ?
* Quelle est l’empreinte carbone du numérique en termes de consommation ?
* Pourquoi chiffrer ses communications ?
* Les réseaux sociaux sont-ils compatibles avec la politique ?
* Les réseaux sociaux sont-ils compatibles avec le journalisme ?
* Les réseaux sociaux permettent-ils de lutter contre les infox ?
* L’informatique va-t-elle révolutionner le dessin animé ?
* L’informatique va-t-elle révolutionner la composition musicale ?
* L’informatique va-t-elle révolutionner l’art ?
* L’informatique va-t-elle révolutionner le cinéma ?
* L’informatique va-t-elle révolutionner la médecine ?
* L’informatique va-t-elle révolutionner la physique ?
* L’informatique va-t-elle révolutionner l’entreprise ?
* Le numérique : facteur de démocratisation ou de fractures sociales ?
* Informatique : quel impact sur le climat ?


## Questions non valides

Je rappelle que la question ne doit pas juste reprendre un point du cours, ni être un "exposé".

Voici par exemple deux questions non valides qui seraient refusées au Grand Oral :

* Quelle fut la vie d'Alan Turing ?
* Comment utiliser des jointures dans une base de données ?


## Questions des années précédentes

Pour encore plus d'exemples, voici une liste des questions des élèves de terminale des années précédentes. Il n'est techniquement pas interdit de vous en inspirer, mais je vous le déconseille : n'oubliez pas que vous devez justifier votre choix de question, et même faire le lien avec votre projet d'orientation : **votre choix de question doit donc être personnel**.

#### 2021

* Pourquoi Javascript est-il devenu le langage le plus utilisé au monde ?
* Pourquoi chiffrer les communications ?
* L'informatique va-t-elle révolutionner le dessin animé ?
* Comment l'informatique a-t-elle révolutionné la vie des gens dans le domaine du divertissement au fil des années ?
* Comment fonctionnent les bases de données distribuées ?
* Nos informations personnelles sont-elles en sécurité dans les bases de données ?
* Comment l'informatique s'inspire du vivant ?
* Enigma et décryptage.
* Comment évolue la cyber-sécurité ?
* Modélisation 3D et graphes.
* L'art algorithmique est-il vraiment un art ?
* Comment protéger les données numériques sur les réseaux ?
* Quels sont les enjeux des cyberguerres ?
* Fractales, l'info et les maths imitent-elles la nature ?
* Les réseaux sociaux sont-ils compatibles avec la politique ?
* Avenir de la réalité virtuelle
* Les réseaux sociaux sont-ils compatibles avec le journalisme ?
* Cryptologie et deuxième guerre mondiale
* Comment fonctionne Tor ?
* Comment flash est-il passé du statut d'utilisable à mort en 2021 ?
* Pourquoi Python a-t-il été inventé ?
* Comment l'automate cellulaire peut-il imiter les êtres vivants par de simples règles informatiques ?
* HTML 5 et révolution du développement web

#### 2022

* Comment les évolutions technologiques affectent-elles la voiture autonome ?
* Comment les crypto-monnaies peuvent-elles être sécurisées ?
* Quel est l'impact de l'IA dans notre société ?
* L'arrivée de l'IA peut-elle perturber la société ?
* La cyberguerre peut-elle être un danger pour la société ?
* Comment l'évolution de l'informatique va permettre l'émergence du neuro-gaming ?
* Cybersécurité : comment l'UE lutte contre les cybermenaces ?
* Comment une pénurie peut-elle affecter le domaine de l'informatique
* Le commerce et l'entreprise dépendent-ils du numérique aujourd'hui ?
* Comment est utilisé le langage Ruby actuellement ?
* Quels sont les dangers de l'influence des réseaux sociaux sur les utilisateurs ?
* Comment infiltrer le réseau d'une entreprise ?
* Quel est le rôle du dark web dans les guerres ?
* Quels sont les moyens pour se protéger dans l'informatique de nos jours ?
* Quel langage est à l'origine de nos langages de programmation actuels ?
* Peut-on considérer que les cyberattaques représentent un nouveau visage de la guerre ?
* Comment la pénurie des cartes graphiques est-elle survenue ?
* Quelle est l'influence de l'informatique dans les sports ?
* Peut-on se passer de l'informatique dans le développement d'un dessin animé aujourd'hui ?

* L'informatique quantique est-elle utopique ?
* Un robot sait-il apprendre comme un humain ?
* Quels sont les enjeux de l'intelligence artificielle ?
* Données personnelles : la vie privée en voie d'extinction ?
* Qu'est ce que le machine learning ?
* Le blockchain a-t-il une utilité ?
* Comment les technologies améliorent-elles le e-commerce ?
* Est-ce que les crypto-monnaies peuvent remplacer les monnaies centrales ?
* Comment les révélations d'Edward Snowden ont-elles prouvé que les données personnelles sont menacées ?
* Comment est généré l'aléatoire ?
* L'ordinateur quantique, une révolution de l'informatique ?
* IA, le futur de l'informatique ?
* Comment se développe la cybersécurité ?
