# Grand Oral

## Présentation

Le grand oral est donc une épreuve orale qui se déroulera en juin après les épreuves écrites. Son coefficient est de 10 (sur un total de 100).

Vous devez préparer **deux questions** en rapport avec vos spécialités de terminale.

Ces questions doivent porter sur les deux enseignements de spécialité soit pris isolément, soit abordés de manière transversale. Il est donc nécessaire d'aborder les deux enseignements de spécialité, et il est possible de les mélanger. Néanmoins il est nécessaire (pour des raisons d'organisation, voir plus bas) d'avoir une spécialité majeure différente dans chaque question.  
Voici donc le schéma des répartitions possibles des spécialités (A et B) dans les deux questions (1 et 2) :

|  Question 1   |   Question 2  |
| :---: | :---: |
|  A   |  B   |
|  A   |  majeure B et mineure A  |
|  majeure A et mineure B   |  B   |
| majeure A et mineure B    |  majeure B et mineure A   |


## Déroulé

Vous serez évalué par un jury composé d'un professeur d'une de vos spécialités et d'un professeur d'une autre matière. Le jury choisira donc la question pour laquelle il est compétent.

### Préparation : 20 mn

Une fois que vous connaissez la question choisie par le jury, vous avez **20 minutes pour mettre vos idées au clair**. Vous avez la possibilité de réaliser un support sur une feuille de brouillon fournie. Vous devrez cependant donner ce support au jury au début de l'entretien, *vous ne pourrez pas le garder sur vous*.

### Passage : 20 mn

L'entretien se déroule en trois temps. Tout l'entretien se fait **sans notes et sans support**.

#### Présentation de la question : 5 mn

Vous disposez de 5 minutes pour :

* expliquer pourquoi vous avez choisi cette question ;
* développer la question ;
* y répondre.

Le jury ne vous interrompt pas sauf si vous dépassez du temps imparti. Tout cela se fait sans notes et sans support.


#### Echange avec le jury : 10 mn

Pendant ce temps, le jury vous interroge sur votre question, il demande des précisions. Il élargit ensuite les questions au thème abordé, puis à **tout le programme de terminale et première**.

Il faut donc connaître votre sujet de manière approfondie mais aussi avoir en tête tout le programme de NSI (y compris les chapitres que l'on a fait après les écrits du bac).


#### Présentation du projet d'orientation : 5 mn

Pendant les cinq dernières minutes, vous devez présenter votre projet d'orientation.

Vous devez expliquer les étapes de la maturation de votre projet d'orientation et détailler votre projet après le bac :
* pourquoi la filière générale ?
* comment avez-vous choisi vos spécialités en Première ?
* pourquoi avez-vous choisi de conserver ces deux spécialités là en Terminale ?
* quel type d'études post-bac visiez-vous ? dans quel domaine ?
* votre projet a-t-il changé à un moment donné ? pourquoi ?
* avez-vous fait des stages en lien avec votre projet d'orientation ?
* etc.

Il est *fortement conseillé* de faire un lien entre la question traitée et votre projet d'orientation.


## Critères d'évaluation

Lors de votre présentation, le jury évalue **les capacités argumentatives et les qualités oratoires du candidat**. Vous n'êtes pas évalué ici sur le fond, mais plutôt sur la forme (je rappelle qu'il y aura vraisemblablement un membre du jury qui n'aura rien compris à ce que vous avez dit).

Lors de l'échange avec le jury, il évalue **la solidité des connaissances et les capacités argumentatives du candidat**. Vous êtes donc évalué ici sur vos connaissances, je rappelle sur *tout* le programme de NSI.

Lors de l'échange sur votre projet d'orientation, le jury mesure **la capacité du candidat à conduire et exprimer une réflexion personnelle témoignant de sa curiosité intellectuelle et de son aptitude à exprimer ses motivations**. Il n'évalue surtout pas votre projet d'orientation en lui-même.

Voici la grille d'évaluation *indicative* (ce n'est pas un barème, simplement une aide pour que vous sachiez où vous vous situez dans ces critères) :

|                        |    Qualité orale de l'épreuve                                                                                                                                                                                                                |    Qualité de la prise de parole en continu                                                                |    Qualité des connaissances                                                                                                                                            |    Qualité de l'interaction                                                                                                                                                  |    Qualité et construction de l'argumentation                                                                                 |
|------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------|
|    très insuffisant    |    Difficilement audible sur l'ensemble de la prestation.    Le candidat ne parvient pas à capter l'attention.                                                                                                                               |    Enoncés courts, ponctués de pauses et de faux démarrages ou énoncés longs à la syntaxe mal maîtrisée.   |    Connaissances imprécises, incapacité à répondre aux questions, même avec une aide et des relances.                                                                   |    Réponses courtes ou rares. La communication repose principalement sur l'évaluateur.                                                                                       |    Pas de compréhension du sujet, discours non argumenté et décousu.                                                          |
|    insuffisant         |    La voix devient plus audible et intelligible au fil de l'épreuve mais demeure monocorde.    Vocabulaire limité ou approximatif.                                                                                                           |    Discours assez clair mais vocabulaire limité et énoncés schématiques.                                   |    Connaissances réelles, mais difficulté à les mobiliser en situation à l'occasion des questions du jury.                                                              |    L'entretien permet une amorce d'échange. L'interaction reste limitée.                                                                                                     |    Début de démonstration mais raisonnement lacunaire.    Discours insuffisamment structuré.                                  |
|    satisfaisant        |    Quelques variations dans l'utilisation de la voix ; prise de parole affirmée. Il utilise un lexique adapté.    Le candidat parvient à susciter l'intérêt.                                                                                 |    Discours articulé et pertinent, énoncés bien construits.                                                |    Connaissances précises, une capacité à les mobiliser en réponses aux questions du jury avec éventuellement quelques relances                                         |    Répond, contribue, réagit. Se reprend, reformule en s'aidant des propositions du jury.                                                                                    |    Démonstration construite et appuyée sur des arguments précis et pertinents.                                                |
|    très satisfaisant   |    La voix soutient efficacement le discours.    Qualités prosodiques marquées (débit, fluidité, variations et nuances pertinentes, etc.).    Le candidat est pleinement engagé dans sa parole. Il utilise un vocabulaire riche et précis.   |    Discours fluide, efficace, tirant pleinement profit du temps et développant ses propositions.           |    Connaissances  maîtrisées, les réponses aux questions du jury témoignent d'une  capacité à mobiliser ces connaissances à bon escient et à les exposer  clairement.   |    S'engage dans  sa parole, réagit de façon pertinente. Prend l'initiative dans  l'échange. Exploite judicieusement les éléments fournis par la situation  d'interaction.   |    Maîtrise des enjeux du sujet, capacité à conduire et exprimer une argumentation personnelle, bien construite et raisonnée. |


## Choix de la question

Le BO indique : « Les questions mettent en lumière un des grands enjeux du ou des programmes des enseignements de spécialité. Elles sont adossées à tout ou partie du programme du cycle terminal ».

Les questions doivent donc avoir un lien avec le programme de terminale ou éventuellement de première. La question de NSI (ou de majeure NSIet mineure votre autre spé) doit donc s'inscrire dans les thèmes suivants :

Pour la terminale :

* histoire de l'informatique ;
* structures de données ;
* bases de données ;
* architectures matérielles, systèmes d’exploitation et réseaux ;
* langages et programmation
* algorithmique

Pour la première :

* histoire de l'informatique ;
* représentation des données : types et valeurs de base ;
* représentation des données : types construits ;
* traitement de données en tables ;
* interactions entre l’homme et la machine sur le Web
* architectures matérielles et systèmes d’exploitation
* langages et programmation
* algorithmique

Le BO précise aussi : « L’entrée choisie par l’élève peut être variée : le choix du champ disciplinaire dans un parcours d’orientation ; des exemples de notions mathématiques qui ont changé son regard ou lui ont apporté des clés de lecture ; des obstacles didactiques auxquels il a été confronté ; une notion du programme ; un point de l’histoire des sciences ; une démonstration ; un lien avec une autre spécialité, une attention portée à une notion pour ses enjeux sociétaux ou dans un parcours d’orientation comme l’éducation à la santé, au développement durable, aux médias et à l’information, aux problèmes bioéthiques ».

Si la question rentre dans ces critères, elle peut donc être valide.

**La question doit être une question ouverte en lien avec les thèmes du programme**. Elle ne peut pas être une question du cours ni un approfondissement d'un point du cours. Cela ne doit pas donner lieu à un exposé.

Vous pouvez aller sur cette page d'exemples de questions si vous le souhaitez, mais ce sont des exemples officiels, **vous ne devez pas choisir votre question parmi celles-ci** : [exemples de questions](questions.md)

## Documents officiels

Pour plus de lecture sur le Grand Oral, voici les liens vers tous les documents officiels :

* Page de présentation du Grand Oral : [ici](https://www.education.gouv.fr/baccalaureat-2021-epreuve-du-grand-oral-permettre-aux-eleves-de-travailler-une-competence-89576)
* Bulletin Officiel définissant le Grand Oral (avec grille d'évaluation notamment) : [ici](https://www.education.gouv.fr/bo/20/Special2/MENE2002780N.htm)
* Objectifs du Grand Oral : [ici](https://eduscol.education.fr/media/3925/download)
* Liste officielle d'exemples de questions : [ici](https://eduscol.education.fr/media/3919/download)
