# Épreuve écrite

L'épreuve écrite compte pour **12 points** sur 20 de la note de NSI.

Elle dure **3 heures 30** et consiste en la résolution de **3 exercices** (chacun sur 4 points).

Elle se déroule au cours du deuxième trimestre, à la même période que l'épreuve pratique.



## Contenu

Le sujet propose **5 exercices**, parmi lesquels le candidat choisit les 3 qu'il traitera. Ces 5 exercices permettent d'aborder les  différentes rubriques des **programmes de Première et de Terminale**, sans obligation d'exhaustivité.

Le sujet **comprend obligatoirement** au moins un exercice relatif à chacune des trois rubriques suivantes :

* traitement de données en tables et bases de données
* architectures matérielles, systèmes d'exploitation et réseaux
* algorithmique, langages et programmation

Le sujet ne pourra par contre **jamais comprendre** les notions suivantes :

* sécurisation des communications
* notion de programme en tant que donnée, calculabilité, décidabilité

* paradigmes de programmation
* algorithmes sur les graphes
* programmation dynamique
* recherche textuelle



## Annales

Vous pouvez trouver en ligne les sujets des années précédentes ([ici par exemple](https://www.sujetdebac.fr/annales/specialites/spe-numerique-informatique/2021/)).