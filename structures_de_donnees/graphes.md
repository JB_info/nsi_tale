# Structures de données : graphes



Les graphes représentent une grande partie du programme de terminale car nous allons voir beaucoup d'algorithmes concernant les graphes. Dans un premier temps, nous allons découvrir les graphes pour aller jusqu'à ceux qui nous intéressent particulièrement cette année : les arbres binaires. Dans un second temps, nous verrons des algorithmes utilisés avec les  graphes.

La théorie des graphes a commencé en 1735 avec Leonhard Euler, alors qu'il cherchait à résoudre le problème des sept ponts de Königsberg.



## I. Définition

Un **graphe** est un objet mathématique très simple avec lequel on peut faire des choses extrêmement compliquées. Il est constitué de **sommets** reliés par des **arêtes** (ou liens).

Si les arêtes sont fléchées, on les appelle **arcs** et on parle de **graphe orienté** ; sinon on parle de **graphe non-orienté**. Dans un graphe orienté, on appelle *prédécesseur* d'un sommet, les sommets qui pointent vers lui. On appelle *successeur* d'un sommet, les sommets vers lesquels il pointe ($`prédécesseur  \rightarrow successeur`$).

Un graphe est une structure dite **relationnelle**. Un graphe peut représenter un réseau social, des liens d'amitié, un réseau routier, un réseau informatique...

​	**1)** Pour le graphe ci-dessous, dire s'il est orienté ou non, donner le nombre de sommets et le nombre d'arêtes/arcs.

​	![q1](img/q1.png)

​	**2)** Pour le graphe ci-dessous, dire s'il est orienté ou non, donner le nombre de sommets et le nombre d'arêtes/arcs.

​	![q2](img/q2.png)

On utilisera parfois des graphes **pondérés** pour représenter par exemple des distances sur une carte. Dans un tel graphe, les arêtes sont associées à un poids :

​	![graphe pondéré](img/pondere.png)

​	**3)** Autre que les distances sur un graphe de réseau routier, que peut permettre de représenter un graphe pondéré ?

On dit qu'un graphe est **complet** si ses sommets sont tous reliés deux à deux par une arête.

​	**4)** Parmi les graphes suivants, dire lesquels sont complets.

![complet](img/complet1.png) ![complet](img/complet2.png) ![complet](img/complet3.png)

## II. Représentation

Nous avons utilisé intuitivement une représentation visuelle d'un graphe. Mais nous aurions pû utiliser une description textuelle d'un graphe. Par exemple pour celui de la question 2 :

- A est relié à E ;
- B est relié à C, D et E ;
- C est relié à B, D et E ;
- D est relié à C et B ;
- E est relié à A, B et C.

​	**5)** Construisez un graphe de réseau social à partir des informations suivantes :
​	Tom est ami avec Eve et Max. Eve est ami avec Tom et Sam. Sam est ami avec Eve, Camille et Dean. Dean est ami avec Sam, Camille et Max. Max est ami avec Tom, Dean et Camille. Camille est ami avec Sam, Dean et Max.
​	Le graphe est-il orienté ? Pourquoi ?

​	**6)** Construisez un graphe de réseau social à partir des informations suivantes :
​	Audrey est abonné à Ben et Ethan. Ben est abonné à Charles. Charles est abonné à Ben, Karima et Tam. Tam est abonné à Karima et Ethan. Ethan est abonné à Audrey et Karima.
​	Le graphe est-il orienté ? Pourquoi ?

​	**7)** Construisez un graphe de réseau routier à partir des informations suivantes :
​	Calais est situé à 109km de Lille, à 158km d'Amiens et à 42km de Dunkerque. Amiens est situé à 198km de Dunkerque et à 141km de Lille. Lille est situé à 76km de Dunkerque.
​	Quelle est la particularité de ce graphe ?



## III. Implémentations

On parle d'implémentation lorsqu'il s'agit de « représenter » un graphe dans un système informatique. Même si nous ne verrons pas directement comment on implémente un graphe en Python nous allons parler des deux principales implémentations d'un  graphe.

### 1. Matrice d'adjacence

Une matrice n'est qu'un tableau à double entrée :

​	![matrice exemple](img/matrice.png)

Nous avons ci-dessus une matrice à 3 lignes et 4 colonnes. En mathématiques on utilise les matrices pour effectuer des calculs,  nous les utiliserons ici simplement comme des tableaux à deux dimensions.

Une matrice d'adjacence est une matrice carrée (même nombre de lignes que de colonnes) où chaque ligne et chaque colonne représentent un sommet. Pour un graphe non-orienté on mettra un « 1 » sur la case `[i][j]` s'il y a une arête entre le sommet i et le sommet j, et un « 0 » sinon. Pour un graphe orienté on mettra un « 1 » sur la case `[i][j]` s'il y a une arête du sommet i vers le sommet j, et un « 0 » sinon. Pour un graphe pondéré on mettra sur la case `[i][j]` le poids du lien entre le sommet i et le sommet j.

​	**8)** Donnez la matrice d'adjacence du graphe ci-dessous.

​	![graphe q2](img/q2.png)

​	**9)** Donnez la matrice d'adjacence du graphe ci-dessous.

​	![graphe q1](img/q1.png)

​	**10)** Donnez la matrice d'adjacence du graphe ci-dessous.

​	![graphe pondéré](img/pondere.png)

​	**11)** Soit la matrice d'adjacence d'un graphe G composé des sommets A, B, C, D  :
​	![matrice adjacence](img/matrice_adj.png)
​	Le graphe G est-il orienté ou non-orienté ? Justifiez votre réponse.
​	Représentez ce graphe G.

### 2. Liste d'adjacence

Pour un graphe non orienté, une liste d'adjacence associe à chaque sommet la liste de ses voisins. Par exemple, pour le graphe ci-dessous :

​	![graphe ex adjacence](img/adjacence.png)

La liste d'adjacence serait :

- A → [B, C, E] ;
- B → [A] ;
- C → [A, D] ;
- D → [C, E] ;
- E → [A, D].

Pour un graphe orienté, une liste d'adjacence associe à chaque sommet la liste de ses successeurs, c'est à dire les sommets vers lesquels il pointe. Il est également possible de faire une liste d'adjacence des prédécesseurs, ou même les deux pour des raisons de symétrie.

​	**12)** Donnez la liste d'adjacence du graphe ci-dessous.

​	![graphe q2](img/q2.png)

​	**13)** Donnez la liste d'adjacence des successeurs du graphe ci-dessous.

​	![graphe q1](img/q1.png)

​	**14)** Représentez le graphe correspondant à la liste d'adjacence suivante :
 - A → [B] ;
 - B → [A, C, D, E] ;
 - C → [];
 - D → [A, C] ;
 - E → [B, C, D].

### 3. Choix d'implémentation

On utilisera une matrice d'adjacence dans le cas d'un graphe avec beaucoup de liens (on parle de graphe dense) et une liste d'adjacence dans le cas contraire. Cela permettera de gagner de la mémoire.

Le choix peut aussi dépendre de l'utilité du graphe. En fonction de l'algorithme utilisé, on pourra préférer une implémentation à l'autre.

​	**15)** Donnez la matrice d'adjacence du graphe dont la liste d'adjacence est la suivante :

 - A → [B, C] ;

 - B → [C] ;

 - C → [B];

 - D → [B, C] ;

​	**16)** Donnez la liste d'adjacence du graphe dont la matrice d'adjacence est la suivante :
   ![q16](img/q16.png)



## IV. Propriétés

Les propriétés les plus communes pour parler d'un graphe sont les suivantes :

- une **chaîne** (ou *chemin*) est une suite d'arêtes consécutives ;
- la longueur d'une chaîne est le nombre d'arêtes qui la compose ;
- la **distance** entre deux sommets est la longueur de la chaîne la plus courte entre ces deux sommets ;
- un **cycle** est une chaîne fermée, c'est à dire qui commence et termine par le même sommet.

​	**17)** Pour le graphe ci-dessous, trouvez deux cycles. Donnez également la distance entre A et D.

​	![img](https://kxs.fr/cours/graphes/img/graphe_non_oriente.svg)

Nous verrons les algorithmes qui permettent de rechercher une chaîne, de repérer un cycle et de calculer une distance plus tard dans l'année.



## V. Pour aller plus loin

​	**18)** Si vous voulez en savoir plus sur l'origine de la découverte des graphes, vous pouvez regarder cette vidéo : [Briller en Société #27: Les 7 ponts de Königsberg](https://www.youtube.com/watch?v=F1G4srEXq2s).

​	**19)** Pour vous entraîner, vous pouvez donner les matrices d'adjacence et listes d'adjacence des graphes des questions 5 - 6 - 7.

​	**20)** À votre avis, quelle structure de données utiliserait-on pour implémenter une matrice d'adjacence en Python ? Et pour une liste d'adjacence ?



---

Cours inspiré du travail de Thomas Beline : [kxs](https://kxs.fr/cours/)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
