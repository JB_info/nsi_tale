# Structures de données : arbres

Les arbres sont une structure de données dite **hiérarchique**.

## I. Définition

En informatique nous parlerons d'un type de graphe particulier : les *arbres enracinés* ou *arborescence*. Par abus de langage on omettra souvent le terme « enraciné » et nous parlerons simplement d'**arbre**. En voici une définition :

Un arbre (enraciné) ou une arborescence est un **graphe orienté sans cycle** dont :

- un seul sommet n'a aucun parent, on l'appellera la **racine** ;
- tous les autres sommets n'ont qu'un seul parent.

Cette définition fut établie par Arthur Cayley en 1889.

Voici donc un arbre enraciné :

​	![arbre fléché](img/arbre_fleche.png)

A est ici la racine.



## II. Informatique

En informatique, nous simplifierons la représentation des arbres en omettant les flèches et en mettant la racine en haut. Voici le même arbre simplifié :

​	![arbre exemple](img/arbre_exemple.png)

Une arborescence (arborescence de fichiers gérée par le système d'exploitation comme vous l'avez vu en première) est donc un arbre :

```bash
/
├── home
│	├── eleve
│	│	└── fichier.txt
│	└── administrateur
├── lost+found
├── media
└── var
	└── www
		└── index.html
```

​	**1)** Dessinez l'arbre correspondant à l''arborescence précédente.



## III. Vocabulaire et propriétés

* pour les graphes on parle de *sommets*, pour les arbres on parle de **nœuds**
* les **feuilles** sont les nœuds ne possédant pas de fils ;
* les **nœuds internes** sont les nœuds possédant des fils.

Pour l'arbre ci-dessus, A est la racine, A, C et D sont des nœuds internes et B, E, F et G sont des feuilles.



Voici les principales propriétés d'un arbre :

- la **profondeur** d'un nœud est la distance (nombre d'arêtes) entre la racine et ce nœud ;
- la **hauteur** d'un arbre est la plus grande profondeur d'une feuille de l'arbre ;
- la **taille** d'un arbre est le nombre de nœuds.

Pour l'arbre ci-dessus, la profondeur de A est 0 et celle de E est 2. Sa hauteur est 2 et sa taille est 7.



​	**2)** Pour l'arbre ci-dessous, répondre aux questions suivantes :

- Quelle est la racine ?
- Combien y a-t-il de feuilles ?
- Qui sont les fils de D ?
- Quelle est la profondeur de H ?
- Quelle est la profondeur de P ?
- Quelle est la hauteur de cet arbre ?
- Quelle est la taille de cet arbre ?

​	![arbre q2](img/arbre1.png)

​	**3)** Pour l'arborescence linux ci-dessous, répondre aux questions suivantes :

- Quelle est la racine ?
- Combien y a-t-il de feuilles ?
- Quelle est la profondeur de `index.html` ?
- Quelle est la profondeur de `administrateur` ?
- Quelle est la hauteur de cet arbre ?
- Quelle est la taille de cet arbre ?

```bash
/
├── home
│	├── eleve
│	│	└── fichier.txt
│	└── administrateur
├── lost+found
├── media
└── var
	└── www
		└── index.html
```



## IV. Arbres binaires

### 1. Définition et propriétés

Les **arbres binaires** sont des arbres dont chaque nœud a *au maximum deux fils*. Voici un exemple d'arbre binaire :

​	![arbre binaire](img/arbre_binaire.png)

À partir d'un nœud qui n'est pas une feuille, on peut définir le **sous-arbre gauche** et le **sous-arbre droit**. Pour le graphe ci-dessus, le sous-arbre gauche de B contient les nœuds D, H, I, O et P, alors que son sous-arbre droit contient les nœuds E et J.

​	**4)** Dessinez le sous-arbre droit de D et le sous-arbre gauche de C.

### 2. Modéliser une situation en arbre

​	**5)** Dessinez votre arbre généalogique. Cet arbre est-il binaire ?

​	**6)** Dessinez l'arbre de la phase finale de la dernière ligue des champions. Cet arbre est-il binaire ?

​	**7)** Dessinez un arbre permettant de décomposer cette opération mathématique : 2 + (3 * (7 - x)). Cet arbre est-il binaire ?

### 3. Encadrement de la hauteur

Il est possible d'avoir des arbres binaires de même taille, mais de "forme" très différente :

![arbre filiforme](img/filiforme.png) ![arbre complet](img/complet.png)

Sur le schéma ci-dessus le premier arbre est dit **filiforme** alors que le deuxième est dit **complet** (un arbre binaire est complet si  tous les nœuds possèdent 2 fils et toutes les feuilles se situent à la même profondeur). On pourra aussi dire que le premier arbre est déséquilibré alors que le second est équilibré.

Si on prend un arbre filiforme de taille `n`, on peut dire que la hauteur de cet arbre est égale à `n−1`. Dans le cas de l'arbre filiforme ci-dessus, nous avons `n=7` et donc la hauteur est 7-1=6.

Si on prend un arbre complet de taille `n`, on peut démontrer que la hauteur de cet arbre est égale à la partie entière de $`log_2(n)`$ Dans le cas de l'arbre complet ci-dessus, nous avons $`log_2(7) = 2,8`$ donc en prenant la partie entière on a bien la hauteur de l'arbre égale à 2.

Un arbre filiforme et un arbre complet étant deux cas extrêmes, on peut affirmer que pour un arbre binaire quelconque :

$`⌊log_2(n)⌋ \leq hauteur \leq n-1`$ (avec n la taille de l'arbre, et $`⌊log_2(n)⌋`$ permet de prendre la partie entière du logarithme base 2 de n).

​	**8)** Quelle est la hauteur minimale d'un arbre possédant 4 nœuds ? la hauteur maximale ? Dessinez l'ensemble des arbres possibles ayant 4 nœuds pour vérifier cet encadrement.

### 4. Pour aller plus loin

Mais pourquoi la hauteur d'un arbre complet de taille `n`est $`⌊log_2(n)⌋`$ ?

On peut étiqueter les nœuds d’un arbre de la façon suivante :

* la racine porte l’étiquette 0 ;
* si un nœud quelconque porte l’étiquette x...yz, la racine de son fils gauche porte l’étiquette x...yz0 et la racine
  de son fils droit l’étiquette x...yz1.

On observe que les étiquettes des nœuds de profondeur `p` sont constituées de `p+1` bits.
Les étiquettes peuvent être vues comme les écritures de nombres en base 2 : on a ainsi numéroté les différents nœuds de l’arbre.

![arbre numéroté](img/arbre_numerote.png)

Si on convertit en décimal les étiquettes des nœuds, on constate qu’on les a simplement numérotés de 1 à 7, de haut en bas et de gauche à droite.

On voit donc que l’étiquette du dernier nœud, de profondeur maximale, est `111` ce qui est l’écriture binaire de 7 : la taille de l'arbre !

Or le logarithme en base 2 d’un entier correspond au nombre de bits nécessaire à son écriture en base 2 diminué d’une
unité.

Ici on a bien la taille `n = 7` qui s'écrit en base 2 sur 3 bits (`111`) donc, diminué d'une unité, on trouve alors 2 : cela correspond bien à la hauteur de l'arbre.

​	**9)** Pour chacun des arbres binaires suivants, donnez la taille et la hauteur de l'arbre ainsi que la profondeur des nœuds 1, 2, 5, 7, 10, 13 et 14.
​	![arbres q9](img/exo_arbres.png)

​	**10)** Quel paradigme de programmation utiliseriez-vous pour manipuler des arbres binaires en Python ?



---

Cours inspiré du travail de Thomas Beline : [kxs](https://kxs.fr/cours/)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)