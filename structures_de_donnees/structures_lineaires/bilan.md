# Structures linéaires de données : bilan



## I. Interface VS Implémentation

Pour chaque structure de données linéaire (tableau, liste, pile, file), nous avons vu d'abord son interface puis au moins un exemple d'implémentation.

L'**interface** d'une structure de données définit la façon dont on interagit avec cette structure. Cette interface est composée d'un ensemble de méthodes primitives. On manipule alors un type abstrait de données, qui n'existe pas physiquement dans l'ordinateur.

Afin de rendre la structure utilisable par une machine, il faut l'implémenter dans un langage de programmation. Cette **implémentation** dépend du langage utilisé mais également de ce qu'on souhaite faire avec la structure : selon l'implémentation choisie, les méthodes de la liste seront plus ou moins efficaces et bien choisir l'implémentation rendra l'algorithme qui l'utilisera plus ou moins *performant*.

​	

## II. Coût des opérations

Nous allons résumer les coûts des différentes opérations dans les structures de données linéaires en fonction de leur implémentation.

### 1. Tableaux (= type list de Python vu en 1ère)

| Action                         | Coût |
| ------------------------------ | :--: |
| Insertion en fin de liste      | O(1) |
| Insertion à la position i      | O(n) |
| Insertion en début de liste    | O(n) |
| Suppression du dernier élément | O(1) |
| Suppression de l'élément i     | O(n) |
| Recherche d'un élément         | O(n) |

### 2. Listes chainées

| Action                         | Coût |
| ------------------------------ | :--: |
| Insertion en fin de liste      | O(n) |
| Insertion à la position i      | O(n) |
| Insertion en début de liste    | O(1) |
| Suppression du dernier élément | O(n) |
| Suppression de l'élément i     | O(n) |
| Recherche d'un élément         | O(n) |

### 3. Piles (peu importe l'implémentation)

On ne s'intéresse ici qu'à l'empilement et au dépilement car si on veut faire davantage de choses, on utilisera une liste.

| Action     | Coût |
| ---------- | :--: |
| Empilement | O(1) |
| Dépilement | O(1) |

### 4. Files (implémentées avec une liste chainée)

Ici on considère une File implémentée avec une liste chaînée car avec un tableau dynamique, cela ne serait pas intéressant du point de vue de la complexité. Comme pour la pile, on ne s'intéresse ici qu'à l'enfilement et au défilement car si on veut faire davantage de choses, on utilisera une liste.

| Action     | Coût |
| ---------- | :--: |
| Enfilement | O(1) |
| Défilement | O(1) |

