# Structures linéaires de données : exercices



## Exercice 1 : Pile et alphabet

On forme une pile en empilant successivement toutes les lettres de l'alphabet par ordre alphabétique en commençant donc par le A. 

1. On dépile une lettre. Quelle est cette lettre ?
2. On dépile maintenant 5 lettres. Quel élément se trouve alors au sommet de la pile ?



## Exercice 2 : File et alphabet

Recommencez l'exercice 1 avec une file.



## Exercice 3 : choix d'une structure

Pour chacune des situations suivantes, donnez la structure de données la plus adaptée.

1. La gestion du flux de personnes arrivant dans une préfecture.
2. Les opérations effectuées dans un logiciel de dessin pour que l'on puisse les annuler.
3. Envoyer des fichiers à un serveur d'impression.
4. L'option "Reculer d'une page" des navigateurs.



##  Exercice 4 : manipulations de piles


On se donne trois piles _P1_, _P2_ et _P3_. 

* La pile _P1_ contient des nombres entiers positifs empilés, par exemple 4 9 2 5 3 et 8, 8 étant au dessus de la pile.
* Les piles _P2_ et _P3_ sont initialement vides. 

### Partie 1 : on ne code pas.

On utilise une piles de papiers sur lesquels on écrit les valeurs présentes dans la pile _P1_. En manipulant les papiers (dépilant, empilant ...) **trouvez les manipulations** à faire pour réaliser les exercices ci-dessous. Ecrivez, ensuite, en **pseudo-code**, l'algorithme à réaliser dans chaque cas.

1. On veut inverser l'ordre des éléments de la pile _P1_.

2. On veut déplacer un entier sur deux dans _P2_ ou _P3_. 

3. On veut déplacer les entiers de _P1_ de façon à avoir les entiers pairs dans _P2_ et les impairs dans _P3_. 

4. On veut faire la même chose mais les nombres dans _P2_ et _P3_ doivent être dans le même ordre qu'il l'étaient dans _P1_.


### Partie 2

 Implémentez ces fonctions en Python.



## Exercice 5 : implémenter une file avec deux piles

On dispose uniquement de la structure de donnée de Pile et on souhaite créer, en utilisant deux piles P1 et P2, une nouvelle implémentation de la structure de File. On décide que la P1 contiendra les valeurs de la file. Le tête de P1 sera la tête de la file.

1. Comment vérifier que la file est vide ou pas ?

2. Comment défiler l'élément de tête de la file ?

3. Comment faire alors pour enfiler un élément en queue de file (donc en queue de P1) ?

4. Implémenter, en Python, de cette façon une classe qu'on nommera `File_avec_2_piles`, dont les méthodes sont _enfiler_, _defiler_ et _est_vide_.

     

## Exercice 6 : implémenter une pile avec un tableau

La structure de tableaux de Python possède deux méthodes particulières :

* `tableau.pop()` renvoie l'élément en queue du tableau (celui de plus grand indice) et l'enlève du tableau.
* `tableau.append(elt)` ajoute _elt_ en queue du tableau.



1. Quelle structure parmi les Piles et les Files pourrait facilement être implémentée via un tableau Python grâce à ces deux méthodes ? Pourquoi ?
2. Implémenter une Pile en utilisant un tableau Python.



## Pour aller plus loin : exercice 7

L'écriture polonaise inverse est une manière d'écrire des expressions mathématiques dans laquelle l'opérateur se trouve derrière les opérandes. 

* pour calculer  3 + 4, on écrit 3 4 + 
* 1 3 + 5 * revient à effectuer $`(1 + 3) \times 5`$

Pour simplifier, on suppose que :

*  Les seuls symboles opératoires sont ceux des 4 opérations de base. 

* Les opérandes sont des chiffres entiers. 


Notre objectif sera d'implémenter une fonction `evaluer(expression)` qui renvoie le résultat d'une expression (chaîne de caractères) écrite en notation polonaise inverse. 

### Exemples

Voici quelques expressions.

​	**1)** Quel est, dans chacun des cas suivants, le résultat attendu ? 

* 1 3 + 6 +  
*  2 5 * 8 +
*  3 4 * 2 /
* 7 3 + 4 * 5 /

​	**2)** Définissez la fonction `evaluer(expression)`, de paramètre une chaîne de caractères, dont le but sera de renvoyer le résultat de l'évaluation de l'expression passée en paramètre. On ne demande pas, pour l'instant, de coder cette fonction. En revanche, documentez la et donnez deux Doctests autres que ceux déjà proposés plus haut.

### Première pile

Nous allons construire une première pile en lisant une expression de gauche à droite.

Par exemple, si l'expression est '1 3 + 6 +' la pile obtenue s'affichera ainsi, l'élément en bas de pile étant l'entier 1 et l'élément le plus haut étant l'opérateur '+' :

```txt
+
6
+
3
1
```

​	**3)** Définissez et codez la fonction `creer_pile(expression)` dont le paramètre est une chaîne de caractères qui renvoie une pile constituée des éléments rencontrés en lisant l'expression de gauche à droite. On convertira les caractères correspondant à des chiffres en _int_.

### Deux autres piles

On va construire deux piles à partir de celle créée précédemment :

* la pile des opérations à effectuer.
* la pile des opérandes (les nombres entiers).

Par exemple, pour l'expression '3 4 + 5 *' :

* la pile construite via la fonction précédente sera :

    ```txt
    *
    5
    +
    4
    3
    ```

* la pile des opérations contient dans l'ordre + puis * :

    ```txt
    +
    *
    ```

* la pile des opérandes contient dans l'ordre 3, 4 puis 5 :

    ```txt
    3
    4
    5
    ```
    
    **4)** Déterminez la pile des opérations et celle des opérandes dans les cas suivants :
    
* 1 3 + 6 +  
* 2 5 * 8 +
* 3 4 * 2 /
* 7 3 + 4 * 5 /

​	**5)** Codez une fonction `extraire_piles(pile_polonaise)`, de paramètre une _Pile_ renvoyée par `creer_pile`, qui renvoie un tuple constitué de la pile d'opérations et de la pile des opérandes correspondantes. Documentez la fonction, donnez deux Doctests.

### Evaluation

Reprenons notre fonction `evaluer(expression)` qu'il nous reste à coder.

Le principe est le suivant :

* On construit une première pile à partir de l'expression
* On extrait de cette pile la pile de chiffres et la pile d'opérations
* tant que la pile d'opération n'est pas vide :
    * on dépile la pile de chiffre vers une variable, par exemple, _nbre1_
    * on dépile la pile de chiffre vers une variable, par exemple, _nbre2_
    * on dépile la pile d'opération vers une variable, par exemple, _op_
    * on empile dans la pile de chiffres le résultat de l'opération _op_ sur les opérandes _nbre1_ et _nbre2_
* on renvoie le dernier élément de la pile de chiffres

​	**6)** Codez la fonction `evaluer(expression)`.



_________________

Exercices inspirés de documents de Luc Bournonville.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)



