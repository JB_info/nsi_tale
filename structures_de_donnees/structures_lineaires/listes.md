# Structures de données linéaires : listes

Une **structure de données linéaire** est une structure de données permettant de regrouper des données de manière à pouvoir y accéder librement. Dans une structure de données linéaire, les éléments ont une place bien précise, il y a un **ordre**.

L'une d'elle, la **liste** est à la base de structures de données plus complexes comme la pile, la file, les arbres, etc. L'importance de la liste comme structure de données est telle qu'elle est à la base du langage de programmation [Lisp](https://fr.wikipedia.org/wiki/Lisp) (de l'anglais *list processing*) inventé en 1958 par [John Mc Carty](https://fr.wikipedia.org/wiki/John_McCarthy).



## I. Interface VS Implémentation

Une liste peut avoir les méthodes permettant de :

* créer une liste vide
* compter le nombre d'éléments présents dans une liste
* tester si une liste est vide
* ajouter un élément en tête de liste
* supprimer la tête de la liste et la renvoyer
* accéder à un élément de la liste à un indice donné
* modifier un élément de la liste à un indice donné
* supprimer un élément de la liste à un indice donné



Ces méthodes constituent l'**interface** d'une liste. Une structure de liste est une _vue de l'esprit_ : elle existe uniquement _dans la tête_ des informaticiens. Une telle _vue de l'esprit_ est appelée un **type abstrait de données**. 

Afin de rendre la structure utilisable par une machine, il faut l'**implémenter** dans le langage de programmation souhaité par l'informaticien, c'est à dire construire, dans ce langage, une classe représentant cette structure. Cette implémentation dépend du langage utilisé mais également de ce qu'on souhaite faire avec la structure :  selon l'implémentation choisie, les méthodes de la liste seront plus ou moins efficaces et bien choisir l'implémentation rendra l'algorithme qui l'utilisera plus ou moins _performant_.

​	**1)**  Quel paradigme de programmation va-t-on utiliser pour implémenter nos structures linéaires ?



## II. Tableaux

Nous allons commencer par faire un rappel sur le tableau, seule structure linéaire que vous avez déjà vue l'année dernière.

### 1. Principe

Pour stocker une séquence d'éléments, on peut utiliser une structure en **tableau** : c'est ce que vous avez vu en 1ère (le type `<list>` de Python, qui porte mal son nom puisqu'il sert à représenter des tableaux et non des listes...).



Ainsi,  si on veut conserver des données dans un tableau, on commence par _réserver_ une zone, contigüe et ordonnée, en mémoire assez importante pour y ranger ces données.



| adresse mémoire |     0     |     1     |     2     |     3     |     4     |     5     |
| :-------------: | :-------: | :-------: | :-------: | :-------: | :-------: | :-------: |
|    éléments     | élément 1 | élément 2 | élément 3 | élément 4 | élément 5 | élément 6 |



### **2. Avantages de cette structure :**

* Elle permet un accès direct et rapide à n'importe quel élément du tableau : l'élément _n_ est _rangé_ dans la zone mémoire _m_.

  **2)** Rappelez la syntaxe Python pour accéder à l'élément d'indice `i` dans un tableau `t`.

  **3)** Pour un tableau de taille `n`, quel est l'indice du premier élément ? du dernier élément ?

* Certains langages, comme _Python_, permettent de modifier _dynamiquement_ le tableau et on peut alors ajouter facilement un élément à la fin (on dit **en queue**).

  **4)** Rappelez le nom de la méthode Python permettant d'ajouter un élément en queue d'un tableau.



### **3. Inconvénients** :

* Dans certains langages de programmation, comme le _C_, la zone mémoire, une fois réservée n'est plus modifiable : le tableau est _statique_ et donc la structure de donnée est limitée en terme de nombre d'éléments dès sa création : on ne peut donc pas ajouter de nouveaux éléments si leur nombre maximum, défini initialement, est déjà atteint.

    **5)** Est-ce le cas en Python ?

* Insérer un élément à une position dans le tableau, tout particulièrement en première position (on dit **en tête**) demande un grand nombre de manipulations. Regardons le tableau ci-dessous par exemple :


    | mémoire  |  0   |   1   |  2   |  3   |  4   |  5   |
    | :------: | :--: | :---: | :--: | :--: | :--: | :--: |
    | éléments | -13  | 'abc' |  23  | 'a'  |  -1  |  3   |
    
    Je souhaite insérer en première position la valeur `2`. Il va falloir :
    
    * si nécessaire et si possible, _agrandir_ le tableau en ajoutant une zone à sa suite :
    
      | mémoire |  0   |   1   |  2   |  3   |  4   |  5   | 6 |
      | :-------------: | :--: | :---: | :--: | :--: | :--: | :--: |:--:|
      |    éléments     | -13  | 'abc' |  23  | 'a'  |  -1  |  3   | None|
    
    * _décaler_ toutes les valeurs de la listes d'un rang dans la zone mémoire :
    
        | mémoire  |  0   |  1   |   2   |  3   |  4   |  5   |  6   |
        | :------: | :--: | :--: | :---: | :--: | :--: | :--: | :--: |
        | éléments |      | -13  | 'abc' |  23  | 'a'  |  -1  |  3   |
    
    * _insérer_ l'élément dans la première _case_ de la mémoire :
    
        | mémoire  |  0   |  1   |   2   |  3   |  4   |  5   |  6   |
        | :------: | :--: | :--: | :---: | :--: | :--: | :--: | :--: |
        | éléments | `2`  | -13  | 'abc' |  23  | 'a'  |  -1  |  3   |
    
    **6)** Quel est le principal problème ici, le **coût en mémoire** (l'insertion en tête du tableau prend de la place) ou bien le **coût en temps** (les opérations pour l'insertion en tête du tableau sont longues) ?

### 4. Bilan

* Cette structure est donc adaptée lorsqu'on a besoin d'accéder rapidement à un élément de la liste. L'accès est immédiat, on dit qu'il est d'ordre 1 (1 accès mémoire seulement est nécessaire) et on note **O(1)**.

* On est capable d'ajouter efficacement une valeur en fin de liste. À nouveau l'accès est immédiat donc d'ordre **O(1)**.

* Insérer des données supplémentaires en tête du tableau est très _coûteux_ en terme de nombre d'opération à effectuer. Le coût est proportionnel à la taille du tableau, puisqu'il faut décaler tous les éléments. On dit alors que l'opération est d'ordre **O(n)**, avec `n` la taille du tableau.

  **7)** Complétez le tableau suivant avec le coût de chaque opération pour un tableau de taille `n`.

  | Méthode                        | Coût  |
  | :----------------------------- | :---: |
  | Accès à un élément d'indice i  | O(  ) |
  | Insertion en queue du tableau  | O(  ) |
  | Insertion en tête du tableau   | O(  ) |
  | Suppression du dernier élément | O(  ) |
  | Suppression du premier élément | O(  ) |



## III. Listes chaînées

Lorsqu'un informaticien parle de "liste", il ne parle en réalité pas des tableaux comme vu ci-dessus, mais des **listes chaînées**.

### 1. Principe

La structure chaînée d'une liste n'utilise pas un tableau mais une série de **maillons** chacun composé d'une valeur et d'un lien vers le maillon suivant, le dernier maillon étant vide.



![structure chaînée](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Singly-linked-list.svg/1920px-Singly-linked-list.svg.png)



On pourrait représenter cette liste de la façon suivante : (12, (99, (37, () ))).

​	**8)**  Dessinez les listes chaînées suivantes :

* (1, (2, (3, () )))
* (26, (9, () ))
* (7, () )

### 2. Avantages et inconvénients

Cette structure est particulièrement performante pour insérer une cellule n'importe où (**et particulièrement en tête*) car il suffit pour cela de :

* créer un nouveau maillon :

    ![création nouveau maillon](https://upload.wikimedia.org/wikipedia/commons/0/01/Liste_ajout1.png)

     

    

* Modifier les _liens_ entre les maillons :

    ![modifier liens](https://upload.wikimedia.org/wikipedia/commons/a/a1/Liste_ajout3.png))



Il n'y a donc pas besoin de décaler tous les autres maillons, comme c'était le cas pour un tableau : **le coût est faible et constant, quelque soit la taille de la liste chaînée**.

En revanche, pour _atteindre_ un maillon particulier de la liste, il faudra parcourir tous les maillons en partant du premier et _remonter_ la liste jusqu'au maillon désiré. L'accès à un maillon particulier via son indice dans la liste est donc beaucoup plus coûteux que pour les tableaux !

​	**9)** Complétez le tableau suivant avec le coût de chaque opération pour une liste chaînée de taille `n`.

| Méthode                        | Coût  |
| :----------------------------- | :---: |
| Accès à un élément d'indice i  | O(  ) |
| Insertion en queue de la liste | O(  ) |
| Insertion en tête de la liste  | O(  ) |
| Suppression du dernier élément | O(  ) |
| Suppression du premier élément | O(  ) |

### 3. Exercices de manipulation A LA MAIN

​	**Exercice 1 :**

Considérons une liste chaînée vide `chaine = ()` qui possède une méthode `ajout(valeur)` pour ajouter la _valeur_ en tête de liste. Comment procéder pour créer la liste `chaine = (1, (2, (3, (4, ())))) `?



​	**Exercice 2 :**

Considérons la liste chaînée `chaine = (1, (2, (3, (4, ())))) `.

Chaque maillon est définit ainsi par le couple  (_valeur_, _maillon suivant_) .

Dans un maillon donné :

* on récupère l'attribut _valeur_ en demandant : `maillon.valeur`

* on récupère l'attribut _maillon_suivant_ en demandant : `maillon.suivant`.

Seule la valeur du maillon de tête est directement accessible : comment accéder à la valeur du second maillon ? du troisième ? du n-ième ?



​	**Exercice 3 :**

Comme pour l'insertion, supprimer un maillon est particulièrement aisé avec cette structure. 

Sauriez-vous expliquer comment procéder ?



### 4. Pour aller plus loin

Au programme de NSI nous n'étudierons ici que ce type de liste chaînée mais il en existe de nombreuses variantes :

* La liste doublement chaînée :

    ![double chaînage](https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Liste_doublement_cha%C3%AEn%C3%A9e.png/800px-Liste_doublement_cha%C3%AEn%C3%A9e.png)

     

    

* La liste chaînée avec boucle où le dernier _maillon_ est relié au premier.

    **Bonus :** Dessinez un exemple d'une telle liste.



## IV. Implémentation d'une liste chaînée

Pour l'instant, nous avons travaillé uniquement avec l'*interface* des listes chaînées, c'est-à-dire à la main, avec des schémas et avec l'ensemble des opérations que l'on peut réaliser (insertion, accès, suppression...).

Nous allons maintenant *implémenter* nos listes chaînées en Python.

​	**10)** Créez un programme `module_lineaire.py` dans lequel nous allons définir les classes nécessaires à l'implémentation d'une liste chaînée... et d'autres structures linéaires par la suite. *IL FAUT IMPERATIVEMENT CONSERVER CE FICHIER, ON S'EN SERVIRA POUR LES PILES ET LES FILES PAR LA SUITE.*



### 1. Un maillon

Commençons donc par construire la classe `Maillon`  qui possède une valeur et un lien vers le maillon suivant. 

```python
class Maillon :
    '''
    une classe pour construire un maillon d'nue liste chainee
    '''
    
    def __init__(self, valeur = None, suivant = None) :
        '''
        Construit un maillon avec une valeur et un lien vers le maillon suivant.
        
        :param valeur: (any) valeur du maillon
        :param suivant: (Maillon) lien vers le maillon suivant
        '''
        self.valeur = valeur
        self.suivant = suivant
```

​	**11)** Copiez la classe Maillon dans votre fichier `module_lineaire.py`.

​	**12)** Dans la définition du `__init__`, nous avons mis les paramètres `valeur = None` et `suivant = None`. A quoi cela sert-il ?



### 2. La classe `Liste_chainee`

Définissons maintenant la classe `Liste_chainee` qui possède un maillon de _tête_ (vide au départ) et un attribut `longueur` qui correspondra au nombre d'éléments dans la liste (0 au départ donc).

```python
class Liste_chainee :
    '''
    une classe pour implémenter une liste chainee
    '''
    
    def __init__(self):
        '''
        Construit une liste chainee vide de longueur 0.
        '''
        self.tete = None
        self.longueur = ???
```

​	**13)** Copiez la classe `Liste_chainee` dans votre fichier `module_lineaire.py`, et complétez le `__init__`.

On va avoir besoin d'une méthode pour savoir si la liste est vide ou pas :

```python
	def est_vide(self):
        '''
        Renvoie True si la liste est vide et False sinon.
        
        >>> l = Liste_chainee()
        >>> l.est_vide()
        True
        >>> l.ajout(1)
        >>> l.est_vide()
        False
        '''
        return self.tete == ???
```

​	**14)** Copiez la méthode `est_vide` dans votre fichier `Liste_chainee`, et complétez la.

N'oublions pas les lignes indispensables aux Doctests :

```python
################################################
################# doctests
################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = True) 
```

​	**15)** Copiez ces lignes à la toute fin de votre fichier `module_lineaire.py`.


La première méthode à programmer est celle qui ajoute une valeur en tête de liste. 

Pour cela :

* on crée un maillon contenant la valeur et un lien vers le maillon de tête.
* on remplace le maillon de tête de la liste par ce maillon
* on augmente la longueur de la liste de 1

```python
	def ajout(self, valeur):
        '''
        Ajoute un maillon de valeur précisée en tête de liste.
        
        :param valeur: (any) valeur du maillon de tête
        '''
        maillon = Maillon(valeur, self.tete)
        self.tete = ???
        self.longueur = ???
```

​	**16)** Copiez la méthode `ajout` dans votre fichier `Liste_chainee`, et complétez la.


Pour visualiser tout cela, ajoutons la méthode `__str__` pour pouvoir afficher la liste :

* si la liste est vide, on ne renvoie rien.
* sinon, on crée la chaîne souhaitée pour l'affichage en parcourant la liste et on la renvoie.

La méthode `__str__` est une méthode spéciale des classes Python qui permet ensuite d'appeler `print` sur nos objets. Regardez les doctests pour bien comprendre :

```python
	def __str__(self):
        '''
        Renvoie une chaine pour visualiser la liste.
        
        :return: (str)
        
        >>> l = Liste_chainee()
        >>> l.ajout(1)
        >>> print(l)
        (1, ())
        >>> l.ajout(2)
        >>> print(l)
        (2, (1, ()))
        '''
        chaine = '('
        if not self.est_vide() :
            maillon = self.tete
            nb_maillon = 1
            for _ in range(self.longueur) :
                chaine += str(maillon.valeur) + ', ('
                maillon = maillon.suivant
            for _ in range(self.longueur):
                chaine += ')'
        
        chaine += ')'
        return chaine
```

​	**17)** Copiez la méthode `__str__` dans votre classe `Liste_chainee`, regardez bien le code pour en comprendre le fonctionnement, et ajoutez des commentaires pour expliquez ce qui est fait.

​	**18)** Ajoutez une méthode `__len__(self)` qui renvoie le nombre d'éléments de la liste. N'oubliez pas la documentation. Vous pouvez utiliser les doctests suivants :

```python
>>> l = Liste_chainee()
>>> print(l)
???
>>> len(l)
???
>>> l.ajout(4)
>>> l.ajout(3)
>>> l.ajout(2)
>>> l.ajout(1)
>>> print(l)
???
>>> len(l)
???
```



La méthode réservée `__getitem__(self, n)` permet un accès à la valeur d'indice _n_ dans la liste via la syntaxe `l[n]`, comme sur un tableau Python habituel.

Pour coder cette primitive pour une liste chaînée, il faudra parcourir les maillons, de maillon en maillon jusqu'à attendre le maillon d'indice _n_ et enfin renvoyer son attribut _valeur_.

​	**19)** Définissez et codez la méthode `__getitem__(self, n)`. Pour vous aider, voici le départ :

```python
	def __getitem__(self, n):
        '''
        Renvoie la valeur du maillon d'indice précisé.

        :param n: (int) compris entre 0 et la longueur de la liste exclue
        :return: (any) la valeur du maillon d'indice n

        >>> l = Liste_chainee()
        >>> l.ajout(4)
        >>> l.ajout(3)
        >>> l.ajout(2)
        >>> l.ajout(1)
        >>> l[0]
        1
        >>> l[1]
        2
        >>> l[3] 
        4
        '''      
```



Nous avons vu plus haut que l'insertion à un indice _n_ donné était LE point fort des liste chaînée. Nous avons déjà réaliser une insertion en tête de liste (la méthode `ajout`). Nous allons maintenant réaliser la méthode `insere(self, n, valeur)` qui insère la _valeur_ à l'indice _n_ de la liste. Attention n est un entier positif ou nul inférieur ou égal à la longueur de la liste.

![modifier liens](https://upload.wikimedia.org/wikipedia/commons/a/a1/Liste_ajout3.png)



Deux cas sont à distinguer :

* si _n_ vaut 0, il suffit d'appeler la méthode `ajout`.
* sinon, il va falloir _passer_ d'un maillon au suivant jusqu'à atteindre le maillon d'indice _n - 1_ :

    * On crée maintenant un _nouveau_maillon_ avec :
      * pour _valeur_ : celle passée en paramètre.
      * pour _suivant_ : le suivant du maillon d'indice _n - 1_ trouvé précédemment

    * On change le _suivant_ du maillon d'indice _n - 1_ pour notre _nouveau_maillon_ et c'est terminé !

    **20)** Programmez la méthode`insere(self, n, valeur)`. Utilisez une assertion pour que n ne sorte pas de son domaine. Documentez la méthode. Utilisez les doctests ci-dessous :

```python
>>> l = Liste_chainee()
>>> l.ajout(4)
>>> l.ajout(3)
>>> l.ajout(2)
>>> l.ajout(1)
>>> l.insere(2,'a')
>>> print(l)
(1, (2, (a, (3, (4, ())))))
>>> l.insere(0,'b')
>>> print(l)
(b, (1, (2, (a, (3, (4, ()))))))
>>> l.insere(6,'c')
>>> print(l)
(b, (1, (2, (a, (3, (4, (c, ())))))))
```


La méthode `supprime(self, n)` enlève le maillon d'indice _n_ de la liste en modifiant l'attribut _suivant_ du maillon qui le précède dans la liste pour le relier directement au maillon qui le suit. Attention, _n_ est un entier positif ou nul strictement inférieur à la longueur de la liste.

​	**21)** Codez cette méthode `supprime(self, n)`. Documentez la et donnez au moins un doctest.



### Pour aller plus loin

Nous n'avons pas coder toutes les méthodes que peut posséder une liste. Sauriez vous, par exemple, réaliser les méthodes suivantes :

* la méthode réservée `__contains__(self, valeur)` , qui est appelée par le mot-clef `in`, et qui renvoie `True` si la _valeur_ est présente dans la liste et `False` sinon
* la méthode réservée `__setitem__(self, n, valeur)` est l'équivalent de la méthode `__getitem__` codé plus haut : elle modifie la valeur du maillon d'indice _n_ en la remplaçant par celle passée en paramètre.
* la méthode `renverse(self)` qui ré-arrange les éléments de la liste dans le sens inverse.



_________________

Cours rédigés par Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)