# Structures de données linéaires : piles



## I. Interface

### 1. Principe

La **pile** est une structure de données qui fonctionne sur le principe du **dernier arrivé premier sorti**. Pour une pile, on parle de mode **LIFO** pour **L**ast **I**n, **F**irst **O**ut.

![une pile](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Stack_%28data_structure%29_LIFO.svg/800px-Stack_%28data_structure%29_LIFO.svg.png)



Par analogie, on peut penser à une pile d'assiettes:

*  la première assiette que vous avez rangée se retrouve au bas de la pile et ce sera la dernière a être ressortie.
* la dernière assiette rangée se retrouve en haut de la pile et sera sortie en premier.

**On est bien dans une structure dernier arrivé premier sorti !**

![pile d'assiettes](img/assiettes.jpg)

### 2. Méthodes

L'interface d'une pile ne possède que 3 méthodes :

* `est_vide` : renvoie `True` si la pile est vide et `False` sinon
* `empile` ajoute un élément en haut de la pile
* `depile` enlève et renvoie l'élément en haut de la pile

![empiler / depiler](img/empile_depile.png)

​	**1)** Considérant la suite d'instructions suivante, donnez le contenu de la pile à chaque ligne. `ma_pile` est initialement une pile vide. On représentera une pile par des éléments superposés comme dans le schéma ci-dessus.

```python
ma_pile.empile(4)
ma_pile.empile(8)
ma_pile.empile(1)
ma_pile.empile(3)
ma_pile.depile()
ma_pile.empile(5)
ma_pile.depile()
ma_pile.depile()
```

​	**2)** Idem avec les instructions suivantes :

```python
ma_pile.empile(1)
ma_pile.empile(2)
ma_pile.empile(3)
x = ma_pile.depile()
y = ma_pile.depile()
z = ma_pile.depile()
ma_pile.empile(x)
ma_pile.empile(y)
ma_pile.empile(z)
```



## II. Implémentation

En plus des 3 méthodes `est_vide`, `empile` et `depile` vues ci-dessus, nous allons implémenter les méthodes suivantes :

* `__init__(self)` le constructeur qui crée une pile vide avec deux attributs :
    * _tete_ : le maillon de tête (la tête est le haut de la pile) (`None` à l'initialisation)
    * _longueur_ : le nombre d'éléments de la liste
* `__str__(self)` qui renvoie la chaîne de caractère permettant l'utilisation du `print`.



**Nous allons utiliser la structure chaînée** pour implémenter notre Pile. Ce n'est pas la seule façon de faire. On pourrait, par exemple, utiliser la structure  des tableaux de Python. Mais la structure chaînée est bien plus adaptée à l'ajout et au retrait d'éléments, particulièrement en tête,  que celle en tableau indexé comme nous l'avons vu en étudiant les [listes chaînées](listes.md).



​	**3)** Placez le code suivant dans votre fichier `module_lineaire.py` et complétez le :

```python
class Pile:
    '''
    une classe pour modéliser une pile
    '''
    
    def __init__(self):
        '''
        Construit une pile vide.
        '''
        self.tete = None
        self.longueur = ???
    
    def est_vide(self):
        '''
        Renvoie True si la pile est vide et False sinon.
        
        :return: (bool)
        
        >>> p = Pile()
        >>> p.est_vide()
        True
        >>> p.empile(1)
        >>> p.est_vide()
        False
        '''
        return ???
    
    def empile(self, valeur):
        '''
        Ajoute la valeur en haut de la pile.
        
        >>> p = Pile()
        >>> p.empile(1)
        >>> p.empile(2)
        >>> p.empile('a')
        >>> print(p)
        (a, (2, (1, ())))        
        '''
        maillon = Maillon(???, ???)
        self.tete = ???
        self.longueur = ???
        
    def depile(self):
        '''
        Enlève la valeur en haut de pile et la renvoie.
        
        :return: (any) la valeur dépilée
        
        >>> p = Pile()
        >>> p.empile(1)
        >>> p.empile(2)
        >>> p.empile('a')
        >>> p.depile()
        'a'
        >>> print(p)
        (2, (1, ()))
        '''
        assert (not self.est_vide()), "impossible de dépiler une pile vide"
        
        valeur_a_renvoyer = ???
        self.tete = ???
        self.longueur = ???
        return valeur_a_renvoyer

        
    def __str__(self):
        '''
        Renvoie une chaine pour visualiser la liste.
        
        :return: (str)
        >>> l = Pile()
        >>> l.empile(1)
        >>> print(l)
        (1, ())
        >>> l.empile(2)
        >>> print(l)
        (2, (1, ()))
        '''
        chaine = '('
        if not self.est_vide() :
            maillon = self.tete
            nb_maillon = 1
            for _ in range(self.longueur) :
                chaine += str(maillon.valeur)+ ', ('
                maillon = maillon.suivant
            for _ in range(self.longueur):
                chaine += ')'
        
        chaine += ')'
        return chaine
```



### Pour aller plus loin

​	**4)** Implémentez la méthode `__len__` dans votre classe Pile. Cette méthode renvoie le nombre d'éléments de la pile. Vous pouvez faire 2 versions de cette méthode :

* première version qui renvoie `self.longueur`
* deuxième version qui vide la pile en comptant le nombre d'éléments retirés, puis ré-empile les éléments.



_________________

Cours rédigés par Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)