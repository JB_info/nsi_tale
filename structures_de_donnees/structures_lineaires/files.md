# Structures de données linéaires : files



## I. Interface

### 1. Principe

La **file** est une structure de données qui fonctionne sur le principe du **premier arrivé premier sorti**. Pour une file, on parle de mode **FIFO** pour **F**irst **I**n, **F**irst **O**ut.



![une file](https://upload.wikimedia.org/wikipedia/commons/2/2a/FIFO_PEPS.png)



Par analogie, on peut penser à une file d'attente :

*  les gens entrent dans la file par l'arrière. 
*  le premier qui entre dans la file sera le premier à en sortir

**On est bien sur le principe du premier arrivé, premier sorti**

![file d'attente](img/attente.jpg)

### 2. Méthodes

L'interface d'une file ne possède que 3 méthodes :

- `est_vide` : renvoie `True` si la file est vide et `False` sinon
- `enfile` ajoute un élément en queue de la file
- `defile` enlève et renvoie l'élément en tête de la file

[![enfiler / defiler](img/enfile_defile.png)]()

​	**1)** Considérant la suite d'instructions suivante, donnez le contenu de la file à chaque ligne. `ma_file` est initialement une file vide. On représentera une file comme dans le schéma ci-dessus.

```python
ma_file.enfile(2)
ma_file.enfile(3)
ma_file.enfile(5)
ma_file.enfile(7)
ma_file.defile()
ma_file.enfile(9)
ma_file.defile()
```

​	**2)** Idem avec les instructions suivantes :

```python
ma_file.enfile(1)
ma_file.enfile(2)
ma_file.enfile(3)
x = ma_file.defile()
y = ma_file.defile()
z = ma_file.defile()
ma_file.enfile(z)
ma_file.enfile(y)
ma_file.enfile(x)
```



## II. Implémentation

En plus des 3 méthodes `est_vide`, `enfile` et `defile` vues ci-dessus, nous allons implémenter les méthodes suivantes :

* `__init__(self)` le constructeur qui crée une file vide avec deux attributs :
    * _tete_ : le maillon de tête (`None` à l'initialisation)
    * _longueur_ : le nombre d'élément(s) de la liste (0 à l'initialisation)
* `__str__(self)` qui renvoie la chaîne de caractère permettant l'utilisation du `print`.



**Nous allons utiliser la structure chaînée** pour implémenter notre File. Ce n'est pas la seule façon de faire. On pourrait, par exemple, utiliser la structure des tableaux de Python. Mais la structure chaînée est bien plus adaptée à l'ajout et au retrait d'éléments que celle en tableau indexé comme nous l'avons vu en étudiant les [listes chaînées](listes.md).



Notre File aura une tête, comme une liste chaînée classique, mais aussi une *queue*, ce qui permettra d'ajouter un élément un queue sans être obligé de parcourir la File à chaque fois.



**3)** Placez le code suivant dans votre fichier `module_lineaire.py` et complétez le :



```python
class File:
    '''
    une classe pour les Files
    '''
    
    def __init__(self):
        '''
        Contruit une file vide de longueur 0 possédant une tete et une queue.
        '''
        self.tete = None
        self.queue = None
        self.longueur = ???
    
    def est_vide(self):
        '''
        Renvoie True si la file est vide et False sinon.
        
        >>> f = File()
        >>> f.est_vide()
        True
        '''
        return ???
    
    def enfile(self, valeur):
        '''
        Ajoute la valeur en queue de file.
        
        :param valeur: (any) la valeur à ajouter

        >>> f = File()
        >>> f.enfile(3)
        >>> f.est_vide()
        False
        '''
        maillon = Maillon(???, ???)
        if self.longueur == 0:
            self.tete = ???
            self.queue = ???
        else:
            self.queue.suivant = ???
            self.queue = ???
        self.longueur = ???
        
    def defile(self):
        '''
        Renvoie la valeur en tête de file et la retire de la file.
        
        :return: (any) la valeur défilée

        >>> f = File()
        >>> f.enfile(3)
        >>> f.enfile(2)
        >>> f.defile()
        3
        >>> f.defile()
        2
        >>> f.est_vide()
        True
        '''
        assert (not self.est_vide()), "impossible de défiler une file vide"
        valeur_a_renvoyer = ???
        self.tete = ???
        self.longueur = ???
        return valeur_a_renvoyer
    
    def __str__(self):
        '''
        Renvoie la chaine permettant d'afficher la file.
        :return: (str)
        
        >>> f = File()
        >>> f.enfile(3)
        >>> f.enfile(2)
        >>> f.enfile ('a')
        >>> print(f)
        (3, (2, (a, ())))        
        '''
        chaine = '('
        if not self.est_vide() :
            maillon = self.tete
            nb_maillon = 1
            for _ in range(self.longueur) :
                chaine += str(maillon.valeur)+ ', ('
                maillon = maillon.suivant
            for _ in range(self.longueur):
                chaine += ')'
        
        chaine += ')'
        return chaine
```



### Pour aller plus loin

​	**4)** Implémentez la méthode `__len__` dans votre classe File. Cette méthode renvoie le nombre d'éléments de la file. Vous pouvez faire 2 versions de cette méthode :

* première version qui renvoie `self.longueur`
* deuxième version qui vide la file en comptant le nombre d'éléments retirés, puis ré-enfile les éléments.



_________________

Cours rédigés par Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)















